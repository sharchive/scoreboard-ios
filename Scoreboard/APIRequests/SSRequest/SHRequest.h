//
//  SSRequest.h
//  Scoreboard
//
//  Created by Tom Bates on 27/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHRequest : NSMutableURLRequest

+ (NSMutableURLRequest *)POSTRequestWithExtension:(NSString *)extension andBody:(NSData *)body;
+ (NSMutableURLRequest *)PUTRequestWithExtension:(NSString *)extension andBody:(NSData *)body;
+ (NSMutableURLRequest *)GETRequestWithExtension:(NSString *)extension;
+ (NSMutableURLRequest *)BasicRequestWithExtension:(NSString *)extension;
+ (NSMutableURLRequest *)BasicRequest;
+ (NSString *)BaseURLString;

@end
