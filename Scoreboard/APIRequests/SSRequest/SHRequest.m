//
//  SSRequest.m
//  Scoreboard
//
//  Created by Tom Bates on 27/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHRequest.h"

@implementation SHRequest

+ (NSMutableURLRequest *)POSTRequestWithExtension:(NSString *)extension andBody:(NSData *)body
{
    NSMutableURLRequest *request =  [SHRequest BasicRequestWithExtension:extension];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    
    return request;
}

+ (NSMutableURLRequest *)PUTRequestWithExtension:(NSString *)extension andBody:(NSData *)body
{
    NSMutableURLRequest *request =  [SHRequest BasicRequestWithExtension:extension];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"PUT"];
    [request setHTTPBody:body];
    
    return request;
}

+ (NSMutableURLRequest *)GETRequestWithExtension:(NSString *)extension
{
    NSMutableURLRequest *request =  [SHRequest BasicRequestWithExtension:extension];
    [request setHTTPMethod:@"GET"];
    return request;
}

+ (NSMutableURLRequest *)BasicRequestWithExtension:(NSString *)extension
{
    NSString *string = [NSString stringWithFormat:@"%@/%@",[SHRequest BaseURLString],extension];
    NSString* webStringURL = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL* url = [NSURL URLWithString:webStringURL];
    NSMutableURLRequest *request =  [[NSMutableURLRequest alloc]initWithURL:url];
    return request;
}

+ (NSMutableURLRequest *)BasicRequest
{
    NSString *string = [SHRequest BaseURLString];
    NSMutableURLRequest *request =  [[NSMutableURLRequest alloc]initWithURL:
                                     [NSURL URLWithString:string]];
    return request;
}

+ (NSString *)BaseURLString
{
    return @"http://scoreboard.sporthub.io";
}

@end
