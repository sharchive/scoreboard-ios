//
//  AppDelegate.h
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CoreDataManager;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CoreDataManager *coreDataManager;

@end
