//
//  GameBuilder.h
//  Scoreboard
//
//  Created by Tom Bates on 12/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHSport;
@class Match;

@interface MatchBuilder : NSObject

@property (strong, nonatomic) SHSport *sport;
@property (strong, nonatomic) NSArray *teams;

+ (Match *)gameWithBlock:(void (^)(MatchBuilder *builder))block;

- (Match *)build;

@end
