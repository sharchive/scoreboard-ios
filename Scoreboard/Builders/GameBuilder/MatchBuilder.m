//
//  GameBuilder.m
//  Scoreboard
//
//  Created by Tom Bates on 12/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "MatchBuilder.h"
#import "Match.h"

#import "MatchService.h"
#import "TeamService.h"
#import "TeamInstanceService.h"
#import "Match.h"
#import "Team.h"
#import "TeamInstance.h"
#import "ColourService.h"

@interface MatchBuilder ()

@property (strong, nonatomic) MatchService *matchService;
@property (strong, nonatomic) TeamInstanceService *teamGameService;
@property (strong, nonatomic) TeamService *teamService;
@property (strong, nonatomic) ColourService *colourService;

@end

@implementation MatchBuilder

+ (Match *)gameWithBlock:(void (^)(MatchBuilder *builder))block {
    NSParameterAssert(block);
    
    MatchBuilder *builder = [[MatchBuilder alloc] init];
    block(builder);
    return [builder build];
}

- (Match *)build
{
    Match *game = [self.matchService createNewGame];
    if (self.teams)
    {
        [NSOrderedSet orderedSetWithArray:self.teams];
    }
    else
    {
        [game setTeams:[self createHomeAndAwayTeams]];
    }
    game.sport = self.sport;
    return game;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.matchService = [MatchService new];
        self.teamGameService = [TeamInstanceService new];
        self.teamService = [TeamService new];
        self.colourService = [ColourService new];
    }
    return self;
}

- (NSOrderedSet *)createHomeAndAwayTeams
{
    TeamInstance *teamHome = [self createNewTeamGameWithTeamName:NSLocalizedString(@"Home", nil)];
    TeamInstance *teamAway = [self createNewTeamGameWithTeamName:NSLocalizedString(@"Away", nil)];
    
    return [NSOrderedSet orderedSetWithObjects:teamHome, teamAway, nil];
}

- (Match *)createHomeAndAwayGame
{
    Match *game = [self.matchService createNewGame];
    [game addTeamsObject:[self createNewTeamGameWithTeamName:NSLocalizedString(@"Home", nil)]];
    [game addTeamsObject:[self createNewTeamGameWithTeamName:NSLocalizedString(@"Away", nil)]];
    return game;
}

- (TeamInstance *)createNewTeamGameWithTeamName:(NSString *)name
{
    TeamInstance *newTeamGame = [self.teamGameService createNewTeamGame];
    newTeamGame.team = [self createNewTeamWithLocalName:name];
    newTeamGame.assignedColour = newTeamGame.team.colour;
    return newTeamGame;
}

- (Team *)createNewTeamWithLocalName:(NSString *)name
{
    Team *newTeam = [self.teamService createTeam];
    newTeam.full_name = name;
    newTeam.colour = [self.colourService createRandomColour];
    
    return newTeam;
}

@end
