//
//  UserMessageBuilder.h
//  Scoreboard
//
//  Created by Tom Bates on 03/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserNotification;

@interface UserMessageBuilder : NSObject

@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) UIImage *icon;

+ (UserNotification *)messageWithBlock:(void (^)(UserMessageBuilder *builder))block;

@end
