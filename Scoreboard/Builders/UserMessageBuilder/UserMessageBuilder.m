//
//  UserMessageBuilder.m
//  Scoreboard
//
//  Created by Tom Bates on 03/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "UserMessageBuilder.h"
#import "UserNotification.h"

@implementation UserMessageBuilder

+ (UserNotification *)messageWithBlock:(void (^)(UserMessageBuilder *builder))block {
    NSParameterAssert(block);
    UserMessageBuilder *builder = [UserMessageBuilder new];
    block(builder);
    return [builder build];
}

- (UserNotification *)build
{
    UserNotification *notificationMessage = [UserNotification new];
    notificationMessage.message = self.message;
    notificationMessage.icon = self.icon;
    return notificationMessage;
}

@end
