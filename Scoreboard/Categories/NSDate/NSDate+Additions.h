//
//  NSDate+Additions.h
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Additions)

+ (NSDate *)nowUTC;

@end
