//
//  NSDate+Additions.m
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (Additions)

+ (NSDate *)nowUTC
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    return [dateFormatter dateFromString:dateString];
    
}

@end
