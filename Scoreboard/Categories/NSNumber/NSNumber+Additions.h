//
//  NSNumber+Additions.h
//  Scoreboard
//
//  Created by Tom Bates on 07/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Additions)

- (NSString *)stringNotNil;

@end
