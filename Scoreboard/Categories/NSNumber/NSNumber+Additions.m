//
//  NSNumber+Additions.m
//  Scoreboard
//
//  Created by Tom Bates on 07/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "NSNumber+Additions.h"

@implementation NSNumber (Additions)

- (NSString *)stringNotNil
{
    if (!self)
    {
        return @"0";
    }
    else return [self stringValue];
}

@end
