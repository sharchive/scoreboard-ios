//
//  NSNumber+TimeAdditions.h
//  Scoreboard
//
//  Created by Tom Bates on 07/08/2013.
//  Copyright (c) 2013 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (TimeAdditions)

- (NSString *)bigClockString;
- (NSString *)smallClockString;

- (NSString *)minutesString;
- (NSString *)secondsString;
- (NSString *)milliString;

- (NSNumber *)asMilliToSeconds;

@end
