//
//  NSNumber+TimeAdditions.m
//  Scoreboard
//
//  Created by Tom Bates on 07/08/2013.
//  Copyright (c) 2013 Sport Search UK. All rights reserved.
//

#import "NSNumber+TimeAdditions.h"

@implementation NSNumber (TimeAdditions)

- (NSString *)bigClockString
{
    NSString *timeString = [NSString stringWithFormat:@"%@%@%@",   [self minutesString],
     [self secondsString],
     [self milliString]];
    
    return timeString;
}

- (NSMutableAttributedString *)bigAttributedStringWithString:(NSString *)string
{
    NSMutableAttributedString *largeString = [[NSMutableAttributedString alloc] initWithString:string];
    [largeString addAttribute:NSForegroundColorAttributeName value:[AppearanceManager aquaBlueColour] range:NSMakeRange(0,largeString.length)];
    [largeString addAttribute:NSFontAttributeName value:[AppearanceManager largeFont] range:NSMakeRange(0,5)];
    [largeString addAttribute:NSFontAttributeName value:[AppearanceManager mediumFont] range:NSMakeRange(6,2)];

    return largeString;
}

- (NSString *)smallClockString
{
    return [NSString stringWithFormat:@"%@:%@", [self secondsString],
                                                [self milliString]];
}

- (NSString *)minutesString
{
    NSInteger time = [self integerValue]/6000;
    NSString *string = [NSString stringWithFormat:@"%02ld",(long)time];
    return string;
}

- (NSNumber *)asMilliToSeconds
{
    int time = ([self integerValue] /100)%60;
    NSNumber *number = [NSNumber numberWithInt:time];
    return number;
}

- (NSString *)secondsString
{
    int time = ([self integerValue] /100)%60;
    NSString *string = [NSString stringWithFormat:@"%02d",time];
    return string;
}

- (NSString *)milliString
{
    int time = [self integerValue]%100;
    NSString *string = [NSString stringWithFormat:@"%02d",time];
    return string;
}

@end
