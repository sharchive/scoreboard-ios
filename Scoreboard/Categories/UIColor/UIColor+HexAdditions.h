//
//  UIColor+HexAdditions.h
//  Scoreboard
//
//  Created by Tom Bates on 27/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Colour;

@interface UIColor (HexAdditions)

+(UIColor *)colorWithHexString:(NSString *)hexString;
+(NSString *)hexValuesFromUIColor:(UIColor *)color;
+(NSString *)hexValuesFromColor:(Colour *)color;

@end
