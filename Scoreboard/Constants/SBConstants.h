//
//  SBConstants.h
//  Scoreboard
//
//  Created by Tom Bates on 11/05/2013.
//  Copyright (c) 2013 Sport Search UK. All rights reserved.
//

typedef enum{
    SBScoreboardTypeGeneric,
    SBScoreboardTypeTennis
}SBScoreboardType;

typedef enum{
    SBDefaultTypeNone,
    SBDefaultTypeSwitch,
    SBDefaultTypeSegment,
    SBDefaultTypeTime
}SBDefaultType;

typedef enum{
    SBClockCountDirectionDown,
    SBClockCountDirectionUp
}SBClockCountDirection;

static NSString *SBGameClockKey = @"Game Clock";
static NSString *SBShotClockKey = @"Shot Clock";
static NSString *SBTimeKey= @"Time";
static NSString *SBBuzzerKey= @"Buzzer";
static NSString *SBCountUpKey = @"Count Up";

static NSString *SBGameTimerFinished = @"GameTimerFinished";



#ifndef Scoreboard_SBConstants_h
#define Scoreboard_SBConstants_h

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define SB_DEVICE_IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define SB_DEVICE_IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#endif

