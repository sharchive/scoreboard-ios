//
//  SocialConstants.h
//  Scoreboard
//
//  Created by Tom Bates on 08/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#ifndef Scoreboard_SocialConstants_h
#define Scoreboard_SocialConstants_h

typedef enum {
    SCSDisconnected, 
    SCSConnected
}SocialConnectionStatus;


#endif
