#import "_Colour.h"

@interface Colour : _Colour {}

- (NSString *)hexValue;

@end
