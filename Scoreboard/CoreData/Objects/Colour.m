#import "Colour.h"


@interface Colour ()

@end


@implementation Colour

- (NSString *)hexValue
{
    int redDec = (int)(self.redValue * 255);
    int greenDec = (int)(self.greenValue * 255);
    int blueDec = (int)(self.blueValue * 255);
    
    NSString *returnString = [NSString stringWithFormat:@"%02x%02x%02x", (unsigned int)redDec, (unsigned int)greenDec, (unsigned int)blueDec];
    
    return returnString;
}

@end
