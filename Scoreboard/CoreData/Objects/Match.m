#import "Match.h"
#import "SHSport.h"
#import "TeamInstance.h"
#import "NSDate+Additions.h"

@interface Match ()

// Private interface goes here.

@end


@implementation Match

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.date = [NSDate nowUTC];
}

-(void)addTeamsObject:(TeamInstance *)value_
{
    NSMutableOrderedSet *teamSet = [self.teams mutableCopy];
    [teamSet addObject:value_];
    self.teams = [NSOrderedSet orderedSetWithOrderedSet:teamSet];
}

- (void)removeTeamsObject:(TeamInstance *)value_
{
    NSMutableOrderedSet *teamSet = [self.teams mutableCopy];
    [teamSet removeObject:value_];
    self.teams = [NSOrderedSet orderedSetWithOrderedSet:teamSet];
}

- (NSMutableDictionary *)asDictionary
{
    NSMutableDictionary *dictionary = [super asDictionary];
    [dictionary setValue:self.date forKey:@"date"];
    [dictionary setValue:self.sport.guid forKey:@"sportID"];
    
    return dictionary;
}

- (BOOL)hasBeenShared
{
    return self.externalURL != nil;
}

@end
