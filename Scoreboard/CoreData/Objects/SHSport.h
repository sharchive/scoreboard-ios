#import "_SHSport.h"

@interface SHSport : _SHSport {}

-(UIColor*)teamColourFromRGB:(NSString *)RGB;
-(UIColor*)teamColourFromImage:(NSString *)imagePath;

- (void)increasePeriods;
- (void)decreasePeriods;

@end
