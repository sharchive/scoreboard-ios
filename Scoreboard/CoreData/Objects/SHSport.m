#import "SHSport.h"


@interface SHSport ()

// Private interface goes here.

@end


@implementation SHSport

-(UIColor*)teamColourFromRGB:(NSString *)RGB
{
    NSArray *values = [RGB componentsSeparatedByString:@" "];
    CGFloat red = [[values objectAtIndex:1] floatValue];
    CGFloat green = [[values objectAtIndex:2] floatValue];
    CGFloat blue = [[values objectAtIndex:3] floatValue];
    CGFloat alpha = [[values objectAtIndex:4] floatValue];
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

-(UIColor*)teamColourFromImage:(NSString *)imagePath
{
    UIColor *colour = [UIColor colorWithPatternImage:[UIImage imageNamed:imagePath]];
    return colour;
}

- (void)increasePeriods
{
    if (self.periodsValue < 4) self.periodsValue ++;
}

- (void)decreasePeriods
{
    if (self.periodsValue > 1) self.periodsValue --;
}

@end
