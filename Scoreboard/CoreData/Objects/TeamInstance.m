#import "TeamInstance.h"
#import "Team.h"

@interface TeamInstance ()

// Private interface goes here.

@end


@implementation TeamInstance

+ (TeamInstance *)teamGameWithTeam:(Team *)team
{
    //    TeamGame *tGame = [TeamGame MR_createEntity];
    TeamInstance *tGame;
    [tGame setTeam:team];
    
    return tGame;
}

-(NSString *)fullTeamName
{
    return self.team.full_name;
}

-(Colour *)teamColour
{
    return self.team.colour;
}

@end
