// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Colour.h instead.

#import <CoreData/CoreData.h>
#import "_SHEntity.h"

extern const struct ColourAttributes {
	__unsafe_unretained NSString *blue;
	__unsafe_unretained NSString *green;
	__unsafe_unretained NSString *red;
} ColourAttributes;

extern const struct ColourRelationships {
	__unsafe_unretained NSString *team;
	__unsafe_unretained NSString *teamInstance;
} ColourRelationships;

extern const struct ColourFetchedProperties {
} ColourFetchedProperties;

@class Team;
@class TeamInstance;





@interface ColourID : NSManagedObjectID {}
@end

@interface _Colour : _SHEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ColourID*)objectID;





@property (nonatomic, strong) NSNumber* blue;



@property float blueValue;
- (float)blueValue;
- (void)setBlueValue:(float)value_;

//- (BOOL)validateBlue:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* green;



@property float greenValue;
- (float)greenValue;
- (void)setGreenValue:(float)value_;

//- (BOOL)validateGreen:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* red;



@property float redValue;
- (float)redValue;
- (void)setRedValue:(float)value_;

//- (BOOL)validateRed:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Team *team;

//- (BOOL)validateTeam:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) TeamInstance *teamInstance;

//- (BOOL)validateTeamInstance:(id*)value_ error:(NSError**)error_;





@end

@interface _Colour (CoreDataGeneratedAccessors)

@end

@interface _Colour (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveBlue;
- (void)setPrimitiveBlue:(NSNumber*)value;

- (float)primitiveBlueValue;
- (void)setPrimitiveBlueValue:(float)value_;




- (NSNumber*)primitiveGreen;
- (void)setPrimitiveGreen:(NSNumber*)value;

- (float)primitiveGreenValue;
- (void)setPrimitiveGreenValue:(float)value_;




- (NSNumber*)primitiveRed;
- (void)setPrimitiveRed:(NSNumber*)value;

- (float)primitiveRedValue;
- (void)setPrimitiveRedValue:(float)value_;





- (Team*)primitiveTeam;
- (void)setPrimitiveTeam:(Team*)value;



- (TeamInstance*)primitiveTeamInstance;
- (void)setPrimitiveTeamInstance:(TeamInstance*)value;


@end
