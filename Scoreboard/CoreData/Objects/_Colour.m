// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Colour.m instead.

#import "_Colour.h"

const struct ColourAttributes ColourAttributes = {
	.blue = @"blue",
	.green = @"green",
	.red = @"red",
};

const struct ColourRelationships ColourRelationships = {
	.team = @"team",
	.teamInstance = @"teamInstance",
};

const struct ColourFetchedProperties ColourFetchedProperties = {
};

@implementation ColourID
@end

@implementation _Colour

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Colour" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Colour";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Colour" inManagedObjectContext:moc_];
}

- (ColourID*)objectID {
	return (ColourID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"blueValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"blue"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"greenValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"green"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"redValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"red"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic blue;



- (float)blueValue {
	NSNumber *result = [self blue];
	return [result floatValue];
}

- (void)setBlueValue:(float)value_ {
	[self setBlue:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveBlueValue {
	NSNumber *result = [self primitiveBlue];
	return [result floatValue];
}

- (void)setPrimitiveBlueValue:(float)value_ {
	[self setPrimitiveBlue:[NSNumber numberWithFloat:value_]];
}





@dynamic green;



- (float)greenValue {
	NSNumber *result = [self green];
	return [result floatValue];
}

- (void)setGreenValue:(float)value_ {
	[self setGreen:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveGreenValue {
	NSNumber *result = [self primitiveGreen];
	return [result floatValue];
}

- (void)setPrimitiveGreenValue:(float)value_ {
	[self setPrimitiveGreen:[NSNumber numberWithFloat:value_]];
}





@dynamic red;



- (float)redValue {
	NSNumber *result = [self red];
	return [result floatValue];
}

- (void)setRedValue:(float)value_ {
	[self setRed:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveRedValue {
	NSNumber *result = [self primitiveRed];
	return [result floatValue];
}

- (void)setPrimitiveRedValue:(float)value_ {
	[self setPrimitiveRed:[NSNumber numberWithFloat:value_]];
}





@dynamic team;

	

@dynamic teamInstance;

	






@end
