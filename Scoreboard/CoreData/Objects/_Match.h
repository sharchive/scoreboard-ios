// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Match.h instead.

#import <CoreData/CoreData.h>
#import "_SHEntity.h"

extern const struct MatchAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *externalURL;
	__unsafe_unretained NSString *gameShared;
	__unsafe_unretained NSString *inProgress;
} MatchAttributes;

extern const struct MatchRelationships {
	__unsafe_unretained NSString *sport;
	__unsafe_unretained NSString *teams;
} MatchRelationships;

extern const struct MatchFetchedProperties {
} MatchFetchedProperties;

@class SHSport;
@class TeamInstance;






@interface MatchID : NSManagedObjectID {}
@end

@interface _Match : _SHEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MatchID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* externalURL;



//- (BOOL)validateExternalURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gameShared;



@property BOOL gameSharedValue;
- (BOOL)gameSharedValue;
- (void)setGameSharedValue:(BOOL)value_;

//- (BOOL)validateGameShared:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* inProgress;



@property BOOL inProgressValue;
- (BOOL)inProgressValue;
- (void)setInProgressValue:(BOOL)value_;

//- (BOOL)validateInProgress:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) SHSport *sport;

//- (BOOL)validateSport:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSOrderedSet *teams;

- (NSMutableOrderedSet*)teamsSet;





@end

@interface _Match (CoreDataGeneratedAccessors)

- (void)addTeams:(NSOrderedSet*)value_;
- (void)removeTeams:(NSOrderedSet*)value_;
- (void)addTeamsObject:(TeamInstance*)value_;
- (void)removeTeamsObject:(TeamInstance*)value_;

@end

@interface _Match (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSString*)primitiveExternalURL;
- (void)setPrimitiveExternalURL:(NSString*)value;




- (NSNumber*)primitiveGameShared;
- (void)setPrimitiveGameShared:(NSNumber*)value;

- (BOOL)primitiveGameSharedValue;
- (void)setPrimitiveGameSharedValue:(BOOL)value_;




- (NSNumber*)primitiveInProgress;
- (void)setPrimitiveInProgress:(NSNumber*)value;

- (BOOL)primitiveInProgressValue;
- (void)setPrimitiveInProgressValue:(BOOL)value_;





- (SHSport*)primitiveSport;
- (void)setPrimitiveSport:(SHSport*)value;



- (NSMutableOrderedSet*)primitiveTeams;
- (void)setPrimitiveTeams:(NSMutableOrderedSet*)value;


@end
