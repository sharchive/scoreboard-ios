// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Match.m instead.

#import "_Match.h"

const struct MatchAttributes MatchAttributes = {
	.date = @"date",
	.externalURL = @"externalURL",
	.gameShared = @"gameShared",
	.inProgress = @"inProgress",
};

const struct MatchRelationships MatchRelationships = {
	.sport = @"sport",
	.teams = @"teams",
};

const struct MatchFetchedProperties MatchFetchedProperties = {
};

@implementation MatchID
@end

@implementation _Match

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Match" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Match";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Match" inManagedObjectContext:moc_];
}

- (MatchID*)objectID {
	return (MatchID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"gameSharedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gameShared"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"inProgressValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"inProgress"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic date;






@dynamic externalURL;






@dynamic gameShared;



- (BOOL)gameSharedValue {
	NSNumber *result = [self gameShared];
	return [result boolValue];
}

- (void)setGameSharedValue:(BOOL)value_ {
	[self setGameShared:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveGameSharedValue {
	NSNumber *result = [self primitiveGameShared];
	return [result boolValue];
}

- (void)setPrimitiveGameSharedValue:(BOOL)value_ {
	[self setPrimitiveGameShared:[NSNumber numberWithBool:value_]];
}





@dynamic inProgress;



- (BOOL)inProgressValue {
	NSNumber *result = [self inProgress];
	return [result boolValue];
}

- (void)setInProgressValue:(BOOL)value_ {
	[self setInProgress:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveInProgressValue {
	NSNumber *result = [self primitiveInProgress];
	return [result boolValue];
}

- (void)setPrimitiveInProgressValue:(BOOL)value_ {
	[self setPrimitiveInProgress:[NSNumber numberWithBool:value_]];
}





@dynamic sport;

	

@dynamic teams;

	
- (NSMutableOrderedSet*)teamsSet {
	[self willAccessValueForKey:@"teams"];
  
	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"teams"];
  
	[self didAccessValueForKey:@"teams"];
	return result;
}
	






@end
