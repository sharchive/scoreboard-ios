// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Period.h instead.

#import <CoreData/CoreData.h>
#import "_SHEntity.h"

extern const struct PeriodAttributes {
	__unsafe_unretained NSString *date;
} PeriodAttributes;

extern const struct PeriodRelationships {
} PeriodRelationships;

extern const struct PeriodFetchedProperties {
} PeriodFetchedProperties;




@interface PeriodID : NSManagedObjectID {}
@end

@interface _Period : _SHEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PeriodID*)objectID;





@property (nonatomic, strong) NSNumber* date;



@property int16_t dateValue;
- (int16_t)dateValue;
- (void)setDateValue:(int16_t)value_;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;






@end

@interface _Period (CoreDataGeneratedAccessors)

@end

@interface _Period (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveDate;
- (void)setPrimitiveDate:(NSNumber*)value;

- (int16_t)primitiveDateValue;
- (void)setPrimitiveDateValue:(int16_t)value_;




@end
