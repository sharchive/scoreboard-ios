// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Period.m instead.

#import "_Period.h"

const struct PeriodAttributes PeriodAttributes = {
	.date = @"date",
};

const struct PeriodRelationships PeriodRelationships = {
};

const struct PeriodFetchedProperties PeriodFetchedProperties = {
};

@implementation PeriodID
@end

@implementation _Period

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Period" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Period";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Period" inManagedObjectContext:moc_];
}

- (PeriodID*)objectID {
	return (PeriodID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"dateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"date"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic date;



- (int16_t)dateValue {
	NSNumber *result = [self date];
	return [result shortValue];
}

- (void)setDateValue:(int16_t)value_ {
	[self setDate:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveDateValue {
	NSNumber *result = [self primitiveDate];
	return [result shortValue];
}

- (void)setPrimitiveDateValue:(int16_t)value_ {
	[self setPrimitiveDate:[NSNumber numberWithShort:value_]];
}










@end
