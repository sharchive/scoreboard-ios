// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SHApp.h instead.

#import <CoreData/CoreData.h>


extern const struct SHAppAttributes {
	__unsafe_unretained NSString *bundleID;
	__unsafe_unretained NSString *currentVersion;
	__unsafe_unretained NSString *iTunesURL;
	__unsafe_unretained NSString *imagePath;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *urlScheme;
} SHAppAttributes;

extern const struct SHAppRelationships {
} SHAppRelationships;

extern const struct SHAppFetchedProperties {
} SHAppFetchedProperties;









@interface SHAppID : NSManagedObjectID {}
@end

@interface _SHApp : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (SHAppID*)objectID;





@property (nonatomic, strong) NSString* bundleID;



//- (BOOL)validateBundleID:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* currentVersion;



//- (BOOL)validateCurrentVersion:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* iTunesURL;



//- (BOOL)validateITunesURL:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* imagePath;



//- (BOOL)validateImagePath:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* urlScheme;



//- (BOOL)validateUrlScheme:(id*)value_ error:(NSError**)error_;






@end

@interface _SHApp (CoreDataGeneratedAccessors)

@end

@interface _SHApp (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveBundleID;
- (void)setPrimitiveBundleID:(NSString*)value;




- (NSString*)primitiveCurrentVersion;
- (void)setPrimitiveCurrentVersion:(NSString*)value;




- (NSString*)primitiveITunesURL;
- (void)setPrimitiveITunesURL:(NSString*)value;




- (NSString*)primitiveImagePath;
- (void)setPrimitiveImagePath:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitiveUrlScheme;
- (void)setPrimitiveUrlScheme:(NSString*)value;




@end
