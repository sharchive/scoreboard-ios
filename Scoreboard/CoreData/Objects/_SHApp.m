// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SHApp.m instead.

#import "_SHApp.h"

const struct SHAppAttributes SHAppAttributes = {
	.bundleID = @"bundleID",
	.currentVersion = @"currentVersion",
	.iTunesURL = @"iTunesURL",
	.imagePath = @"imagePath",
	.name = @"name",
	.urlScheme = @"urlScheme",
};

const struct SHAppRelationships SHAppRelationships = {
};

const struct SHAppFetchedProperties SHAppFetchedProperties = {
};

@implementation SHAppID
@end

@implementation _SHApp

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SHApp" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SHApp";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SHApp" inManagedObjectContext:moc_];
}

- (SHAppID*)objectID {
	return (SHAppID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic bundleID;






@dynamic currentVersion;






@dynamic iTunesURL;






@dynamic imagePath;






@dynamic name;






@dynamic urlScheme;











@end
