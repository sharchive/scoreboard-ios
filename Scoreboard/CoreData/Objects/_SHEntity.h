#import "__SHEntity.h"

@interface _SHEntity : __SHEntity {}

- (NSMutableDictionary *)asDictionary;
- (void)resetObject;
- (NSManagedObject *) clone;

@end
