#import "_SHEntity.h"
#import "NSDate+Additions.h"

@interface _SHEntity ()

// Private interface goes here.

@end


@implementation _SHEntity
@synthesize dateUpdatedUTC = _dateUpdatedUTC;
@synthesize pushRequired = _pushRequired;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.dateCreatedUTC = [NSDate nowUTC];
    self.dateUpdatedUTC = [NSDate nowUTC];
}

- (void)didChangeValueForKey:(NSString *)key
{
    [super didChangeValueForKey:key];
    if (![key isEqualToString:@"dateUpdatedUTC"]) [self updateDateUpdated];
    if (![key isEqualToString:@"pushRequired"]) [self updatePushRequired];
}


- (void)updateDateUpdated
{
    self.dateUpdatedUTC = [NSDate nowUTC];
    [super willChangeValueForKey:@"dateUpdatedUTC"];
    [super didChangeValueForKey:@"dateUpdatedUTC"];
}

- (void)updatePushRequired
{
    self.pushRequired = @1;
    [super willChangeValueForKey:@"pushRequired"];
    [super didChangeValueForKey:@"pushRequired"];
}

- (void)willChange:(NSKeyValueChange)changeKind valuesAtIndexes:(NSIndexSet *)indexes forKey:(NSString *)key
{
    [super willChange:changeKind valuesAtIndexes:indexes forKey:key];
    self.dateUpdatedUTC = [NSDate nowUTC];
}

- (NSMutableDictionary *)asDictionary
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    [dictionary setValue:self.dateCreatedUTC forKey:@"dateCreated"];
    [dictionary setValue:self.dateUpdatedUTC forKey:@"dateUpdated"];
    return dictionary;
}

- (void)resetObject
{
    [self.managedObjectContext refreshObject:self mergeChanges:NO];
}

- (NSManagedObject *) clone
{
    NSManagedObject *cloned = [[NSManagedObject alloc]initWithEntity:[self entity]
                                    insertIntoManagedObjectContext:nil];

    //loop through all attributes and assign then to the clone
    NSDictionary *attributes = [[NSEntityDescription
                                 entityForName:[[self entity]name]
                                 inManagedObjectContext:self.managedObjectContext] attributesByName];
    
    [attributes enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        id value = [self valueForKey:key];
        
        if ([key isEqualToString:@"name"])
            value = [NSMutableString stringWithFormat:@"%@-copy",(NSString *)value];

        [cloned setValue:value forKey:key];
    }];
    
    [self.managedObjectContext rollback];
    [self.managedObjectContext insertObject:cloned];
    
    return cloned;
}

@end
