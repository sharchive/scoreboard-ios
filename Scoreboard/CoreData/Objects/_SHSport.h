// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SHSport.h instead.

#import <CoreData/CoreData.h>
#import "_SHEntity.h"

extern const struct SHSportAttributes {
	__unsafe_unretained NSString *editable;
	__unsafe_unretained NSString *gameClockBuzzerEnabled;
	__unsafe_unretained NSString *gameClockContinuous;
	__unsafe_unretained NSString *gameClockCountUp;
	__unsafe_unretained NSString *gameClockEnabled;
	__unsafe_unretained NSString *gameClockTime;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *order;
	__unsafe_unretained NSString *periods;
	__unsafe_unretained NSString *secondaryScoreEnabled;
	__unsafe_unretained NSString *shotClockBuzzerEnabled;
	__unsafe_unretained NSString *shotClockEnabled;
	__unsafe_unretained NSString *shotClockTime;
	__unsafe_unretained NSString *showSettings;
	__unsafe_unretained NSString *userGenerated;
} SHSportAttributes;

extern const struct SHSportRelationships {
	__unsafe_unretained NSString *matches;
	__unsafe_unretained NSString *parent;
	__unsafe_unretained NSString *variations;
} SHSportRelationships;

extern const struct SHSportFetchedProperties {
} SHSportFetchedProperties;

@class Match;
@class SHSport;
@class SHSport;

















@interface SHSportID : NSManagedObjectID {}
@end

@interface _SHSport : _SHEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (SHSportID*)objectID;





@property (nonatomic, strong) NSNumber* editable;



@property BOOL editableValue;
- (BOOL)editableValue;
- (void)setEditableValue:(BOOL)value_;

//- (BOOL)validateEditable:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gameClockBuzzerEnabled;



@property BOOL gameClockBuzzerEnabledValue;
- (BOOL)gameClockBuzzerEnabledValue;
- (void)setGameClockBuzzerEnabledValue:(BOOL)value_;

//- (BOOL)validateGameClockBuzzerEnabled:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gameClockContinuous;



@property BOOL gameClockContinuousValue;
- (BOOL)gameClockContinuousValue;
- (void)setGameClockContinuousValue:(BOOL)value_;

//- (BOOL)validateGameClockContinuous:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gameClockCountUp;



@property BOOL gameClockCountUpValue;
- (BOOL)gameClockCountUpValue;
- (void)setGameClockCountUpValue:(BOOL)value_;

//- (BOOL)validateGameClockCountUp:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gameClockEnabled;



@property BOOL gameClockEnabledValue;
- (BOOL)gameClockEnabledValue;
- (void)setGameClockEnabledValue:(BOOL)value_;

//- (BOOL)validateGameClockEnabled:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gameClockTime;



@property int32_t gameClockTimeValue;
- (int32_t)gameClockTimeValue;
- (void)setGameClockTimeValue:(int32_t)value_;

//- (BOOL)validateGameClockTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* order;



@property int16_t orderValue;
- (int16_t)orderValue;
- (void)setOrderValue:(int16_t)value_;

//- (BOOL)validateOrder:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* periods;



@property int32_t periodsValue;
- (int32_t)periodsValue;
- (void)setPeriodsValue:(int32_t)value_;

//- (BOOL)validatePeriods:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* secondaryScoreEnabled;



@property BOOL secondaryScoreEnabledValue;
- (BOOL)secondaryScoreEnabledValue;
- (void)setSecondaryScoreEnabledValue:(BOOL)value_;

//- (BOOL)validateSecondaryScoreEnabled:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* shotClockBuzzerEnabled;



@property BOOL shotClockBuzzerEnabledValue;
- (BOOL)shotClockBuzzerEnabledValue;
- (void)setShotClockBuzzerEnabledValue:(BOOL)value_;

//- (BOOL)validateShotClockBuzzerEnabled:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* shotClockEnabled;



@property BOOL shotClockEnabledValue;
- (BOOL)shotClockEnabledValue;
- (void)setShotClockEnabledValue:(BOOL)value_;

//- (BOOL)validateShotClockEnabled:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* shotClockTime;



@property int32_t shotClockTimeValue;
- (int32_t)shotClockTimeValue;
- (void)setShotClockTimeValue:(int32_t)value_;

//- (BOOL)validateShotClockTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* showSettings;



@property BOOL showSettingsValue;
- (BOOL)showSettingsValue;
- (void)setShowSettingsValue:(BOOL)value_;

//- (BOOL)validateShowSettings:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* userGenerated;



@property BOOL userGeneratedValue;
- (BOOL)userGeneratedValue;
- (void)setUserGeneratedValue:(BOOL)value_;

//- (BOOL)validateUserGenerated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *matches;

- (NSMutableSet*)matchesSet;




@property (nonatomic, strong) SHSport *parent;

//- (BOOL)validateParent:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *variations;

- (NSMutableSet*)variationsSet;





@end

@interface _SHSport (CoreDataGeneratedAccessors)

- (void)addMatches:(NSSet*)value_;
- (void)removeMatches:(NSSet*)value_;
- (void)addMatchesObject:(Match*)value_;
- (void)removeMatchesObject:(Match*)value_;

- (void)addVariations:(NSSet*)value_;
- (void)removeVariations:(NSSet*)value_;
- (void)addVariationsObject:(SHSport*)value_;
- (void)removeVariationsObject:(SHSport*)value_;

@end

@interface _SHSport (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveEditable;
- (void)setPrimitiveEditable:(NSNumber*)value;

- (BOOL)primitiveEditableValue;
- (void)setPrimitiveEditableValue:(BOOL)value_;




- (NSNumber*)primitiveGameClockBuzzerEnabled;
- (void)setPrimitiveGameClockBuzzerEnabled:(NSNumber*)value;

- (BOOL)primitiveGameClockBuzzerEnabledValue;
- (void)setPrimitiveGameClockBuzzerEnabledValue:(BOOL)value_;




- (NSNumber*)primitiveGameClockContinuous;
- (void)setPrimitiveGameClockContinuous:(NSNumber*)value;

- (BOOL)primitiveGameClockContinuousValue;
- (void)setPrimitiveGameClockContinuousValue:(BOOL)value_;




- (NSNumber*)primitiveGameClockCountUp;
- (void)setPrimitiveGameClockCountUp:(NSNumber*)value;

- (BOOL)primitiveGameClockCountUpValue;
- (void)setPrimitiveGameClockCountUpValue:(BOOL)value_;




- (NSNumber*)primitiveGameClockEnabled;
- (void)setPrimitiveGameClockEnabled:(NSNumber*)value;

- (BOOL)primitiveGameClockEnabledValue;
- (void)setPrimitiveGameClockEnabledValue:(BOOL)value_;




- (NSNumber*)primitiveGameClockTime;
- (void)setPrimitiveGameClockTime:(NSNumber*)value;

- (int32_t)primitiveGameClockTimeValue;
- (void)setPrimitiveGameClockTimeValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitiveOrder;
- (void)setPrimitiveOrder:(NSNumber*)value;

- (int16_t)primitiveOrderValue;
- (void)setPrimitiveOrderValue:(int16_t)value_;




- (NSNumber*)primitivePeriods;
- (void)setPrimitivePeriods:(NSNumber*)value;

- (int32_t)primitivePeriodsValue;
- (void)setPrimitivePeriodsValue:(int32_t)value_;




- (NSNumber*)primitiveSecondaryScoreEnabled;
- (void)setPrimitiveSecondaryScoreEnabled:(NSNumber*)value;

- (BOOL)primitiveSecondaryScoreEnabledValue;
- (void)setPrimitiveSecondaryScoreEnabledValue:(BOOL)value_;




- (NSNumber*)primitiveShotClockBuzzerEnabled;
- (void)setPrimitiveShotClockBuzzerEnabled:(NSNumber*)value;

- (BOOL)primitiveShotClockBuzzerEnabledValue;
- (void)setPrimitiveShotClockBuzzerEnabledValue:(BOOL)value_;




- (NSNumber*)primitiveShotClockEnabled;
- (void)setPrimitiveShotClockEnabled:(NSNumber*)value;

- (BOOL)primitiveShotClockEnabledValue;
- (void)setPrimitiveShotClockEnabledValue:(BOOL)value_;




- (NSNumber*)primitiveShotClockTime;
- (void)setPrimitiveShotClockTime:(NSNumber*)value;

- (int32_t)primitiveShotClockTimeValue;
- (void)setPrimitiveShotClockTimeValue:(int32_t)value_;




- (NSNumber*)primitiveShowSettings;
- (void)setPrimitiveShowSettings:(NSNumber*)value;

- (BOOL)primitiveShowSettingsValue;
- (void)setPrimitiveShowSettingsValue:(BOOL)value_;




- (NSNumber*)primitiveUserGenerated;
- (void)setPrimitiveUserGenerated:(NSNumber*)value;

- (BOOL)primitiveUserGeneratedValue;
- (void)setPrimitiveUserGeneratedValue:(BOOL)value_;





- (NSMutableSet*)primitiveMatches;
- (void)setPrimitiveMatches:(NSMutableSet*)value;



- (SHSport*)primitiveParent;
- (void)setPrimitiveParent:(SHSport*)value;



- (NSMutableSet*)primitiveVariations;
- (void)setPrimitiveVariations:(NSMutableSet*)value;


@end
