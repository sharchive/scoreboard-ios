// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SHSport.m instead.

#import "_SHSport.h"

const struct SHSportAttributes SHSportAttributes = {
	.editable = @"editable",
	.gameClockBuzzerEnabled = @"gameClockBuzzerEnabled",
	.gameClockContinuous = @"gameClockContinuous",
	.gameClockCountUp = @"gameClockCountUp",
	.gameClockEnabled = @"gameClockEnabled",
	.gameClockTime = @"gameClockTime",
	.name = @"name",
	.order = @"order",
	.periods = @"periods",
	.secondaryScoreEnabled = @"secondaryScoreEnabled",
	.shotClockBuzzerEnabled = @"shotClockBuzzerEnabled",
	.shotClockEnabled = @"shotClockEnabled",
	.shotClockTime = @"shotClockTime",
	.showSettings = @"showSettings",
	.userGenerated = @"userGenerated",
};

const struct SHSportRelationships SHSportRelationships = {
	.matches = @"matches",
	.parent = @"parent",
	.variations = @"variations",
};

const struct SHSportFetchedProperties SHSportFetchedProperties = {
};

@implementation SHSportID
@end

@implementation _SHSport

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SHSport" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SHSport";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SHSport" inManagedObjectContext:moc_];
}

- (SHSportID*)objectID {
	return (SHSportID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"editableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"editable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gameClockBuzzerEnabledValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gameClockBuzzerEnabled"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gameClockContinuousValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gameClockContinuous"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gameClockCountUpValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gameClockCountUp"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gameClockEnabledValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gameClockEnabled"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"gameClockTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gameClockTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"orderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"order"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"periodsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"periods"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"secondaryScoreEnabledValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"secondaryScoreEnabled"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"shotClockBuzzerEnabledValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shotClockBuzzerEnabled"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"shotClockEnabledValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shotClockEnabled"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"shotClockTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shotClockTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"showSettingsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"showSettings"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userGeneratedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userGenerated"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic editable;



- (BOOL)editableValue {
	NSNumber *result = [self editable];
	return [result boolValue];
}

- (void)setEditableValue:(BOOL)value_ {
	[self setEditable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveEditableValue {
	NSNumber *result = [self primitiveEditable];
	return [result boolValue];
}

- (void)setPrimitiveEditableValue:(BOOL)value_ {
	[self setPrimitiveEditable:[NSNumber numberWithBool:value_]];
}





@dynamic gameClockBuzzerEnabled;



- (BOOL)gameClockBuzzerEnabledValue {
	NSNumber *result = [self gameClockBuzzerEnabled];
	return [result boolValue];
}

- (void)setGameClockBuzzerEnabledValue:(BOOL)value_ {
	[self setGameClockBuzzerEnabled:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveGameClockBuzzerEnabledValue {
	NSNumber *result = [self primitiveGameClockBuzzerEnabled];
	return [result boolValue];
}

- (void)setPrimitiveGameClockBuzzerEnabledValue:(BOOL)value_ {
	[self setPrimitiveGameClockBuzzerEnabled:[NSNumber numberWithBool:value_]];
}





@dynamic gameClockContinuous;



- (BOOL)gameClockContinuousValue {
	NSNumber *result = [self gameClockContinuous];
	return [result boolValue];
}

- (void)setGameClockContinuousValue:(BOOL)value_ {
	[self setGameClockContinuous:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveGameClockContinuousValue {
	NSNumber *result = [self primitiveGameClockContinuous];
	return [result boolValue];
}

- (void)setPrimitiveGameClockContinuousValue:(BOOL)value_ {
	[self setPrimitiveGameClockContinuous:[NSNumber numberWithBool:value_]];
}





@dynamic gameClockCountUp;



- (BOOL)gameClockCountUpValue {
	NSNumber *result = [self gameClockCountUp];
	return [result boolValue];
}

- (void)setGameClockCountUpValue:(BOOL)value_ {
	[self setGameClockCountUp:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveGameClockCountUpValue {
	NSNumber *result = [self primitiveGameClockCountUp];
	return [result boolValue];
}

- (void)setPrimitiveGameClockCountUpValue:(BOOL)value_ {
	[self setPrimitiveGameClockCountUp:[NSNumber numberWithBool:value_]];
}





@dynamic gameClockEnabled;



- (BOOL)gameClockEnabledValue {
	NSNumber *result = [self gameClockEnabled];
	return [result boolValue];
}

- (void)setGameClockEnabledValue:(BOOL)value_ {
	[self setGameClockEnabled:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveGameClockEnabledValue {
	NSNumber *result = [self primitiveGameClockEnabled];
	return [result boolValue];
}

- (void)setPrimitiveGameClockEnabledValue:(BOOL)value_ {
	[self setPrimitiveGameClockEnabled:[NSNumber numberWithBool:value_]];
}





@dynamic gameClockTime;



- (int32_t)gameClockTimeValue {
	NSNumber *result = [self gameClockTime];
	return [result intValue];
}

- (void)setGameClockTimeValue:(int32_t)value_ {
	[self setGameClockTime:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveGameClockTimeValue {
	NSNumber *result = [self primitiveGameClockTime];
	return [result intValue];
}

- (void)setPrimitiveGameClockTimeValue:(int32_t)value_ {
	[self setPrimitiveGameClockTime:[NSNumber numberWithInt:value_]];
}





@dynamic name;






@dynamic order;



- (int16_t)orderValue {
	NSNumber *result = [self order];
	return [result shortValue];
}

- (void)setOrderValue:(int16_t)value_ {
	[self setOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveOrderValue {
	NSNumber *result = [self primitiveOrder];
	return [result shortValue];
}

- (void)setPrimitiveOrderValue:(int16_t)value_ {
	[self setPrimitiveOrder:[NSNumber numberWithShort:value_]];
}





@dynamic periods;



- (int32_t)periodsValue {
	NSNumber *result = [self periods];
	return [result intValue];
}

- (void)setPeriodsValue:(int32_t)value_ {
	[self setPeriods:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePeriodsValue {
	NSNumber *result = [self primitivePeriods];
	return [result intValue];
}

- (void)setPrimitivePeriodsValue:(int32_t)value_ {
	[self setPrimitivePeriods:[NSNumber numberWithInt:value_]];
}





@dynamic secondaryScoreEnabled;



- (BOOL)secondaryScoreEnabledValue {
	NSNumber *result = [self secondaryScoreEnabled];
	return [result boolValue];
}

- (void)setSecondaryScoreEnabledValue:(BOOL)value_ {
	[self setSecondaryScoreEnabled:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSecondaryScoreEnabledValue {
	NSNumber *result = [self primitiveSecondaryScoreEnabled];
	return [result boolValue];
}

- (void)setPrimitiveSecondaryScoreEnabledValue:(BOOL)value_ {
	[self setPrimitiveSecondaryScoreEnabled:[NSNumber numberWithBool:value_]];
}





@dynamic shotClockBuzzerEnabled;



- (BOOL)shotClockBuzzerEnabledValue {
	NSNumber *result = [self shotClockBuzzerEnabled];
	return [result boolValue];
}

- (void)setShotClockBuzzerEnabledValue:(BOOL)value_ {
	[self setShotClockBuzzerEnabled:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveShotClockBuzzerEnabledValue {
	NSNumber *result = [self primitiveShotClockBuzzerEnabled];
	return [result boolValue];
}

- (void)setPrimitiveShotClockBuzzerEnabledValue:(BOOL)value_ {
	[self setPrimitiveShotClockBuzzerEnabled:[NSNumber numberWithBool:value_]];
}





@dynamic shotClockEnabled;



- (BOOL)shotClockEnabledValue {
	NSNumber *result = [self shotClockEnabled];
	return [result boolValue];
}

- (void)setShotClockEnabledValue:(BOOL)value_ {
	[self setShotClockEnabled:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveShotClockEnabledValue {
	NSNumber *result = [self primitiveShotClockEnabled];
	return [result boolValue];
}

- (void)setPrimitiveShotClockEnabledValue:(BOOL)value_ {
	[self setPrimitiveShotClockEnabled:[NSNumber numberWithBool:value_]];
}





@dynamic shotClockTime;



- (int32_t)shotClockTimeValue {
	NSNumber *result = [self shotClockTime];
	return [result intValue];
}

- (void)setShotClockTimeValue:(int32_t)value_ {
	[self setShotClockTime:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveShotClockTimeValue {
	NSNumber *result = [self primitiveShotClockTime];
	return [result intValue];
}

- (void)setPrimitiveShotClockTimeValue:(int32_t)value_ {
	[self setPrimitiveShotClockTime:[NSNumber numberWithInt:value_]];
}





@dynamic showSettings;



- (BOOL)showSettingsValue {
	NSNumber *result = [self showSettings];
	return [result boolValue];
}

- (void)setShowSettingsValue:(BOOL)value_ {
	[self setShowSettings:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveShowSettingsValue {
	NSNumber *result = [self primitiveShowSettings];
	return [result boolValue];
}

- (void)setPrimitiveShowSettingsValue:(BOOL)value_ {
	[self setPrimitiveShowSettings:[NSNumber numberWithBool:value_]];
}





@dynamic userGenerated;



- (BOOL)userGeneratedValue {
	NSNumber *result = [self userGenerated];
	return [result boolValue];
}

- (void)setUserGeneratedValue:(BOOL)value_ {
	[self setUserGenerated:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUserGeneratedValue {
	NSNumber *result = [self primitiveUserGenerated];
	return [result boolValue];
}

- (void)setPrimitiveUserGeneratedValue:(BOOL)value_ {
	[self setPrimitiveUserGenerated:[NSNumber numberWithBool:value_]];
}





@dynamic matches;

	
- (NSMutableSet*)matchesSet {
	[self willAccessValueForKey:@"matches"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"matches"];
  
	[self didAccessValueForKey:@"matches"];
	return result;
}
	

@dynamic parent;

	

@dynamic variations;

	
- (NSMutableSet*)variationsSet {
	[self willAccessValueForKey:@"variations"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"variations"];
  
	[self didAccessValueForKey:@"variations"];
	return result;
}
	






@end
