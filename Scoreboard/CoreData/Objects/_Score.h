// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Score.h instead.

#import <CoreData/CoreData.h>
#import "_SHEntity.h"

extern const struct ScoreAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *points;
} ScoreAttributes;

extern const struct ScoreRelationships {
} ScoreRelationships;

extern const struct ScoreFetchedProperties {
} ScoreFetchedProperties;





@interface ScoreID : NSManagedObjectID {}
@end

@interface _Score : _SHEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ScoreID*)objectID;





@property (nonatomic, strong) NSDate* date;



//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* points;



@property int16_t pointsValue;
- (int16_t)pointsValue;
- (void)setPointsValue:(int16_t)value_;

//- (BOOL)validatePoints:(id*)value_ error:(NSError**)error_;






@end

@interface _Score (CoreDataGeneratedAccessors)

@end

@interface _Score (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;




- (NSNumber*)primitivePoints;
- (void)setPrimitivePoints:(NSNumber*)value;

- (int16_t)primitivePointsValue;
- (void)setPrimitivePointsValue:(int16_t)value_;




@end
