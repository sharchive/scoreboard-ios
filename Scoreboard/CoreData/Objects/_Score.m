// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Score.m instead.

#import "_Score.h"

const struct ScoreAttributes ScoreAttributes = {
	.date = @"date",
	.points = @"points",
};

const struct ScoreRelationships ScoreRelationships = {
};

const struct ScoreFetchedProperties ScoreFetchedProperties = {
};

@implementation ScoreID
@end

@implementation _Score

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Score" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Score";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Score" inManagedObjectContext:moc_];
}

- (ScoreID*)objectID {
	return (ScoreID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"pointsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"points"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic date;






@dynamic points;



- (int16_t)pointsValue {
	NSNumber *result = [self points];
	return [result shortValue];
}

- (void)setPointsValue:(int16_t)value_ {
	[self setPoints:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePointsValue {
	NSNumber *result = [self primitivePoints];
	return [result shortValue];
}

- (void)setPrimitivePointsValue:(int16_t)value_ {
	[self setPrimitivePoints:[NSNumber numberWithShort:value_]];
}










@end
