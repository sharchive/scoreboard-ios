// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Team.h instead.

#import <CoreData/CoreData.h>
#import "_SHEntity.h"

extern const struct TeamAttributes {
	__unsafe_unretained NSString *appCreated;
	__unsafe_unretained NSString *full_name;
} TeamAttributes;

extern const struct TeamRelationships {
	__unsafe_unretained NSString *colour;
	__unsafe_unretained NSString *teamInstance;
} TeamRelationships;

extern const struct TeamFetchedProperties {
} TeamFetchedProperties;

@class Colour;
@class TeamInstance;




@interface TeamID : NSManagedObjectID {}
@end

@interface _Team : _SHEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TeamID*)objectID;





@property (nonatomic, strong) NSNumber* appCreated;



@property BOOL appCreatedValue;
- (BOOL)appCreatedValue;
- (void)setAppCreatedValue:(BOOL)value_;

//- (BOOL)validateAppCreated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* full_name;



//- (BOOL)validateFull_name:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Colour *colour;

//- (BOOL)validateColour:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) TeamInstance *teamInstance;

//- (BOOL)validateTeamInstance:(id*)value_ error:(NSError**)error_;





@end

@interface _Team (CoreDataGeneratedAccessors)

@end

@interface _Team (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveAppCreated;
- (void)setPrimitiveAppCreated:(NSNumber*)value;

- (BOOL)primitiveAppCreatedValue;
- (void)setPrimitiveAppCreatedValue:(BOOL)value_;




- (NSString*)primitiveFull_name;
- (void)setPrimitiveFull_name:(NSString*)value;





- (Colour*)primitiveColour;
- (void)setPrimitiveColour:(Colour*)value;



- (TeamInstance*)primitiveTeamInstance;
- (void)setPrimitiveTeamInstance:(TeamInstance*)value;


@end
