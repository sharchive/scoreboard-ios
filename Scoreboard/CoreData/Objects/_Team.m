// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Team.m instead.

#import "_Team.h"

const struct TeamAttributes TeamAttributes = {
	.appCreated = @"appCreated",
	.full_name = @"full_name",
};

const struct TeamRelationships TeamRelationships = {
	.colour = @"colour",
	.teamInstance = @"teamInstance",
};

const struct TeamFetchedProperties TeamFetchedProperties = {
};

@implementation TeamID
@end

@implementation _Team

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Team" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Team";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Team" inManagedObjectContext:moc_];
}

- (TeamID*)objectID {
	return (TeamID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"appCreatedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"appCreated"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic appCreated;



- (BOOL)appCreatedValue {
	NSNumber *result = [self appCreated];
	return [result boolValue];
}

- (void)setAppCreatedValue:(BOOL)value_ {
	[self setAppCreated:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAppCreatedValue {
	NSNumber *result = [self primitiveAppCreated];
	return [result boolValue];
}

- (void)setPrimitiveAppCreatedValue:(BOOL)value_ {
	[self setPrimitiveAppCreated:[NSNumber numberWithBool:value_]];
}





@dynamic full_name;






@dynamic colour;

	

@dynamic teamInstance;

	






@end
