// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TeamInstance.h instead.

#import <CoreData/CoreData.h>
#import "_SHEntity.h"

extern const struct TeamInstanceAttributes {
	__unsafe_unretained NSString *score;
	__unsafe_unretained NSString *secondaryScore;
} TeamInstanceAttributes;

extern const struct TeamInstanceRelationships {
	__unsafe_unretained NSString *assignedColour;
	__unsafe_unretained NSString *match;
	__unsafe_unretained NSString *team;
} TeamInstanceRelationships;

extern const struct TeamInstanceFetchedProperties {
} TeamInstanceFetchedProperties;

@class Colour;
@class Match;
@class Team;




@interface TeamInstanceID : NSManagedObjectID {}
@end

@interface _TeamInstance : _SHEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TeamInstanceID*)objectID;





@property (nonatomic, strong) NSNumber* score;



@property int32_t scoreValue;
- (int32_t)scoreValue;
- (void)setScoreValue:(int32_t)value_;

//- (BOOL)validateScore:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* secondaryScore;



@property int32_t secondaryScoreValue;
- (int32_t)secondaryScoreValue;
- (void)setSecondaryScoreValue:(int32_t)value_;

//- (BOOL)validateSecondaryScore:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Colour *assignedColour;

//- (BOOL)validateAssignedColour:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Match *match;

//- (BOOL)validateMatch:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Team *team;

//- (BOOL)validateTeam:(id*)value_ error:(NSError**)error_;





@end

@interface _TeamInstance (CoreDataGeneratedAccessors)

@end

@interface _TeamInstance (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveScore;
- (void)setPrimitiveScore:(NSNumber*)value;

- (int32_t)primitiveScoreValue;
- (void)setPrimitiveScoreValue:(int32_t)value_;




- (NSNumber*)primitiveSecondaryScore;
- (void)setPrimitiveSecondaryScore:(NSNumber*)value;

- (int32_t)primitiveSecondaryScoreValue;
- (void)setPrimitiveSecondaryScoreValue:(int32_t)value_;





- (Colour*)primitiveAssignedColour;
- (void)setPrimitiveAssignedColour:(Colour*)value;



- (Match*)primitiveMatch;
- (void)setPrimitiveMatch:(Match*)value;



- (Team*)primitiveTeam;
- (void)setPrimitiveTeam:(Team*)value;


@end
