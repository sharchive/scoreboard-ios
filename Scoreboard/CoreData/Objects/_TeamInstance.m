// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TeamInstance.m instead.

#import "_TeamInstance.h"

const struct TeamInstanceAttributes TeamInstanceAttributes = {
	.score = @"score",
	.secondaryScore = @"secondaryScore",
};

const struct TeamInstanceRelationships TeamInstanceRelationships = {
	.assignedColour = @"assignedColour",
	.match = @"match",
	.team = @"team",
};

const struct TeamInstanceFetchedProperties TeamInstanceFetchedProperties = {
};

@implementation TeamInstanceID
@end

@implementation _TeamInstance

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TeamInstance" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TeamInstance";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TeamInstance" inManagedObjectContext:moc_];
}

- (TeamInstanceID*)objectID {
	return (TeamInstanceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"scoreValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"score"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"secondaryScoreValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"secondaryScore"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic score;



- (int32_t)scoreValue {
	NSNumber *result = [self score];
	return [result intValue];
}

- (void)setScoreValue:(int32_t)value_ {
	[self setScore:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveScoreValue {
	NSNumber *result = [self primitiveScore];
	return [result intValue];
}

- (void)setPrimitiveScoreValue:(int32_t)value_ {
	[self setPrimitiveScore:[NSNumber numberWithInt:value_]];
}





@dynamic secondaryScore;



- (int32_t)secondaryScoreValue {
	NSNumber *result = [self secondaryScore];
	return [result intValue];
}

- (void)setSecondaryScoreValue:(int32_t)value_ {
	[self setSecondaryScore:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSecondaryScoreValue {
	NSNumber *result = [self primitiveSecondaryScore];
	return [result intValue];
}

- (void)setPrimitiveSecondaryScoreValue:(int32_t)value_ {
	[self setPrimitiveSecondaryScore:[NSNumber numberWithInt:value_]];
}





@dynamic assignedColour;

	

@dynamic match;

	

@dynamic team;

	






@end
