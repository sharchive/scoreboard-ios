// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to _SHEntity.h instead.

#import <CoreData/CoreData.h>


extern const struct _SHEntityAttributes {
	__unsafe_unretained NSString *dateCreatedUTC;
	__unsafe_unretained NSString *dateUpdatedUTC;
	__unsafe_unretained NSString *guid;
	__unsafe_unretained NSString *pushRequired;
	__unsafe_unretained NSString *softDeleted;
} _SHEntityAttributes;

extern const struct _SHEntityRelationships {
} _SHEntityRelationships;

extern const struct _SHEntityFetchedProperties {
} _SHEntityFetchedProperties;








@interface _SHEntityID : NSManagedObjectID {}
@end

@interface __SHEntity : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (_SHEntityID*)objectID;





@property (nonatomic, strong) NSDate* dateCreatedUTC;



//- (BOOL)validateDateCreatedUTC:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* dateUpdatedUTC;



//- (BOOL)validateDateUpdatedUTC:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* guid;



//- (BOOL)validateGuid:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* pushRequired;



@property BOOL pushRequiredValue;
- (BOOL)pushRequiredValue;
- (void)setPushRequiredValue:(BOOL)value_;

//- (BOOL)validatePushRequired:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* softDeleted;



@property BOOL softDeletedValue;
- (BOOL)softDeletedValue;
- (void)setSoftDeletedValue:(BOOL)value_;

//- (BOOL)validateSoftDeleted:(id*)value_ error:(NSError**)error_;






@end

@interface __SHEntity (CoreDataGeneratedAccessors)

@end

@interface __SHEntity (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveDateCreatedUTC;
- (void)setPrimitiveDateCreatedUTC:(NSDate*)value;




- (NSDate*)primitiveDateUpdatedUTC;
- (void)setPrimitiveDateUpdatedUTC:(NSDate*)value;




- (NSString*)primitiveGuid;
- (void)setPrimitiveGuid:(NSString*)value;




- (NSNumber*)primitivePushRequired;
- (void)setPrimitivePushRequired:(NSNumber*)value;

- (BOOL)primitivePushRequiredValue;
- (void)setPrimitivePushRequiredValue:(BOOL)value_;




- (NSNumber*)primitiveSoftDeleted;
- (void)setPrimitiveSoftDeleted:(NSNumber*)value;

- (BOOL)primitiveSoftDeletedValue;
- (void)setPrimitiveSoftDeletedValue:(BOOL)value_;




@end
