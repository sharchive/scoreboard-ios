// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to _SHEntity.m instead.

#import "__SHEntity.h"

const struct _SHEntityAttributes _SHEntityAttributes = {
	.dateCreatedUTC = @"dateCreatedUTC",
	.dateUpdatedUTC = @"dateUpdatedUTC",
	.guid = @"guid",
	.pushRequired = @"pushRequired",
	.softDeleted = @"softDeleted",
};

const struct _SHEntityRelationships _SHEntityRelationships = {
};

const struct _SHEntityFetchedProperties _SHEntityFetchedProperties = {
};

@implementation _SHEntityID
@end

@implementation __SHEntity

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"_SHEntity" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"_SHEntity";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"_SHEntity" inManagedObjectContext:moc_];
}

- (_SHEntityID*)objectID {
	return (_SHEntityID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"pushRequiredValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"pushRequired"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"softDeletedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"softDeleted"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic dateCreatedUTC;






@dynamic dateUpdatedUTC;






@dynamic guid;






@dynamic pushRequired;



- (BOOL)pushRequiredValue {
	NSNumber *result = [self pushRequired];
	return [result boolValue];
}

- (void)setPushRequiredValue:(BOOL)value_ {
	[self setPushRequired:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePushRequiredValue {
	NSNumber *result = [self primitivePushRequired];
	return [result boolValue];
}

- (void)setPrimitivePushRequiredValue:(BOOL)value_ {
	[self setPrimitivePushRequired:[NSNumber numberWithBool:value_]];
}





@dynamic softDeleted;



- (BOOL)softDeletedValue {
	NSNumber *result = [self softDeleted];
	return [result boolValue];
}

- (void)setSoftDeletedValue:(BOOL)value_ {
	[self setSoftDeleted:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSoftDeletedValue {
	NSNumber *result = [self primitiveSoftDeleted];
	return [result boolValue];
}

- (void)setPrimitiveSoftDeletedValue:(BOOL)value_ {
	[self setPrimitiveSoftDeleted:[NSNumber numberWithBool:value_]];
}










@end
