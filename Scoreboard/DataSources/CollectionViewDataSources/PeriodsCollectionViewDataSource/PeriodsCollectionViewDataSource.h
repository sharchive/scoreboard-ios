//
//  PeriodsCollectionViewDataSource.h
//  Scoreboard
//
//  Created by Tom Bates on 22/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHCollectionViewDataSource.h"

@class GameManager;

@interface PeriodsCollectionViewDataSource : SHCollectionViewDataSource

@property (strong, nonatomic) GameManager *gameManager;

@property (assign, nonatomic) NSInteger numberOfPeriods;
@property (assign, nonatomic) NSInteger currentPeriod;

@end
