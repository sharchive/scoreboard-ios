//
//  PeriodsCollectionViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 22/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "PeriodsCollectionViewDataSource.h"
#import "PeriodsCollectionViewCell.h"
#import "GameManager.h"

@implementation PeriodsCollectionViewDataSource

- (UICollectionViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    PeriodsCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"PeriodsCollectionViewCell" forIndexPath:indexPath];
    
    [cell setOn:(indexPath.row < self.currentPeriod)];
    return cell;
}

- (NSInteger)numberOfSections
{
    return 1;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    return self.numberOfPeriods;
}

- (void)setGameManager:(GameManager *)gameManager
{
    _gameManager = gameManager;
    
    [self.gameManager addObserver:self
                       forKeyPath:@"gamePeriods"
                          options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                          context:nil];
    
    [self.gameManager addObserver:self
                       forKeyPath:@"currentPeriod"
                          options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                          context:nil];
}

- (void)setCurrentPeriod:(NSInteger)currentPeriod
{
    if (currentPeriod <= 1) currentPeriod = 1;
    _currentPeriod = currentPeriod;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"gamePeriods"]) self.numberOfPeriods = [self.gameManager.gamePeriods intValue];
    else self.currentPeriod = self.gameManager.currentPeriod;
    [self reload];
}

@end
