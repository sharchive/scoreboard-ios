//
//  UserNotificationCollectionViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 03/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "UserNotificationCollectionViewDataSource.h"
#import "UserNotificationCollectionViewCell.h"
#import "UserNotification.h"
#import "SHViewController.h"

@implementation UserNotificationCollectionViewDataSource

- (UICollectionViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    UserNotificationCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"UserNotificationCollectionViewCell" forIndexPath:indexPath];
    UserNotification *notification = (UserNotification *)item;
    cell.userNotification = notification;
    return cell;
}

- (void)removeItem:(id)item
{
    if ([self.items count] == 1)
    {
        [UIView animateWithDuration:0.32 animations:^{
            self.viewController.view.alpha = 0;
        }completion:^(BOOL finished) {
            [self.viewController.view removeFromSuperview];
            self.viewController.view.alpha = 1;
        }];
    }
    [super removeItem:item];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserNotification *notification = (UserNotification *)[self itemAtIndexPath:indexPath];
    NSDictionary *attributes = @{NSFontAttributeName: [AppearanceManager standardFont]};
    CGSize fontSize = [notification.message sizeWithAttributes:attributes];
    return CGSizeMake(fontSize.width + 60, 30);
}

@end
