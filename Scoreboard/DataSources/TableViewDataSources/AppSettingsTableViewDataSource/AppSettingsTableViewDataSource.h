//
//  AppSettingsTableViewDataSource.h
//  Scoreboard
//
//  Created by Tom Bates on 27/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SHTableViewDataSource.h"

@interface AppSettingsTableViewDataSource : SHTableViewDataSource

@end
