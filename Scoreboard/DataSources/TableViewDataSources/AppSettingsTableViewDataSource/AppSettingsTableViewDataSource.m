//
//  AppSettingsTableViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 27/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "AppSettingsTableViewDataSource.h"
#import "AppSettingsTableViewCell.h"
#import "SHApp.h"

@implementation AppSettingsTableViewDataSource

- (UITableViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    SHApp *app = (SHApp *)item;
    
    AppSettingsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"AppSettingsTableViewCell"];
    cell.titleLabel.text = [app.name uppercaseString];
    
    return cell;
}

@end
