//
//  GenericScoringSettingsTableViewDataSource.h
//  Scoreboard
//
//  Created by Tom Bates on 16/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHTableViewDataSource.h"

@class Match;

@interface GenericScoringSettingsTableViewDataSource : SHTableViewDataSource

@property (strong, nonatomic) Match *game;
@property (assign, nonatomic) SEL exitGame;
@property (assign, nonatomic) SEL saveExitGame;
@property (assign, nonatomic) SEL shareGame;

@end
