//
//  GenericScoringSettingsTableViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 16/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "GenericScoringSettingsTableViewDataSource.h"
#import "ScoringShareTableViewCell.h"
#import "Match.h"

@implementation GenericScoringSettingsTableViewDataSource

static NSString * kExitCellIdentifier = @"ExitCell";
static NSString * kSaveExitCellIdentifier = @"SaveExitCell";
static NSString * kShareCellIdentifier = @"ShareCell";

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.items = [NSMutableArray arrayWithObjects:kExitCellIdentifier, kShareCellIdentifier, nil];
    }
    return self;
}

- (UITableViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    NSString *identifier = (NSString *)item;
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
//    if ([cell isKindOfClass:[ScoringShareTableViewCell class]])
//    {
//        [[((ScoringShareTableViewCell *)cell)postedButton] setHidden:![[self.game gameShared] boolValue]];
//    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = (NSString *)self.items[indexPath.row];
    int height = 44;
    if (SB_DEVICE_IS_IPAD) height = 50;
    if ([identifier isEqualToString:kShareCellIdentifier])
    {
        if (SB_DEVICE_IS_IPAD) height = 80;
        else height = 65;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SEL selctor;
    if ([[[tableView cellForRowAtIndexPath:indexPath]reuseIdentifier] isEqualToString:kExitCellIdentifier])
    {
        selctor = self.exitGame;
    }else if ([[[tableView cellForRowAtIndexPath:indexPath]reuseIdentifier] isEqualToString:kSaveExitCellIdentifier])
    {
        selctor = self.saveExitGame;
    }else
    {
        selctor = self.shareGame;
    }
    [self perform:selctor];
}

@end
