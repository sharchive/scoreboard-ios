//
//  SettingsTableViewDataSource.h
//  Scoreboard
//
//  Created by Tom Bates on 11/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHTableViewDataSource.h"
#import "SHViewModel.h"

@interface SettingsTableViewDataSource : SHTableViewDataSource


@end
