//
//  SettingsTableViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 11/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SettingsTableViewDataSource.h"
#import "SettingsTableViewCell.h"
#import "SHViewModel.h"

@interface SettingsTableViewDataSource ()

@end

@implementation SettingsTableViewDataSource

static NSString * kSettingsIdentifier = @"SettingsTableViewCell";
static NSString * kSettingsInfoIdentifier = @"SettingsInfoTableViewCell";

- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    return [super numberOfRowsInSection:section] + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.items count]) return 30;
    else return 44;
}

- (UITableViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    SHTableViewCell *cell;
    if (indexPath.row == [self.items count])
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:kSettingsInfoIdentifier];
    }
    else
    {
        SettingsTableViewCell *settingsCell = [self.tableView dequeueReusableCellWithIdentifier:kSettingsIdentifier];
        SHViewModel *viewModel = (SHViewModel *)item;
        settingsCell.titleLabel.text = viewModel.title;
        cell = settingsCell;
    }
    
    return cell;
}

@end
