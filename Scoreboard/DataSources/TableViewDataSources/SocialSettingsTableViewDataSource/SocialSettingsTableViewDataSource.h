//
//  SettingsSocialTableViewDataSource.h
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHTableViewDataSource.h"

@interface SocialSettingsTableViewDataSource : SHTableViewDataSource

@end
