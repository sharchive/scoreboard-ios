//
//  SettingsSocialTableViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SocialSettingsTableViewDataSource.h"
#import "SBSocialManager.h"
#import "SocialSettingsTableViewCell.h"

@implementation SocialSettingsTableViewDataSource

static NSString * kCellIdentifier = @"SocialSettingsTableViewCell";

- (UITableViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    SocialSettingsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    SBSocialManager *manager = (SBSocialManager *)item;
    cell.socialManager = manager;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

@end
