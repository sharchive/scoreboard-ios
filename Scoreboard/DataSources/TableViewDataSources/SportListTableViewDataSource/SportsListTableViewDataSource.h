//
//  SportListTableViewDataSource.h
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHTableViewDataSource.h"

@class SHSport;

@interface SportsListTableViewDataSource : SHTableViewDataSource

@property (strong, nonatomic) SHSport *selectedSport;

@end
