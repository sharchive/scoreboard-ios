//
//  SportListTableViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportsListTableViewDataSource.h"
#import "SportsListTableViewCell.h"
#import "SHSport.h"
#import "SBConstants.h"
#import "SportService.h"

@interface SportsListTableViewDataSource ()

@property (strong, nonatomic) SportService *sportService;

@end

@implementation SportsListTableViewDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sportService = [SportService new];
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath withItem:(id)item
{
    SportsListTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SportsListTableViewCell"];
    cell.sport = (SHSport *)item;
    cell.dataSource = self;
    cell.backgroundView = nil;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)insertItem:(id)item
{
    [super insertItem:item];
    NSInteger row = [self.items indexOfObject:item];
    NSIndexPath *idx = [NSIndexPath indexPathForRow:row inSection:0];
    [self selectIndexPath:idx];
    [self editSportAtIndex:idx];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SHSport *sport = self.items[indexPath.row];
    if (sport.editableValue == true) {
        return UITableViewCellEditingStyleDelete;
    } else
    {
        return UITableViewCellEditingStyleNone;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (editingStyle) {
        case UITableViewCellEditingStyleDelete:
            [self deleteSportAtIndex:indexPath];
            break;
            
        default:
            break;
    }
}

- (void)deleteSportAtIndex:(NSIndexPath *)indexPath
{
    SHSport *sport = [self itemAtIndexPath:indexPath];
    [self removeItemAtIndex:indexPath];
    [self selectTopItem];
    [self.sportService deleteSport:sport];

}

- (void)editSportAtIndex:(NSIndexPath *)indexPath
{
    SportsListTableViewCell *cell = (SportsListTableViewCell*) [self.tableView cellForRowAtIndexPath:indexPath];
    [cell beginEditing];
}

- (void)setItems:(NSMutableArray *)items
{
    [super setItems:items];
    [self selectIndexPath:[NSIndexPath indexPathForRow:0
                                             inSection:0]];
}

- (void)selectTopItem
{
    if ([self.items count] > 0)
    {
        [self selectIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
}

- (SHSport *)selectedSport
{
    return (SHSport *)self.selectedItem;
}

@end
