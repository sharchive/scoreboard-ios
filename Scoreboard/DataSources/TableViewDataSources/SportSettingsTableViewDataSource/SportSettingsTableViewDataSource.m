//
//  SportSettingsTableViewDataSource.m
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsTableViewDataSource.h"
#import "SportSettingsTableViewCell.h"
#import "SHSport.h"
#import "SBConstants.h"

@implementation SportSettingsTableViewDataSource

- (instancetype)initWithTableView:(UITableView *)tableView
{
    self = [super initWithTableView:tableView];
    if (self) {
        NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"DefaultCells" ofType:@"json"];
        NSData *jsonData = [NSData dataWithContentsOfFile:plistPath];
        NSArray *cellArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        self.items = [cellArray mutableCopy];
        self.madeChanges = false;
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.items count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary * header = self.items[section];
    NSArray * subcells = header[@"cells"];
    NSInteger cells = [subcells count] + 1;
    if ([header[@"identifier"] isEqualToString:@"SportSettingsSwitchTableViewCell"])
    {
        NSString *propertykey = header[@"property"];
        NSNumber *property = [self.selectedSport valueForKeyPath:propertykey];
        BOOL switched = [property boolValue];
        if (switched == false)
        {
            cells = 1;
        }
    }
    return cells;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item = [self itemAtIndexPath:indexPath];
    SportSettingsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:item[@"identifier"]];
    cell.cellItem = item;
    cell.sport = self.selectedSport;
    cell.tableView = self.tableView;
    cell.indexPath = indexPath;
    cell.backgroundView = nil;
    cell.backgroundColor = [UIColor clearColor];
    cell.userInteractionEnabled = self.selectedSport.editableValue;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self heightForIndex:indexPath];
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary * returnDict = self.items[indexPath.section];
    if (indexPath.row != 0)
    {
        NSArray * subcells = returnDict[@"cells"];
        
        if ([subcells count] > 0) returnDict = subcells[indexPath.row - 1];
    }
    return returnDict;
}

- (NSInteger)heightForIndex:(NSIndexPath *)indexPath
{
    NSDictionary *item = [self itemAtIndexPath:indexPath];
    NSString *identifier = item[@"identifier"];
    NSInteger height = 44;
    if ([identifier isEqualToString:@"SportSettingsPeriodsTableViewCell"])
    {
        if (SB_DEVICE_IS_IPAD) height = 75;
        else height = 75;
    }
    else if ([identifier isEqualToString:@"SportSettingsTimeTableViewCell"])
    {
        if (SB_DEVICE_IS_IPAD) height = 75;
        else height = 57;
    }
    else if ([identifier isEqualToString:@"SportSettingsSwitchTableViewCell"])
    {
        if (SB_DEVICE_IS_IPAD) height = 75;
        else height = 44;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView endEditing:true];
}

- (void)setSelectedItem:(id)selectedItem
{
    
    [self.selectedSport removeObserver:self forKeyPath:@"dateUpdatedUTC"];
    [super setSelectedItem:selectedItem];
    [self.selectedSport addObserver:self
                         forKeyPath:@"dateUpdatedUTC"
                            options:NSKeyValueObservingOptionNew
                            context:nil];
    [self reload];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.madeChanges = [self.selectedSport hasChanges];
}

- (SHSport *)selectedSport
{
    return (SHSport *)self.selectedItem;
}

@end
