//
//  ContentNavigationController.h
//  Scoreboard
//
//  Created by Tom Bates on 24/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHNavigationController.h"

@interface ContentNavigationController : SHNavigationController

@end
