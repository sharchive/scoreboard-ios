//
//  ContentNavigationController.m
//  Scoreboard
//
//  Created by Tom Bates on 24/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "ContentNavigationController.h"

@interface ContentNavigationController ()

@end

@implementation ContentNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)handleShowViewController:(UIViewController *)controller animated:(BOOL)animated completion:(void (^)(void))completion
{
   NSMutableArray *vcs = [self.viewControllers mutableCopy];
   if (![vcs containsObject:controller])
   {
       [vcs insertObject:controller atIndex:0];
       self.viewControllers = vcs;
       [self popToRootViewControllerAnimated:false];
   }
}

@end
