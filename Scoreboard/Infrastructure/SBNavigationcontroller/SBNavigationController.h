//
//  SBNavigationController.h
//  Scoreboard
//
//  Created by Tom Bates on 05/12/2013.
//  Copyright (c) 2013 Sport Search UK. All rights reserved.
//

#import "SHNavigationController.h"

@interface SBNavigationController : SHNavigationController <UINavigationControllerDelegate>


@end
