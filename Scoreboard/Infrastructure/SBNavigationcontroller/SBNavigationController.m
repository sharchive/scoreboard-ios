//
//  SBNavigationController.m
//  Scoreboard
//
//  Created by Tom Bates on 05/12/2013.
//  Copyright (c) 2013 Sport Search UK. All rights reserved.
//

#import "SBNavigationController.h"

@interface SBNavigationController ()

@end

@implementation SBNavigationController

-(BOOL)shouldAutorotate
{
    return UIInterfaceOrientationMaskLandscape;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

@end
