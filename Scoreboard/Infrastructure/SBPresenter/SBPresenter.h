//
//  SBPresenter.h
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHPresenter.h"


typedef enum {
    PresentationStyleNormal,
    PresentationStyleModal,
    PresentationStyleModalNav,
    PresentationStyleBase,
    PresentationStyleMenu,
    PresentationStyleContent
}PresentationStyle;

@class ContentNavigationController;
@class SHViewModel;
@class UserNotificationManager;
@class ExternalScreenManager;
@class UserNotification;

@interface SBPresenter : SHPresenter

@property (assign, nonatomic) PresentationStyle presentationStyle;
@property (strong, nonatomic) UserNotificationManager *userMessageManager;
@property (strong, nonatomic) ExternalScreenManager *externalScreenManager;
@property (strong, nonatomic) ContentNavigationController *contentNavigationController;

- (void)showUserNotificationObject:(UserNotification *)messageObject;
- (void)showUserNotification: (NSString *)message;

@end
