//
//  SBPresenter.m
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBPresenter.h"
#import "SBNavigationController.h"
#import "ContentNavigationController.h"
#import "SHViewModel.h"
#import "SHViewController.h"
#import "SHModalViewController.h"
#import "UserNotificationManager.h"
#import "ExternalScreenManager.h"


@implementation SBPresenter

- (id)init
{
    self = [super init];
    if (self) {
        self.presentationStyle = PresentationStyleNormal;
        self.userMessageManager = [UserNotificationManager new];
        self.externalScreenManager = [ExternalScreenManager new];
        self.externalScreenManager.presenter = self;
    }
    return self;
}

- (void)setBaseNavigationController:(SHNavigationController *)baseNavigationControllerer
{
    [super setBaseNavigationController:baseNavigationControllerer];
    self.userMessageManager.navigaitonController = baseNavigationControllerer;
}

- (void)setContentNavigationController:(ContentNavigationController *)contentNavigationController
{
    _contentNavigationController = contentNavigationController;
    _contentNavigationController.presenter = self;
}

- (void)showUserNotificationObject:(UserNotification *)messageObject
{
    [self.userMessageManager showUserMessageObject:messageObject];
}

- (void)showUserNotification:(NSString *)message
{
    [self.userMessageManager showUserMessage:message];
}

- (void)showViewController:(UIViewController *)controller sender:(SHViewModel *)sender animated:(BOOL)animated completion:(void (^)(void))completion
{
    if (self.presentationStyle == PresentationStyleBase)
    {
        SHViewController *SHController = (SHViewController *)controller;
        [self.baseNavigationController showViewController:controller animated:animated completion:completion];
        [self.externalScreenManager presentExternalScreenWithName:NSStringFromClass([SHController class]) andViewModel:SHController.viewModel];
    }
    else if (self.presentationStyle == PresentationStyleMenu || self.presentationStyle == PresentationStyleContent)
    {
        [self.contentNavigationController showViewController:controller animated:animated completion:completion];
    }
    else if (self.presentationStyle == PresentationStyleModalNav)
    {
        SBNavigationController *navController = [[SBNavigationController alloc]initWithRootViewController:controller];
        navController.modalPresentationStyle = controller.modalPresentationStyle;
        [self.baseNavigationController handleShowModalViewController:navController animated:true completion:completion];
    }
    else
    {
        SHNavigationController *senderNav = (SHNavigationController *)sender.viewController.navigationController;
        [senderNav showViewController:controller animated:animated completion:completion];
    }
    
    self.presentationStyle = PresentationStyleNormal;
}

- (void)closeViewController:(UIViewController *)controller animated:(BOOL)animated completion:(void (^)(void))completion
{
    [self.externalScreenManager showHoldingScreen];
    [super closeViewController:controller animated:animated completion:completion];
}

@end
