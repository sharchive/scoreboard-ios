//
//  AppearanceManager.h
//  Scoreboard
//
//  Created by Tom Bates on 31/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SynthesizeSingleton.h"

@interface AppearanceManager : NSObject
SINGLETON_INTERFACE_FOR(AppearanceManager)

@property (strong, nonatomic)UIImage *blackButtonImageNormal;
@property (strong, nonatomic)UIImage *blackButtonImageHighlighted;

+ (UIFont *)smallFont;
+ (UIFont *)standardFont;
+ (UIFont *)mediumFont;
+ (UIFont *)largeFont;

+ (UIFont *)externalSmallFont;
+ (UIFont *)externalStandardFont;
+ (UIFont *)externalMediumFont;
+ (UIFont *)externalLargeFont;

+ (UIColor *)aquaBlueColour;
+ (UIColor *)swithOnBlueColour;

+ (NSString *)mainFontName;

+ (UIImage *)settingsLabelImage;

+ (void)setAppearance;
- (void)setButtons;

@end
