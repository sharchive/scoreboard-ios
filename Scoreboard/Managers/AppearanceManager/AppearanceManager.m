//
//  AppearanceManager.m
//  Scoreboard
//
//  Created by Tom Bates on 31/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "AppearanceManager.h"
#import "SportsListViewController.h"
#import "SBTableViewCell.h"

@implementation AppearanceManager
SINGLETON_IMPLEMENTATION_FOR(AppearanceManager)

+ (void)setAppearance
{
    AppearanceManager *manager = [AppearanceManager instance];
    [manager setButtons];
    [manager setNavBar];
    [manager setLabels];
    [manager setSwitches];
}

- (void)setButtons
{
    [[UIButton appearance]setTitleEdgeInsets:UIEdgeInsetsMake(3, 0, 0, 0)];
}

- (void)setSwitches
{
    [[UISwitch appearance]setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1]];
    [[UISwitch appearance]setTintColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1]];
    [[UISwitch appearance]setOnTintColor:[AppearanceManager swithOnBlueColour]];
    [[UISwitch appearance]setThumbTintColor:[AppearanceManager aquaBlueColour]];
    [[[UISwitch appearance]layer]setCornerRadius:16];
}

- (void)setLabels
{
   [[UILabel appearanceWhenContainedIn:[SBTableViewCell class], nil]setFont:[AppearanceManager standardFont]];
}

- (void)setNavBar
{
    UIImage *navBarImage = [UIImage imageNamed:@"nav_bg"];
    if (navBarImage) [[UINavigationBar appearance]setBackgroundImage:navBarImage forBarMetrics:UIBarMetricsDefault];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setValue:[AppearanceManager standardFont] forKey:NSFontAttributeName];
    [[UINavigationBar appearance]setTitleTextAttributes:attributes];
    [[UINavigationBar appearance]setTintColor:[UIColor blackColor]];
}

- (UIImage *)blackButtonImageNormal
{
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 16, 0, 16);
    if (SB_DEVICE_IS_IPAD) insets = UIEdgeInsetsMake(0, 24, 0, 24);;
    return [[UIImage imageNamed:@"black_button_bg_normal"]resizableImageWithCapInsets:insets];

}

- (UIImage *)blackButtonImageHighlighted
{
    return [[UIImage imageNamed:@"black_button_bg_highlighted"]resizableImageWithCapInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
    
}

+ (UIColor *)aquaBlueColour
{
    return [UIColor colorWithRed:0 green:1 blue:252.0/255.0 alpha:1];
}

+ (UIColor *)swithOnBlueColour
{
    return [UIColor colorWithRed:0 green:194.0/255.0 blue:215.0/255.0 alpha:1];
}

+ (UIFont *)smallFont
{
    float size = 15;
    if (SB_DEVICE_IS_IPAD) size = 18;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (UIFont *)standardFont
{
    float size = 20;
    if (SB_DEVICE_IS_IPAD) size = 25;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (UIFont *)mediumFont
{
    float size = 22;
    if (SB_DEVICE_IS_IPAD) size = 28;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (UIFont *)largeFont
{
    float size = 40;
    if (SB_DEVICE_IS_IPAD) size = 65;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (UIFont *)externalSmallFont
{
    float size = 18;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (UIFont *)externalStandardFont
{
    float size = 25;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (UIFont *)externalMediumFont
{
    float size = 28;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (UIFont *)externalLargeFont
{
    float size = 65;
    return [UIFont fontWithName:[AppearanceManager mainFontName] size:size];
}

+ (NSString *)mainFontName
{
    return @"AkzidenzGroteskBE-Cn";
}

+ (UIImage *)settingsLabelImage
{
    return [UIImage imageNamed:@"settings_label_bg"];
}

@end
