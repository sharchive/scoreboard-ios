//
//  ABAudioManager.h
//  Scoreboard
//
//  Created by Tom Bates on 22/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SynthesizeSingleton.h"

@interface SBAudioManager : NSObject
SINGLETON_INTERFACE_FOR(SBAudioManager);

@property (strong, nonatomic) NSString *gameBuzzerAudio;
@property (strong, nonatomic) NSString *shotBuzzerAudio;

+ (void)playGameBuzzer;
+ (void)playShotBuzzer;

@end
