//
//  ABAudioManager.m
//  Scoreboard
//
//  Created by Tom Bates on 22/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SBAudioManager.h"
#import <AVFoundation/AVFoundation.h>

@interface SBAudioManager ()

@property (strong, nonatomic) AVAudioPlayer *gameBuzzerPlayer;
@property (strong, nonatomic) AVAudioPlayer *shotBuzzerPlayer;

@end

@implementation SBAudioManager
SINGLETON_IMPLEMENTATION_FOR(SBAudioManager);

+ (void)playGameBuzzer
{
    [[[SBAudioManager instance]gameBuzzerPlayer]play];
}

+ (void)playShotBuzzer
{
    [[[SBAudioManager instance]shotBuzzerPlayer]play];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createPlayer];
    }
    return self;
}

- (void)createPlayer
{
    NSError *error;
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource: @"horn" ofType: @"wav"];

    self.gameBuzzerPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:soundFilePath] error: &error];
    [self.gameBuzzerPlayer prepareToPlay];
    
    soundFilePath = [[NSBundle mainBundle] pathForResource: @"buzzer" ofType: @"wav"];
    
    self.shotBuzzerPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:soundFilePath] error: &error];
    [self.shotBuzzerPlayer prepareToPlay];
}

- (void)setGameBuzzerAudio:(NSString *)gameBuzzerAudio
{
    _gameBuzzerAudio = gameBuzzerAudio;
    [self createPlayer];
}

- (void)setShotBuzzerAudio:(NSString *)shotBuzzerAudio
{
    _shotBuzzerAudio = shotBuzzerAudio;
    [self createPlayer];
}

@end
