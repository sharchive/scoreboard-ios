//
//  ExternalScreenManager.h
//  Scoreboard
//
//  Created by Tom Bates on 13/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SBPresenter;
@class SHViewModel;

@interface ExternalScreenManager : NSObject

@property (strong, nonatomic) SBPresenter *presenter;

- (void)showHoldingScreen;
- (void)presentExternalScreenWithName:(NSString *)name andViewModel:(SHViewModel *)viewModel;

@end
