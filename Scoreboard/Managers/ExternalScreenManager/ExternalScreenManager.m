//
//  ExternalScreenManager.m
//  Scoreboard
//
//  Created by Tom Bates on 13/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "ExternalScreenManager.h"
#import "UserNotification.h"
#import "SBPresenter.h"
#import "SHViewController.h"
#import "UserMessageBuilder.h"

@interface ExternalScreenManager ()

@property (strong, nonatomic) UIWindow *externalWindow;
@property (strong, nonatomic) UIStoryboard *storyboard;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) UIViewController *holdingViewController;

@end

@implementation ExternalScreenManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        if ([[UIScreen screens]count]>1) [self screenConnected];
        [self setNotifications];
    }
    return self;
}

- (void)setNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenConnected) name:UIScreenDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenDisconnected) name:UIScreenDidDisconnectNotification object:nil];
}

-(void)prepareScreen:(UIScreen *)connectedScreen
{
    if (!connectedScreen) connectedScreen =[[UIScreen screens] lastObject];
    
    [connectedScreen setOverscanCompensation:UIScreenOverscanCompensationInsetApplicationFrame];
    
    CGRect frame = connectedScreen.bounds;
    self.externalWindow = [[UIWindow alloc]initWithFrame:frame];
    self.externalWindow.rootViewController = self.navigationController;
    [self.externalWindow setBounds:connectedScreen.bounds];
    [self.externalWindow setScreen:connectedScreen];
    
    self.externalWindow.hidden = NO;
}

-(void)screenConnected
{
    if (!self.externalWindow) self.externalWindow = [[UIWindow alloc]init];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.externalWindow setAlpha:0];
                     }completion:^(BOOL finished) {
                         [self prepareScreen:[[UIScreen screens] lastObject]];
                         [UIView animateWithDuration:0.3
                                          animations:^{
                                              [self.externalWindow setAlpha:1];
                                          }];
                         [self sendConnectedUserMessage];
                     }];
    
}

- (void)screenDisconnected
{
    [self.externalWindow setHidden:YES];
    [self setExternalWindow:nil];
    [self sendDisconnectedUserMessage];
}

- (void)sendConnectedUserMessage
{
    UserNotification *notification = [UserMessageBuilder messageWithBlock:^(UserMessageBuilder *builder) {
        builder.message = @"External screen connected";
        builder.icon = [UIImage imageNamed:@"screen_connected_message_icon"];
    }];
    [self.presenter showUserNotificationObject:notification];
}

- (void)sendDisconnectedUserMessage
{
    UserNotification *notification = [UserMessageBuilder messageWithBlock:^(UserMessageBuilder *builder) {
        builder.message = @"External screen disconnected";
        builder.icon = [UIImage imageNamed:@"screen_disconnected_message_icon"];
    }];
    [self.presenter showUserNotificationObject:notification];
}

- (void)showHoldingScreen
{
    NSArray *controllers = [NSArray arrayWithObject:self.holdingViewController];
    self.navigationController.viewControllers = controllers;
}

- (void)presentExternalScreenWithName:(NSString *)name andViewModel:(SHViewModel *)viewModel
{
    SHViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:name];
    viewController.viewModel = viewModel;
    NSArray *controllers = [NSArray arrayWithObject:viewController];
    self.navigationController.viewControllers = controllers;
}

- (UINavigationController *)navigationController
{
    if (!_navigationController)
    {
        _navigationController = [[UINavigationController alloc]initWithRootViewController:self.holdingViewController];
        [_navigationController setNavigationBarHidden:true];
    }
    return _navigationController;
}

- (UIViewController *)holdingViewController
{
    if (!_holdingViewController)
    {
        _holdingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HoldingViewController"];
        UIImage *image = [UIImage imageNamed:@"dark_stripe_bg"];
        [_holdingViewController.view setBackgroundColor:[UIColor colorWithPatternImage:image]];
    }
    return _holdingViewController;
}

- (UIStoryboard *)storyboard
{
    if (!_storyboard)
    {
        _storyboard = [UIStoryboard storyboardWithName:@"External" bundle:[NSBundle mainBundle]];
    }
    return _storyboard;
}

@end
