//
//  FacebookManager.h
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBSocialManager.h"

@class FacebookServiceClient;

@interface FacebookManager : SBSocialManager

@property (strong, nonatomic) FacebookServiceClient *facebookServiceClient;

- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

@end
