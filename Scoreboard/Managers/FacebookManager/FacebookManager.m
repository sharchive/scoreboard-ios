//
//  FacebookManager.m
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "FacebookManager.h"
#import "FacebookServiceClient.h"

@implementation FacebookManager

- (id)init
{
    self = [super init];
    if (self)
    {
        self.title = @"Facebook";
        self.facebookServiceClient = [FacebookServiceClient new];
        [self extendSession];
    }
    return self;
}

- (void)extendSession
{
    [self.facebookServiceClient requestFacebookExtensionWithUpdates:^(SocialConnectionStatus status, NSError *error) {
        self.currentStatus = status;
    }];
}

- (void)login
{
    [self.facebookServiceClient requestFacebookLoginWithUpdates:^(SocialConnectionStatus status, NSError *error) {
        self.currentStatus = status;
    }];
}

- (void)logout
{
    [self.facebookServiceClient requestFacebookSessionLogout];
    self.currentStatus = self.facebookServiceClient.currentFacebookStatus;
}

- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
{
    bool result = [self.facebookServiceClient handleOpenURL:url sourceApplication:sourceApplication];
    
    return result;
}

- (void)shareGameURL:(NSString *)URLString completion:(void (^)(BOOL, NSError *))complete
{
    [self.facebookServiceClient requestShareGameLinkWithURL:URLString completion:^(BOOL success, NSError *error) {
        complete (success, error);
    }];
}

@end
