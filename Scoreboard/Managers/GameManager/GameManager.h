//
//  GameManager.h
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Match;
@class GameTimeManager;
@class SHSport;

@interface GameManager : NSObject

@property (strong, nonatomic) GameTimeManager *timeManager;
@property (strong, nonatomic) Match *game;
@property (strong, nonatomic) SHSport *sport;

@property (strong, nonatomic) NSOrderedSet *teamGames;
@property (assign, nonatomic) NSNumber *gamePeriods;
@property (assign, nonatomic) NSInteger currentPeriod;
@property (assign, nonatomic) NSString *externalURL;


- (void)completeGame;
- (void)saveGame;
- (void)deleteGame;
- (void)removeGameChanges;

- (void)updateGameScoresWithCompletion:(void (^)(BOOL success, NSError *error))complete;
- (void)getURLForGameWithCompletion:(void (^)(NSString *gameURL, NSError *error)) complete;
- (void)refreshGameViewersWithCompletion:(void (^)(BOOL success, NSNumber *viewers, NSNumber *refreshRate))complete;
- (void)updateTeamNames;

- (NSFetchedResultsController *)fetchAllGames;


@end
