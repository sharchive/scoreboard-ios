//
//  GameManager.m
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "GameManager.h"
#import "SSGameUpdateServiceClient.h"
#import "MatchService.h"
#import "TeamService.h"
#import "TeamInstanceService.h"
#import "Match.h"
#import "Team.h"
#import "SHSport.h"
#import "TeamInstance.h"
#import "ColourService.h"
#import "GameTimeManager.h"
#import "NSNumber+TimeAdditions.h"

@interface GameManager ()

@property (strong, nonatomic) SSGameUpdateServiceClient *gameServiceClient;
@property (strong, nonatomic) MatchService *gameService;
@property (strong, nonatomic) TeamInstanceService *teamGameService;

@end

@implementation GameManager

- (id)init
{
    self = [super init];
    if (self)
    {
        self.timeManager = [GameTimeManager new];
        self.gameServiceClient = [SSGameUpdateServiceClient new];
        self.gameService = [MatchService new];
        self.teamGameService = [TeamInstanceService new];
        [self createGame];
    }
    return self;
}

- (void)createGame
{
    self.game = [self.gameService createNewGame];
    [self.game addTeamsObject:self.teamGameService.homeTeamGame];
    [self.game addTeamsObject:self.teamGameService.awayTeamGame];
    self.currentPeriod = 1;
}

- (void)completeGame
{
    self.game.inProgressValue = false;
    if ([self.game hasBeenShared]) [self.gameServiceClient requestGameEndWithID:self.game.guid andTime:@1000 completion:nil];
}

- (void)removeGameChanges
{
    [self.gameService undoAllChanges];
}

- (void)saveGame
{
    [self.gameService save:self.game];
}

- (void)deleteGame
{
    [self.gameService deleteGame:self.game];
}

- (void)getURLForGameWithCompletion:(void (^)(NSString *gameURL, NSError *error)) complete
{
    TeamInstance *team1 = self.game.teams[0];
    TeamInstance *team2 = self.game.teams[1];
    
    [self.gameServiceClient requestGameCreateWithTeam1:team1 team2:team2 andSport:self.game.sport completion:^(BOOL success, NSString *gameID, NSString *gameURL, NSError *error) {
        self.game.guid = gameID;
        complete(gameURL, error);
        NSLog(@"Game url -- %@",gameURL);
    }];
}

- (void)updateGameScoresWithCompletion:(void (^)(BOOL success, NSError *error))complete
{
    TeamInstance *team1 = self.game.teams[0];
    TeamInstance *team2 = self.game.teams[1];
    NSNumber *gameTime = [self currentGameTimeInSeconds];
    NSString *periodString = [NSString stringWithFormat:@"%ld/%@",(long)self.currentPeriod,self.gamePeriods];
    [self.gameServiceClient requestGameUpdateWithID:self.game.guid team1Score:team1.score team2Score:team2.score time:gameTime period:periodString completion:complete];
}

- (void)refreshGameViewersWithCompletion:(void (^)(BOOL success, NSNumber *viewers, NSNumber *refreshRate))complete
{
    [self.gameServiceClient requestGameViewersWithID:self.game.guid completion:complete];
}

- (void)updateTeamNames
{
    TeamInstance *team1 = self.game.teams[0];
    TeamInstance *team2 = self.game.teams[1];
    [self.gameServiceClient requestGameNameUpdateWithID:self.game.guid team1:team1 team2:team2 completion:nil];
}

- (NSString *)externalURL
{
    return self.game.externalURL;
}

- (NSNumber *)gamePeriods
{
    return self.game.sport.periods;
}

- (NSFetchedResultsController *)fetchAllGames
{
    return [self.gameService fetchAllGames];
}

- (NSOrderedSet *)teamGames
{
    return self.game.teams;
}

- (SHSport *)sport
{
    return self.game.sport;
}

- (void)setSport:(SHSport *)sport
{
    self.game.sport = sport;
    self.timeManager.sport = sport;
    [self.timeManager resetTimes];
}

- (NSNumber *)currentGameTimeInSeconds
{
    NSInteger elapsedPreviousPeriodTime = self.timeManager.gameClockTime * [self currentPeriod];
    NSNumber *currentGameTime =[ NSNumber numberWithInteger:self.timeManager.elapsedPeriodTime + elapsedPreviousPeriodTime];
    
    return [currentGameTime asMilliToSeconds];
}

- (NSInteger)completedPeriods
{
    return self.currentPeriod - 1;
}

- (void)setCurrentPeriod:(NSInteger)currentPeriod
{
    if (currentPeriod < 1)
    {
        _currentPeriod = 1;
    }
    else if (currentPeriod > self.sport.periodsValue)
    {
        _currentPeriod = self.sport.periodsValue;
    }
    else
    {
        _currentPeriod = currentPeriod;
    }
}

@end
