//
//  GameTimeManager.h
//  Scoreboard
//
//  Created by Tom Bates on 09/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHSport;

@interface GameTimeManager : NSObject

@property (strong, nonatomic) SHSport *sport;

@property (assign, nonatomic) NSInteger gameClockTime;
@property (assign, nonatomic) NSInteger elapsedPeriodTime;
@property (assign, nonatomic) NSInteger remainingPeriodTime;
@property (assign, nonatomic) BOOL periodCountup;

@property (assign, nonatomic) NSInteger shotClockTime;
@property (assign, nonatomic) NSInteger elapsedShotTime;
@property (assign, nonatomic) NSInteger remainingShotTime;
@property (assign, nonatomic) BOOL secondaryCountup;

- (void)startStopTimer;
- (void)resetTimes;
- (void)resetGameTime;
- (void)resetShotTime;

@end
