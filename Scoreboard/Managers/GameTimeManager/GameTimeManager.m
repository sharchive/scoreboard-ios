//
//  GameTimeManager.m
//  Scoreboard
//
//  Created by Tom Bates on 09/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "GameTimeManager.h"
#import "SHSport.h"
#import "SBAudioManager.h"

@interface GameTimeManager ()

@property (strong, nonatomic) NSTimer *gameTimer;

@end

@implementation GameTimeManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        [SBAudioManager instance];
    }
    return self;
}

- (void)startStopTimer
{
    if ([self.gameTimer isValid]) [self stopGameTimer];
    else [self startGameTimer];
}

- (void)startGameTimer
{
    self.gameTimer = [NSTimer timerWithTimeInterval:1.0/100.0
                                             target:self
                                           selector:@selector(timerFired)
                                           userInfo:nil
                                            repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.gameTimer forMode:NSDefaultRunLoopMode];
}

- (void)stopGameTimer
{
    [self.gameTimer invalidate];
}

- (void)timerFired
{
    if (self.sport.gameClockEnabledValue)
    {
        self.elapsedPeriodTime ++;
        self.remainingPeriodTime --;
        
        if (self.remainingPeriodTime == 0)
        {
            [self.gameTimer invalidate];
            if (self.sport.gameClockBuzzerEnabledValue)[SBAudioManager playGameBuzzer];
        }
    }
    
    if (self.sport.shotClockEnabledValue)
    {
        if (self.remainingShotTime > 0)
        {
            self.elapsedShotTime ++;
            self.remainingShotTime --;
        }
        
        if (self.remainingShotTime == 0)
        {
            if (self.shotClockTime != -1 && self.sport.shotClockBuzzerEnabledValue) [SBAudioManager playShotBuzzer];
            self.remainingShotTime = -1;
        }
    }
}

- (void)resetTimes
{
    [self stopGameTimer];
    [self resetGameTime];
    [self resetShotTime];
}

- (void)resetGameTime
{
    self.elapsedPeriodTime = 0;
    self.remainingPeriodTime = self.gameClockTime;
}

- (void)resetShotTime
{
    self.elapsedShotTime = 0;
    self.remainingShotTime = self.shotClockTime;
}

- (NSInteger)gameClockTime
{
    return self.sport.gameClockTimeValue;
}

- (BOOL)periodCountup
{
    return self.sport.gameClockCountUpValue;
}

- (NSInteger)shotClockTime
{
    return self.sport.shotClockTimeValue;
}


@end
