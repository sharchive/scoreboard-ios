//
//  SBInitialLaunchManager.h
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InitialLaunchManager : NSObject

- (BOOL)initialLaunchSequenceRequired;
- (void)performInitialLaunchTasks;

@end
