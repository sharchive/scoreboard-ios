//
//  SBInitialLaunchManager.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "InitialLaunchManager.h"
#import "SportService.h"
#import "AppService.h"

@interface InitialLaunchManager ()

@property (strong, nonatomic) SportService *sportService;
@property (strong, nonatomic) AppService *appService;

@end

@implementation InitialLaunchManager

- (id)init
{
    self = [super init];
    if (self) {
        self.sportService = [SportService new];
        self.appService = [AppService new];
    }
    return self;
}

- (BOOL)initialLaunchSequenceRequired
{
    NSInteger sportCount = [[self.sportService allSports] count];
    return sportCount == 0;
}

-(void)performInitialLaunchTasks
{
    [self loadSports];
    [self loadApps];
}

-(BOOL) initialLaunchSequenceDone
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *version = infoDictionary[(NSString*)kCFBundleVersionKey];
    NSString *versionKey = [NSString stringWithFormat:@"InitialLaunch-%@",version];
    
    return [[[NSUserDefaults standardUserDefaults]valueForKey:versionKey]boolValue];
}

- (BOOL)saveInitialLaunchDoneForCurrentVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *version = infoDictionary[(NSString*)kCFBundleVersionKey];
    NSString *versionKey = [NSString stringWithFormat:@"InitialLaunch-%@",version];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSNumber numberWithBool:TRUE] forKey:versionKey];
    return [defaults synchronize];
}

- (void)loadSports
{
    NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"Sports" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:plistPath];
    NSArray *sportArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    
    [self.sportService createSportsFromArray:sportArray];
    [self.sportService save];
}

- (void)loadApps
{
    NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"Apps" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:plistPath];
    NSArray *appsArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    [self.appService createAppsFromArray:appsArray];
    [self.appService save];
}

@end
