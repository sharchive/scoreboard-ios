//
//  SSLoginManager.h
//  Scoreboard
//
//  Created by Tom Bates on 03/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSLoginManager : NSObject

- (void)loginUserWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete;

- (void)registerUserWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete;

- (void)logOutCurrentUser;

@end
