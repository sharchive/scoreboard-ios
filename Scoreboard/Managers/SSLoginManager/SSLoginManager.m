//
//  SSLoginManager.m
//  Scoreboard
//
//  Created by Tom Bates on 03/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SSLoginManager.h"
#import "SSLoginServiceClient.h"

@interface SSLoginManager ()

@property (strong, nonatomic) SSLoginServiceClient *loginClient;

@end

@implementation SSLoginManager

- (id)init
{
    self = [super init];
    if (self) {
        self.loginClient = [SSLoginServiceClient new];
    }
    return self;
}

- (void)loginUserWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete
{
    [self.loginClient requestUserLoginWithEmail:email andPassword:password completion:^(BOOL success, NSString *messageKey, NSError *error) {
        complete (success, messageKey, error);
    }];
}

- (void)registerUserWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete
{
    [self.loginClient requestUserRegistrationWithEmail:email andPassword:password completion:^(BOOL success, NSString *messageKey, NSError *error) {
        
        if (success)
        {
            [self loginUserWithEmail:email andPassword:password completion:complete];
        }else
        {
                //Handle Error
        }
        
    }];
}

- (void)logOutCurrentUser
{
    [self.loginClient requestUserSignOut];
}

@end
