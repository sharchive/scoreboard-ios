//
//  SocialManager.h
//  Scoreboard
//
//  Created by Tom Bates on 06/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SynthesizeSingleton.h"
#import "FacebookManager.h"

@interface SocialManager : NSObject
SINGLETON_INTERFACE_FOR(SocialManager)

@property (strong, nonatomic) NSArray *socialClients;

@property (strong, nonatomic) FacebookManager *facebookManager;
@property (assign, nonatomic) SocialConnectionStatus facebookStatus;

- (void)loginToFacebook;
- (void)logoutOfFacebook;

- (void)shareGameURL:(NSString *)URLString completion:(void (^)(BOOL success, NSError *error))complete;

@end
