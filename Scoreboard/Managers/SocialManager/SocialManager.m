//
//  SocialManager.m
//  Scoreboard
//
//  Created by Tom Bates on 06/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SocialManager.h"

@implementation SocialManager
SINGLETON_IMPLEMENTATION_FOR(SocialManager)

- (id)init
{
    self = [super init];
    if (self) {
        self.facebookManager = [FacebookManager new];
        self.socialClients = [NSArray arrayWithObject:self.facebookManager];
    }
    return self;
}

- (void)loginToFacebook
{
    [self.facebookManager login];
}

- (void)logoutOfFacebook
{
    [self.facebookManager logout];
}

- (SocialConnectionStatus)facebookStatus
{
    return self.facebookManager.currentStatus;
}

- (void)shareGameURL:(NSString *)URLString completion:(void (^)(BOOL, NSError *))complete
{
    [self.facebookManager shareGameURL:URLString completion:^(BOOL success, NSError *error) {
        complete (success, error);
    }];
}

@end
