//
//  SportManager.h
//  Scoreboard
//
//  Created by Tom Bates on 23/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SportService;

@interface SportManager : NSObject

@property (strong, nonatomic) SportService *sportService;

- (void)rollbackChanges;
- (NSFetchedResultsController *)fetchAllSportsOrderedByName;

@end
