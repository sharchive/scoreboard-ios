//
//  SportManager.m
//  Scoreboard
//
//  Created by Tom Bates on 23/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportManager.h"
#import "SportService.h"

@implementation SportManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sportService = [SportService new];
    }
    return self;
}

- (NSFetchedResultsController *)fetchAllSportsOrderedByName
{
    NSFetchedResultsController *controller = [self.sportService fetchAllSportsOrderdBy:@"name" groupedBy:@"category"];
//    if ([controller.fetchedObjects count] == 0) [self loadInitialSports];
    return controller;
}

-(void)loadInitialSports
{
    NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"Sports" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:plistPath];
    NSArray *sportArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    
    [self.sportService createSportsFromArray:sportArray];
    [self.sportService save];
}

- (void)rollbackChanges
{
    [self.sportService rollbackChanges];
}

@end
