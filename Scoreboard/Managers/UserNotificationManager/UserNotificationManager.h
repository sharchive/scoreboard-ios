//
//  UserMessageManager.h
//  Scoreboard
//
//  Created by Tom Bates on 17/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHNavigationController;
@class UserNotification;

@interface UserNotificationManager : NSObject

@property (strong, nonatomic) SHNavigationController *navigaitonController;

- (void)showUserMessage: (NSString *)message;
- (void)showUserMessageObject:(UserNotification *)messageObject;

@end
