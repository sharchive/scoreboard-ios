//
//  UserMessageManager.m
//  Scoreboard
//
//  Created by Tom Bates on 17/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "UserNotificationManager.h"
#import "SBNavigationController.h"
#import "UserNotificationViewModel.h"
#import "UserNotification.h"


@interface UserNotificationManager ()

@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) UserNotificationViewModel *notificaitonViewModel;

@end

@implementation UserNotificationManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.notificaitonViewModel = [UserNotificationViewModel new];
    }
    return self;
}

- (void)showUserMessage:(NSString *)message
{
    UserNotification *messageObject = [UserNotification new];
    messageObject.message = message;
    [self showUserMessageObject:messageObject];
}

- (void)showUserMessageObject:(UserNotification *)messageObject
{
    if (self.notificaitonViewModel.view.superview == nil)
    {
        [self.navigaitonController.view addSubview:self.notificaitonViewModel.view];
    }
    [self.notificaitonViewModel showMessage:messageObject];
}

@end
