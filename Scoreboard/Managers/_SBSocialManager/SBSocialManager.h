//
//  SBSocialManager.h
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBSocialManager : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) RACCommand *loginCommand;
@property (assign, nonatomic) SocialConnectionStatus currentStatus;


- (void)login;
- (void)logout;

- (void)shareGameURL:(NSString *)URLString completion:(void (^)(BOOL success, NSError *error))complete;

@end
