//
//  SBSocialManager.m
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBSocialManager.h"

@implementation SBSocialManager

- (id)init
{
    self = [super init];
    if (self) {
        self.currentStatus = SCSDisconnected;
    }
    return self;
}

- (void)login {}

- (void)logout {}

- (void)shareGameURL:(NSString *)URLString completion:(void (^)(BOOL, NSError *))complete {}

@end
