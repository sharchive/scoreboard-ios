//
//  SportSettingItem.h
//  Scoreboard
//
//  Created by Tom Bates on 23/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SportSettingItem : NSObject

@property (strong, nonatomic)NSString *identifier;
@property (strong, nonatomic)NSString *property;
@property (strong, nonatomic)NSString *title;
@property (strong, nonatomic)NSArray *subItems;

@end
