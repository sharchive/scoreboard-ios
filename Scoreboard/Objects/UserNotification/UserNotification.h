//
//  UserMessage.h
//  Scoreboard
//
//  Created by Tom Bates on 17/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserNotification : NSObject

@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) NSDate *date;

@end
