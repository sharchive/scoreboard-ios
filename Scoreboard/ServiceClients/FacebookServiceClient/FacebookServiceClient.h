//
//  FacebookServiceClient.h
//  Scoreboard
//
//  Created by Tom Bates on 08/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacebookServiceClient : NSObject

- (void)requestFacebookExtensionWithUpdates:(void (^)(SocialConnectionStatus status, NSError *error))update;
- (void)requestFacebookLoginWithUpdates:(void (^)(SocialConnectionStatus status, NSError *error))update;
- (void)requestFacebookSessionLogout;
- (SocialConnectionStatus)currentFacebookStatus;

- (void)requestShareGameLinkWithURL:(NSString *)URLstring completion:(void (^)(BOOL success, NSError *error))complete;
//- (void)requestShareGameGraphWithURL:(NSString *)URLString completion:(void (^)(BOOL success, NSError *error))complete;

- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

@end
