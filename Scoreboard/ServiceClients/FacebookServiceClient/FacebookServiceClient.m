//
//  FacebookServiceClient.m
//  Scoreboard
//
//  Created by Tom Bates on 08/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "FacebookServiceClient.h"
#import <FacebookSDK/FacebookSDK.h>

static NSString *kBasicInfo = @"basic_info";
static NSString *kPublishPermisisons = @"publish_actions";


@implementation FacebookServiceClient

- (void)requestFacebookExtensionWithUpdates:(void (^)(SocialConnectionStatus status, NSError *error))update
{
    [FBAppCall handleDidBecomeActive];
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        [FBSession.activeSession openWithCompletionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
            if (update) update([self statusFromFB:state],error);
            if (error) NSLog(@"%@",error);
        }];
    }
    else
    {
        return update ([self currentFacebookStatus], nil);
    }
}

- (void)requestFacebookLoginWithUpdates:(void (^)(SocialConnectionStatus status, NSError *error))update
{
    [self requestFacebookSessionLogout];
    [FBSession openActiveSessionWithReadPermissions:@[kBasicInfo]
                                       allowLoginUI:true
                                  completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                      SocialConnectionStatus SBState = [self statusFromFB:state];
                                      if (update) update(SBState,error);
                                      if (error) NSLog(@"%@",error);
                                  }];
}

- (SocialConnectionStatus)currentFacebookStatus
{
   return [self statusFromFB:FBSession.activeSession.state];
}

- (void)requestFacebookSessionLogout
{
    [FBSession.activeSession closeAndClearTokenInformation];
}

- (void)requestShareGameGraphWithURL:(NSString *)URLString
{
    id<FBGraphObject> gameObject =
    [FBGraphObject openGraphObjectForPostWithType:@"ssuk_scoreboard:game"
                                            title:@"Game Title"
                                            image:@"http://a1.mzstatic.com/us/r30/Purple/v4/47/a8/9e/47a89e65-a5e1-170e-9679-9acd256b37a2/temp..rcdhjwcv.175x175-75.jpg"
                                              url:URLString
                                      description:@"Tom created a new game"];
    
    id<FBOpenGraphAction> createAction = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
    [createAction setObject:gameObject forKey:@"game"];
    
    [FBDialogs presentShareDialogWithOpenGraphAction:createAction
                                          actionType:@"ssuk_scoreboard:create"
                                 previewPropertyName:@"game"
                                             handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                                 if(error) {
                                                     NSLog(@"There was a graph error -- %@",error);
                                                 } else {
                                                     NSLog(@"Graph post successful");
                                                 }
                                             }];
    
    //    NSMutableDictionary<FBOpenGraphObject> *object = [FBGraphObject openGraphObjectForPost];
    //    object.provisionedForPost = YES;
    //    object[@"title"] = @"Game Title";
    //    object[@"description"] = @"Tom created a new game";
    //    object[@"url"] = @"http://example.com/roasted_pumpkin_seeds";
    //    object[@"image"] = @[@{@"url": @"http://a1.mzstatic.com/us/r30/Purple/v4/47/a8/9e/47a89e65-a5e1-170e-9679-9acd256b37a2/temp..rcdhjwcv.175x175-75.jpg", @"user_generated" : @"false" }];
    //    object[@"type"] = @"ssuk_scoreboard:game";
    //
    //    [FBRequestConnection startForPostOpenGraphObject:object completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
    //        if (error == nil)
    //        {
    //            NSLog(@"Graph post successful");
    //        }else
    //        {
    //            NSLog(@"There was a graph error -- %@",error);
    //        }
    //    }];
}

- (void)requestShareGameLinkWithURL:(NSString *)URLstring completion:(void (^)(BOOL, NSError *))complete
{
        [self requestAditionalPermissionsIfRequired:kPublishPermisisons complete:^(bool granted) {
            if  (!granted)
            {
                NSDictionary *userinfo = @{@"Message":@"Not granted permissions"};
                NSError *error = [[NSError alloc]initWithDomain:nil code:400 userInfo:userinfo];
                complete (false, error);
                return ;
            }
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           @"Scoreboard", @"name",
                                           @"New game started", @"caption",
                                           @"Tom started a new game with the scoreboard app. Click to follow his progress", @"description",
                                           URLstring, @"link",
                                           @"http://a1.mzstatic.com/us/r30/Purple/v4/47/a8/9e/47a89e65-a5e1-170e-9679-9acd256b37a2/temp..rcdhjwcv.175x175-75.jpg", @"picture",
                                           nil];
            
            [FBRequestConnection startWithGraphPath:@"/me/feed"
                                         parameters:params
                                         HTTPMethod:@"POST"
                                  completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                      complete (true, error);
                                  }];
        }];
}

- (void)requestAditionalPermissionsIfRequired:(NSString *)newPermission complete:(void (^)(bool granted))complete
{
    if ([self permissionsArePresent:newPermission])
    {
        complete (true);
    }
    else
    {
        __block id weakSelf = self;
       NSArray *newPermissions = [NSArray arrayWithObject:newPermission];
        [FBSession.activeSession requestNewPublishPermissions:newPermissions
                                              defaultAudience:FBSessionDefaultAudienceEveryone
                                            completionHandler:^(FBSession *session, NSError *error) {

                                                complete ([weakSelf permissionsArePresent:newPermission]);
                                            }];
    }
}

- (BOOL)permissionsArePresent:(NSString *)permission
{
    return [FBSession.activeSession.permissions indexOfObject:permission] != NSNotFound;
}

- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
{
   return [FBAppCall handleOpenURL:url
           sourceApplication:sourceApplication
             fallbackHandler:^(FBAppCall *call) {
                 NSLog(@"Unhandled deep link: %@", url);
             }];
}

- (SocialConnectionStatus)statusFromFB:(FBSessionState)state
{
    switch (state) {
            
        case FBSessionStateOpen:
            return SCSConnected;
            
        case FBSessionStateOpenTokenExtended:
            return SCSConnected;
            
        case FBSessionStateCreated:
            return SCSDisconnected;
            
        case FBSessionStateCreatedTokenLoaded:
            return SCSDisconnected;
            
        case FBSessionStateClosedLoginFailed:
            return SCSDisconnected;
            
        case FBSessionStateClosed:
            return SCSDisconnected;
            
        default:
            return SCSDisconnected;
            break;
    }
}

@end
