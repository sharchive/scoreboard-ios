//
//  SSGameUpdateServiceClient.h
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBServiceClient.h"

@class TeamInstance;
@class SHSport;

@interface SSGameUpdateServiceClient : SBServiceClient

- (void)requestGameCreateWithTeam1:(TeamInstance *)team1 team2:(TeamInstance *)team2 andSport:(SHSport *)sport completion:(void (^)(BOOL success, NSString * gameID, NSString * gameURL, NSError *error))complete;
- (void)requestGameUpdateWithID:(NSString *)identity team1Score:(NSNumber *)team1 team2Score:(NSNumber *)team2 time:(NSNumber *)time period:(NSString *)period completion:(void (^)(BOOL success, NSError *error))complete;
- (void)requestGameViewersWithID:(NSString *)identity completion:(void (^)(BOOL success, NSNumber *viewers, NSNumber *refreshRate))complete;
- (void)requestGameEndWithID:(NSString *)identity andTime:(NSNumber *)endTime completion:(void (^)(BOOL success))complete;
- (void)requestGameNameUpdateWithID:(NSString *)identity team1:(TeamInstance *)team1 team2:(TeamInstance *)team2 completion:(void (^)(BOOL success))complete;

@end
