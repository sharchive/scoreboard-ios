//
//  SSGameUpdateServiceClient.m
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SSGameUpdateServiceClient.h"
#import "SHRequest.h"
#import "TeamInstance.h"
#import "Team.h"
#import "Colour.h"
#import "SHSport.h"

@implementation SSGameUpdateServiceClient


- (void)requestGameCreateWithTeam1:(TeamInstance *)team1 team2:(TeamInstance *)team2 andSport:(SHSport *)sport completion:(void (^)(BOOL success, NSString * gameID, NSString * gameURL, NSError *error))complete
{
    NSData *jsonData = [self jsonDataForTeam1:team1 team2:team2 andSport:sport];
    
    NSMutableURLRequest *request =  [SHRequest POSTRequestWithExtension:@"scoreboard" andBody:jsonData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:self.opQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        BOOL success = false;
        NSString *gameURL;
        NSString *gameID;
        if (data) {
            success = true;
            NSDictionary *resultDictionary = [self resultJSONForResponseData:data];
            gameURL = resultDictionary[@"url"];
            gameID = resultDictionary[@"id"];
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            complete (success, gameID, gameURL, connectionError);
        });

    }];
}

- (void)requestGameUpdateWithID:(NSString *)identity team1Score:(NSNumber *)team1 team2Score:(NSNumber *)team2 time:(NSNumber *)time period:(NSString *)period completion:(void (^)(BOOL success, NSError *error))complete
{
    NSData *jsonData = [self jsonDataForTeam1Score:team1 andTeam2Score:team2 time:time period:period];
    NSString *requestURL = [NSString stringWithFormat:@"scoreboard/%@/score",identity];
    NSMutableURLRequest *request =  [SHRequest POSTRequestWithExtension:requestURL andBody:jsonData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:self.opQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        
        BOOL posted = false;
        if (httpResponse.statusCode == 202)
        {
            posted = true;
        }
        if (complete) complete (posted, connectionError);
    }];
}

- (void)requestGameViewersWithID:(NSString *)identity completion:(void (^)(BOOL success, NSNumber *viewers, NSNumber *refreshRate))complete
{
    NSString *requestURL = [NSString stringWithFormat:@"scoreboard/%@/viewers",identity];
    NSMutableURLRequest *request =  [SHRequest GETRequestWithExtension:requestURL];
    [NSURLConnection sendAsynchronousRequest:request queue:self.opQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        
            if (data && httpResponse.statusCode == 200)
            {
                NSDictionary *resultDictionary = [self resultJSONForResponseData:data];
                NSNumber *viewers = resultDictionary[@"viewers"];
                NSNumber *rate = resultDictionary[@"refreshRate"];
                complete (true, viewers, rate);
            }else
            {
                complete (false, nil, nil);
            }
    }];
}

- (void)requestGameNameUpdateWithID:(NSString *)identity team1:(TeamInstance *)team1 team2:(TeamInstance *)team2 completion:(void (^)(BOOL success))complete
{
    NSString *requestURL = [NSString stringWithFormat:@"scoreboard/%@",identity];
    NSData *jsonData = [self jsonDataForTeam1:team1 team2:team2 andSport:nil];
    NSMutableURLRequest *request =  [SHRequest PUTRequestWithExtension:requestURL andBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:self.opQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        
        bool success = httpResponse.statusCode == 200;
        if (complete) complete (success);
    }];
}

- (void)requestGameEndWithID:(NSString *)identity andTime:(NSNumber *)endTime completion:(void (^)(BOOL))complete
{
    NSString *requestURL = [NSString stringWithFormat:@"scoreboard/%@/end",identity];
    NSData *jsonData = [self jsonDataForEnd:endTime];
    NSMutableURLRequest *request =  [SHRequest POSTRequestWithExtension:requestURL andBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:self.opQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        
        bool success = httpResponse.statusCode == 200;
        if (complete) complete (success);
    }];
}

- (NSData *)jsonDataForEnd:(NSNumber *)time
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]initWithObjects:@[time]
                                                                        forKeys:@[@"gameTime"]];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    return jsonData;
}

- (NSData *)jsonDataForTeam1:(TeamInstance *)team1 team2:(TeamInstance *)team2 andSport:(SHSport *)sport
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]initWithObjects:@[team1.team.full_name ,team2.team.full_name]
                                                                        forKeys:@[@"team1Name",@"team2Name"]];
    
    [jsonDict setValue:[team1.assignedColour hexValue] forKey:@"team1Color"];
    [jsonDict setValue:[team2.assignedColour hexValue] forKey:@"team2Color"];
    if (sport) {
        [jsonDict setValue:sport.parent.name forKey:@"Sport"];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    return jsonData;
}

- (NSData *)jsonDataForTeam1Score:(NSNumber *)team1 andTeam2Score:(NSNumber *)team2 time:(NSNumber *)time period:(NSString *)period
{
    NSNumber *team1ScoreParsed = team1 != nil ? team1 : @0;
    NSNumber *team2ScoreParsed = team2 != nil ? team2 : @0;
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]initWithObjects:@[team1ScoreParsed,team2ScoreParsed, time, period]
                                                                        forKeys:@[@"team1Score",@"team2Score", @"gameTime", @"period"]];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    return jsonData;
}

@end
