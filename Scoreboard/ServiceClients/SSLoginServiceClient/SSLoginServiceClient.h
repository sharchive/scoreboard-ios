//
//  LoginServiceClient.h
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBServiceClient.h"

@interface SSLoginServiceClient : SBServiceClient

- (void)requestUserLoginWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete;

- (void)requestUserRegistrationWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete;

- (void)requestUserSignOut;

- (void)requestUserIsLoggedInComplete:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete;

@end
