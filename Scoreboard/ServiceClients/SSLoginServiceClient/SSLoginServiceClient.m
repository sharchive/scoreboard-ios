//
//  LoginServiceClient.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SSLoginServiceClient.h"
#import "SHRequest.h"


@interface SSLoginServiceClient ()

@end

@implementation SSLoginServiceClient

NSInteger const SuccessfulRegister = 200;
NSInteger const UserExists = 409;


- (void)requestUserLoginWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete
{
    NSData *jsonData = [self jsonDataForEmail:email andPassword:password];
    
    NSMutableURLRequest *request =  [SHRequest POSTRequestWithExtension:@"user/sign-in" andBody:jsonData];
    [request setTimeoutInterval:100];

    [NSURLConnection sendAsynchronousRequest:request queue:self.opQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;

        BOOL success = false;
        if (data) {
            NSDictionary *resultDictionary = [self resultJSONForResponseData:data];
            success = (bool)resultDictionary[@"Ok"];
        }
        
        NSString *message = [self loginMessageKeyForStatusCode:httpResponse.statusCode];
        
        complete (success, message, connectionError);
    }];
}

- (void)requestUserRegistrationWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete
{
    NSData *jsonData = [self jsonDataForEmail:email andPassword:password];
    
    NSMutableURLRequest *request =  [SHRequest POSTRequestWithExtension:@"user" andBody:jsonData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:self.opQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;

        BOOL success = httpResponse.statusCode == SuccessfulRegister;
        NSString *message = [self registerMessageKeyForStatusCode:httpResponse.statusCode];
        
        complete (success, message, connectionError);
    }];

}

- (void)requestUserSignOut
{
    NSMutableURLRequest *request =  [SHRequest GETRequestWithExtension:@"user/sign-out"];
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
}

- (void)requestUserIsLoggedInComplete:(void (^)(BOOL success, NSString * messageKey, NSError *error))complete
{
    NSMutableURLRequest *request =  [SHRequest GETRequestWithExtension:@"who-am-i"];
    
    NSError *error = nil;
    NSHTTPURLResponse  *response = nil;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

    bool success = responseData != nil;
    
    complete (success, @"isLoggedinTest", error);
}

- (NSData *)jsonDataForEmail:(NSString *)email andPassword:(NSString *)password
{
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc]initWithObjects:@[email,password]
                                                                        forKeys:@[@"Email",@"Password"]];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    return jsonData;
}

- (NSString *)registerMessageKeyForStatusCode:(NSInteger )code
{
    switch (code) {
        case SuccessfulRegister:
            return @"register-RegisterSuccess";
            break;

        case UserExists:
            return @"register-UserExists";
            break;
            
        default:
            return @"register-UnknownError";
            break;
    }
}

- (NSString *)loginMessageKeyForStatusCode:(NSInteger )code
{
    switch (code) {
        case SuccessfulRegister:
            return @"login-LoginSuccess";
            break;
            
        default:
            return @"login-UnknownError";
            break;
    }
}



@end
