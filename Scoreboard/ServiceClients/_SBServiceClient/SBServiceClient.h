//
//  SBServiceClient.h
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBServiceClient : NSObject

@property (strong, nonatomic) NSOperationQueue *opQueue;

- (NSDictionary *)resultJSONForResponseData:(NSData *)data;

@end
