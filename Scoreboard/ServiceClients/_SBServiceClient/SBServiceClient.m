//
//  SBServiceClient.m
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBServiceClient.h"

@interface SBServiceClient ()


@end

@implementation SBServiceClient

- (id)init
{
    self = [super init];
    if (self) {
        self.opQueue = [NSOperationQueue new];
    }
    return self;
}

- (NSDictionary *)resultJSONForResponseData:(NSData *)data
{
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingAllowFragments
                                                                 error:nil];
    return dictionary;
}

@end
