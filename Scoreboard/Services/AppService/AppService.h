//
//  AppService.h
//  Scoreboard
//
//  Created by Tom Bates on 27/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHRepository;

@interface AppService : NSObject

@property (strong, nonatomic) SHRepository *appRepository;

- (NSFetchedResultsController *)fetchAllApps;
- (void)createAppsFromArray:(NSArray *)array;
- (void)save;

@end
