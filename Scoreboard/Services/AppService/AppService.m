//
//  AppService.m
//  Scoreboard
//
//  Created by Tom Bates on 27/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "AppService.h"
#import "SHApp.h"
#import "SHRepository.h"

@implementation AppService

- (instancetype)init
{
    self = [super init];
    if (self){
        self.appRepository = [SHRepository newRepository:[SHApp class]];
    }
    return self;
}

- (NSFetchedResultsController *)fetchAllApps
{
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:true];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bundleID != %@",[[NSBundle mainBundle] bundleIdentifier]];
    return [self.appRepository fetchControllerWithPredicate:predicate andSortDescriptor:sort];
}

- (void)createAppsFromArray:(NSArray *)array
{
    [array enumerateObjectsUsingBlock:^(NSDictionary *app, NSUInteger idx, BOOL *stop) {
        [self.appRepository createNewEntityFromObject:app];
        [self save];
    }];
}

- (void)save
{
    [self.appRepository save];
}

@end
