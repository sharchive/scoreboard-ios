//
//  ColourService.h
//  Scoreboard
//
//  Created by Tom Bates on 08/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Colour;
@class SHRepository;

@interface ColourService : NSObject

@property (strong, nonatomic) SHRepository *colourRepository;

- (Colour *)createRandomColour;
- (Colour *)colourFromUIColor:(UIColor *)color;
- (Colour *)colourWithRed:(float)red green:(float)green andBlue:(float)blue;

@end
