//
//  ColourService.m
//  Scoreboard
//
//  Created by Tom Bates on 08/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "ColourService.h"
#import "Colour.h"
#import "SHRepository.h"

@implementation ColourService

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.colourRepository = [SHRepository newRepository:[Colour class]];
    }
    return self;
}

- (Colour *)createRandomColour
{
    return [self colourWithRed:[self randomRGB] green:[self randomRGB] andBlue:[self randomRGB]];
}

- (Colour *)colourFromUIColor:(UIColor *)color
{
    CGColorRef cgColor = [color CGColor];
    
    NSInteger numComponents = (NSInteger) CGColorGetNumberOfComponents(cgColor);
    
    if (numComponents == 4)
    {
        const CGFloat *components = CGColorGetComponents(cgColor);
        CGFloat red = components[0];
        CGFloat green = components[1];
        CGFloat blue = components[2];
        
        return [self colourWithRed:red green:green andBlue:blue];
    }
    return [self colourWithRed:0 green:0 andBlue:0];
}

- (Colour *)colourWithRed:(float)red green:(float)green andBlue:(float)blue
{
    Colour *newColour =  [self.colourRepository createNewEntity];
    newColour.red = [NSNumber numberWithFloat:red];
    newColour.green = [NSNumber numberWithFloat:green];
    newColour.blue = [NSNumber numberWithFloat:blue];
    return newColour;
}

- (float )randomRGB
{
    NSInteger randomInt = 0 + arc4random() % (255 - 0 + 1);
    float randomFloat = randomInt / 255.0;
    return randomFloat;
}

@end
