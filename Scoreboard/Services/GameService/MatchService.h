//
//  GameService.h
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHRepository;
@class Match;

@interface MatchService : NSObject

@property (strong, nonatomic) SHRepository *gameRepository;

- (Match *)createNewGame;
- (BOOL)save:(Match *)game;
- (void)undoAllChanges;
- (void)deleteGame:(Match *)game;
- (NSFetchedResultsController *)fetchAllGames;

@end
