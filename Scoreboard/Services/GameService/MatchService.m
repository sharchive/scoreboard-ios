//
//  GameService.m
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "MatchService.h"
#import "SHRepository.h"
#import "Match.h"

@implementation MatchService

- (id)init
{
    self = [super init];
    if (self) {
        self.gameRepository = [SHRepository newRepository:[Match class]];
    }
    return self;
}

- (Match *)createNewGame
{
    Match *game = [self.gameRepository createNewEntity];
    return game;
}

- (BOOL)save:(Match *)game
{
    [self.gameRepository save];
    return true;
}

- (void)undoAllChanges
{
    [self.gameRepository rollBackChanges];
}

- (void)deleteGame:(Match *)game
{
    [self.gameRepository deleteEntity:game];
}

- (NSFetchedResultsController *)fetchAllGames
{
    return [self.gameRepository fetchController];
}

@end
