//
//  SportService.h
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHRepository;
@class SHSport;

@interface SportService : NSObject

@property (strong, nonatomic) SHRepository *sportRepository;

- (SHSport *)createSport;
- (void)save;
- (void)createSportsFromArray:(NSArray *)sportArray;
- (SHSport *)createSportsFromDictionary:(NSDictionary *)sportDictionary;
- (void)rollbackChanges;
- (void)deleteSport:(SHSport *)sport;

- (NSArray *)allSports;
- (NSArray *)allSportsCategories;
- (NSFetchedResultsController *)fetchAllSports;
- (NSFetchedResultsController *)fetchAllSportsInCategory:(NSString *)category;
- (NSFetchedResultsController *)fetchAllSportsOrderdBy:(NSString *)order groupedBy:(NSString *)group;


@end
