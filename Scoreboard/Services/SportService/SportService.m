//
//  SportService.m
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportService.h"
#import "SHRepository.h"
#import "SHSport.h"

@implementation SportService

- (id)init
{
    self = [super init];
    if (self) {
        self.sportRepository = [SHRepository newRepository:[SHSport class]];
    }
    return self;
}

- (SHSport *)createSport
{
    SHSport *sport = (SHSport *)[self.sportRepository createNewEntity];
    return sport;
}

- (void)createSportsFromArray:(NSArray *)sportArray
{
    [sportArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        SHSport *newSport = [self.sportRepository createNewEntityFromObject:dict];
        newSport.orderValue = idx;
        newSport.showSettingsValue = false;
        if (idx != 0)
        {
            SHSport *variation = [self.sportRepository createNewEntityFromObject:dict];
            [newSport addVariationsObject:variation];
        }
    }];
}

- (SHSport *)createSportsFromDictionary:(NSDictionary *)sportDictionary
{
    SHSport *sport = [self.sportRepository createNewEntityFromObject:sportDictionary];
    return sport;
}

- (NSArray *)allSports
{
    return [self.sportRepository findAll];
}

- (NSArray *)allSportsCategories
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"variations.@count > 0 || name == 'Free Scoring'"];
    NSSortDescriptor *descriptorOrder = [[NSSortDescriptor alloc]initWithKey:@"order" ascending:true];
    NSSortDescriptor *descriptorName = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:true];

    return [self.sportRepository findAllInContext:self.sportRepository.coreDataManager.managedObjectContext withPredicate:predicate sortBy:@[descriptorOrder, descriptorName] error:nil];
    return [self.sportRepository findAllInContext:self.sportRepository.coreDataManager.managedObjectContext withPredicate:predicate error:nil];
}

- (NSFetchedResultsController *)fetchAllSports
{
    NSFetchedResultsController *controller = [self.sportRepository fetchController];
    [controller performFetch:nil];
    return controller;
}

- (NSFetchedResultsController *)fetchAllSportsOrderdBy:(NSString *)order groupedBy:(NSString *)group
{
    NSMutableArray *descriptors = [[NSMutableArray alloc]init];
    if (group) {
        [descriptors addObject:[NSSortDescriptor sortDescriptorWithKey:group ascending:true]];
    }
    if (order && ![order isEqualToString:group])
    {
        [descriptors addObject:[NSSortDescriptor sortDescriptorWithKey:order ascending:true]];
    }
    
    NSFetchedResultsController *results = [self.sportRepository fetchControllerWithSortDescriptors:descriptors groupedBy:group];
    
    return results;
}

- (NSFetchedResultsController *)fetchAllSportsInCategory:(NSString *)category
{
    NSPredicate *categoryPredicate = [NSPredicate predicateWithFormat:@"category == %@",category];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"dateCreatedUTC" ascending:false];
    NSFetchedResultsController *results = [self.sportRepository fetchControllerWithPredicate:categoryPredicate andSortDescriptor:sortDescriptor];
    return results;
}

- (void)deleteSport:(SHSport *)sport
{
    [self.sportRepository deleteEntity:sport];
}

- (void)save
{
    [self.sportRepository save];
}

- (void)rollbackChanges
{
    [self.sportRepository rollBackChanges];
}

@end
