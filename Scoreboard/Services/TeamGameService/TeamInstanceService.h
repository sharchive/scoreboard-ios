//
//  TeamGameService.h
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHRepository;
@class TeamInstance;
@class TeamService;

@interface TeamInstanceService : NSObject

@property (strong, nonatomic) SHRepository *teamGameRepository;
@property (strong, nonatomic) TeamService *teamService;

@property (strong, nonatomic) TeamInstance *homeTeamGame;
@property (strong, nonatomic) TeamInstance *awayTeamGame;

- (TeamInstance *)createNewTeamGame;

@end
