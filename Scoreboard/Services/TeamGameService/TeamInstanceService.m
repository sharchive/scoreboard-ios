//
//  TeamGameService.m
//  Scoreboard
//
//  Created by Tom Bates on 05/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "TeamInstanceService.h"
#import "TeamInstance.h"
#import "TeamService.h"
#import "Team.h"
#import "SHRepository.h"

@implementation TeamInstanceService

- (id)init
{
    self = [super init];
    if (self) {
        self.teamGameRepository = [SHRepository newRepository:[TeamInstance class]];
        self.teamService = [TeamService new];
    }
    return self;
}

- (TeamInstance *)createNewTeamGame
{
    TeamInstance *newTeamGame = [self.teamGameRepository createNewEntity];
    return newTeamGame;
}

- (TeamInstance *)homeTeamGame
{
    Team *team = self.teamService.homeTeam;
    TeamInstance *newTeamGame = [self createNewTeamGame];
    newTeamGame.team = team;
    newTeamGame.assignedColour = team.colour;
    return newTeamGame;
}

- (TeamInstance *)awayTeamGame
{
    Team *team = self.teamService.awayTeam;
    TeamInstance *newTeamGame = [self createNewTeamGame];
    newTeamGame.team = team;
    newTeamGame.assignedColour = team.colour;
    return newTeamGame;
}

@end
