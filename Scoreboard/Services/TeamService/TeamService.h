//
//  TeamService.h
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Team;

@interface TeamService : NSObject

@property (strong, nonatomic) Team *homeTeam;
@property (strong, nonatomic) Team *awayTeam;

- (Team *)createTeam;


@end
