//
//  TeamService.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "TeamService.h"
#import "ColourService.h"
#import "SHRepository.h"
#import "Team.h"

@interface TeamService ()

@property (strong, nonatomic) SHRepository *teamRepo;
@property (strong, nonatomic) ColourService *colourService;

@end

@implementation TeamService

- (id)init
{
    self = [super init];
    if (self) {
        self.teamRepo = [SHRepository newRepository:[Team class]];
        self.colourService = [ColourService new];
    }
    return self;
}

- (Team *)homeTeam
{
    NSString *homeString = NSLocalizedString(@"Home", nil);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"full_name == %@ && appCreated == true", homeString];
    Team *homeTeam = (Team *)[self.teamRepo findFirstWithPredicate:predicate];
    if (!homeTeam)
    {
        homeTeam = [self createTeamWithName: homeString];
        homeTeam.colour = [self.colourService colourFromUIColor:[UIColor redColor]];
        homeTeam.appCreatedValue = true;
    }
    return homeTeam;
}

- (Team *)awayTeam
{
    NSString *awayString = NSLocalizedString(@"Away", nil);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"full_name == %@ && appCreated == true", awayString];
    Team *awayTeam = (Team *)[self.teamRepo findFirstWithPredicate:predicate];
    if (!awayTeam)
    {
        awayTeam = [self createTeamWithName: awayString];
        awayTeam.colour = [self.colourService colourFromUIColor:[UIColor blueColor]];
        awayTeam.appCreatedValue = true;
    }
    return awayTeam;
}

- (Team *)createTeamWithName:(NSString *)name
{
    Team *team = [self createTeam];
    team.full_name = name;
    return team;
}

- (Team *)createTeam
{
    return (Team *)[self.teamRepo createNewEntity];
}

@end
