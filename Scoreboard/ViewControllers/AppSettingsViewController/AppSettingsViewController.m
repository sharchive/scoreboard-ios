//
//  AppSettingsViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 27/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "AppSettingsViewController.h"
#import "AppSettingsTableViewDataSource.h"
#import "AppSettingsViewModel.h"

@interface AppSettingsViewController ()

@property (strong, nonatomic) AppSettingsViewModel *localViewModel;
@property (strong, nonatomic) AppSettingsTableViewDataSource *dataSource;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AppSettingsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataSource = [[AppSettingsTableViewDataSource alloc]initWithTableView:self.tableView];
    self.dataSource.itemsController = self.localViewModel.appsController;
}

- (AppSettingsViewModel *)localViewModel
{
    return (AppSettingsViewModel *)self.viewModel;
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

@end
