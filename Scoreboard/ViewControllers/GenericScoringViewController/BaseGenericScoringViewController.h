//
//  BaseGenericScoringViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SHViewController.h"

@class TeamScoreView;
@class GameClockView;
@class ShotClockView;

@interface BaseGenericScoringViewController : SHViewController

@property (strong, nonatomic) IBOutlet GameClockView *gameTimeView;
@property (strong, nonatomic) IBOutlet ShotClockView *shotTimeView;
@property (strong, nonatomic) IBOutlet UICollectionView *periodsCollectionView;

@property (strong, nonatomic) IBOutletCollection(TeamScoreView) NSArray *teamScoreViews;

@property (strong, nonatomic) IBOutlet UIView *mainView;

@end
