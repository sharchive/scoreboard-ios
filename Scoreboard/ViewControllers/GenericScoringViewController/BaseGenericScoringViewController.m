//
//  BaseGenericScoringViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "BaseGenericScoringViewController.h"
#import "TeamScoreViewModel.h"
#import "TeamScoreView.h"
#import "BaseGenericScoringViewModel.h"
#import "SHSport.h"
#import "GameManager.h"
#import "GameClockView.h"
#import "ShotClockView.h"

@interface BaseGenericScoringViewController ()

@property (strong, nonatomic)BaseGenericScoringViewModel *localViewModel;

@end

@implementation BaseGenericScoringViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUp];
    [[UIApplication sharedApplication] setIdleTimerDisabled:true];
}

- (void)setUp
{
    [self.teamScoreViews enumerateObjectsUsingBlock:^(TeamScoreView *teamView, NSUInteger idx, BOOL *stop){
        TeamInstance *teamGame = [self.localViewModel.gameManager.teamGames objectAtIndex:idx];
        TeamScoreViewModel *viewModel = [[TeamScoreViewModel alloc]initWithManager:self.localViewModel.gameManager
                                                                        andTeamGame:teamGame];
         teamView.viewModel = viewModel;
     }];
    
    self.gameTimeView.gameTimeManager = self.localViewModel.timeManager;
    self.shotTimeView.gameTimeManager = self.localViewModel.timeManager;
}

- (void)setAppearance
{
    [super setAppearance];
    
    UIImage *image = [UIImage imageNamed:@"dark_stripe_bg"];
    [self.mainView setBackgroundColor:[UIColor colorWithPatternImage:image]];
    
    self.gameTimeView.hidden = !self.localViewModel.gameManager.sport.gameClockEnabledValue;

    self.gameTimeView.layer.borderWidth = 2.0;
    self.gameTimeView.layer.borderColor = [[AppearanceManager aquaBlueColour]CGColor];
    self.gameTimeView.layer.cornerRadius = 7.0;

    self.shotTimeView.layer.borderWidth = 2.0;
    self.shotTimeView.layer.borderColor = [[AppearanceManager aquaBlueColour]CGColor];
    self.shotTimeView.layer.cornerRadius = 7.0;
    
    self.shotTimeView.hidden = !self.localViewModel.gameManager.sport.shotClockEnabledValue;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationFade;
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

- (BaseGenericScoringViewModel *)localViewModel
{
    return (BaseGenericScoringViewModel *)self.viewModel;
}

@end
