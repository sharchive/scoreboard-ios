//
//  ExternalGenericScoringViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "ExternalGenericScoringViewController.h"
#import "InternalGenericScoringViewModel.h"
#import "PeriodsCollectionViewCell.h"

@interface ExternalGenericScoringViewController () <UICollectionViewDataSource>

@property (strong, nonatomic) InternalGenericScoringViewModel *localViewModel;

@end

@implementation ExternalGenericScoringViewController

- (void)applyBindings
{
    [super applyBindings];
    self.periodsCollectionView.dataSource = self;
    [self.localViewModel.gameManager addObserver:self
                          forKeyPath:@"currentPeriod"
                             options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                             context:nil];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.localViewModel.gameManager.gamePeriods integerValue];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PeriodsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PeriodsCollectionViewCell" forIndexPath:indexPath];
    
    [cell setOn:(indexPath.row < self.localViewModel.gameManager.currentPeriod)];
    
    return cell;
}

- (void)observedKey:(NSString *)keyPath changedValue:(id)newValue
{
    [self.periodsCollectionView reloadData];
}

- (InternalGenericScoringViewModel *)localViewModel
{
    return (InternalGenericScoringViewModel *)self.viewModel;
}


@end
