//
//  InternalGenericScoringViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "BaseGenericScoringViewController.h"

@interface InternalGenericScoringViewController : BaseGenericScoringViewController

@end
