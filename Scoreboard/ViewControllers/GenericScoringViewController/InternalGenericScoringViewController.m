 //
//  InternalGenericScoringViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "InternalGenericScoringViewController.h"
#import "GenericScoringSettingsTableViewDataSource.h"
#import "PeriodsCollectionViewDataSource.h"
#import "InternalGenericScoringViewModel.h"
#import "TeamScoreViewModel.h"
#import "TeamScoreView.h"
#import "Match.h"
#import "GameClockView.h"
#import "ShotClockView.h"


@interface InternalGenericScoringViewController ()

@property (strong, nonatomic) GenericScoringSettingsTableViewDataSource *menuDataSource;

@property (strong, nonatomic) InternalGenericScoringViewModel *localViewModel;
@property (strong, nonatomic) PeriodsCollectionViewDataSource *periodsDataSource;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scoreMenuConstraint;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;
@property (strong, nonatomic) IBOutlet UILabel *viewerNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *viewerTitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *settingsButton;

- (IBAction)menuButtonPressed:(UIButton *)sender;

@end

@implementation InternalGenericScoringViewController


- (void)applyBindings
{
    [super applyBindings];
    self.menuDataSource = [GenericScoringSettingsTableViewDataSource new];
    self.menuDataSource.viewController = self;
    self.menuTableView.dataSource = self.menuDataSource;
    self.menuDataSource.tableView = self.menuTableView;
    self.menuDataSource.game = self.localViewModel.gameManager.game;
    
    self.menuDataSource.exitGame = @selector(exitGame);
    self.menuDataSource.saveExitGame = @selector(saveExitGame);
    self.menuDataSource.shareGame = @selector(shareGame);
    
    [self.localViewModel addObserver:self
                          forKeyPath:@"gameViewers"
                             options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                             context:nil];
    
    self.periodsDataSource = [[PeriodsCollectionViewDataSource alloc]initWithCollectionView:self.periodsCollectionView];
    self.periodsDataSource.gameManager = self.localViewModel.gameManager;
}

- (void)setAppearance
{
    [super setAppearance];

    self.viewerNumberLabel.layer.cornerRadius = 3.0;
    [self.viewerTitleLabel setFont:[AppearanceManager standardFont]];
    [self.viewerNumberLabel setFont:[AppearanceManager standardFont]];
    
    UIImage *viewerImage = [[UIImage imageNamed:@"viewers_label_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
    self.viewerNumberLabel.backgroundColor = [UIColor colorWithPatternImage:viewerImage];
}

- (void)exitGame
{
    [self hideMenu:^{
        [self.localViewModel closeGame:false];
    }];
}

- (void)saveExitGame
{
    [self hideMenu:^{
        [self.localViewModel closeGame:true];
    }];
}

- (void)shareGame
{
    [self hideMenu:^{
        [self.localViewModel shareButtonPressed];
    }];
}

- (IBAction)menuButtonPressed:(UIButton *)sender
{
    if (self.scoreMenuConstraint.constant == 0)
    {
        [self showMenu:nil];
    }else
    {
        [self hideMenu:nil];
    }
}

- (void)showMenu:(void (^)())complete
{
    [self.settingsButton setSelected:true];
    [self.menuDataSource reload];
    [self adjustMenuPosition:self.menuTableView.frame.size.width completion:complete];
}

- (void)hideMenu:(void (^)())complete
{
    [self.settingsButton setSelected:false];
    [self adjustMenuPosition:0 completion:complete];
}

- (void)adjustMenuPosition:(float)position completion:(void (^)())complete
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.32 animations:^{
        self.scoreMenuConstraint.constant = position;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (complete) {
            complete ();
        }
    }];
}

- (void)setViewersLabelText
{
    NSString *viewers = [NSString stringWithFormat:@"%03ld", (long)[self.localViewModel.gameViewers integerValue]];
    self.viewerNumberLabel.text = viewers;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self setViewersLabelText];
}

-(BOOL)shouldAutorotate
{
    return UIInterfaceOrientationMaskLandscape;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeLeft;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

- (InternalGenericScoringViewModel *)localViewModel
{
    return (InternalGenericScoringViewModel *)self.viewModel;
}

@end
