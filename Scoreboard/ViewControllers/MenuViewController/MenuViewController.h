//
//  MenuViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHViewController.h"

@interface MenuViewController : SHViewController

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *menuButtons;
@property (strong, nonatomic) IBOutlet UIButton *settingsButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menuLeftConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menuWidth;

- (IBAction)settingsButtonPressed:(UIButton *)sender;
- (IBAction)menuButtonPressed:(UIButton *)sender;
- (IBAction)handleMenuPanGesture:(UIPanGestureRecognizer *)sender;

@end
