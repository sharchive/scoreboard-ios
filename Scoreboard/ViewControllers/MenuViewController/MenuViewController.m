//
//  MenuViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuViewModel.h"
#import "SportsListViewController.h"
#import "SportsListViewModel.h"

@interface MenuViewController ()

@property (strong, nonatomic) MenuViewModel *localViewModel;

@end

@implementation MenuViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self menuButtonPressed:self.menuButtons[0]];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (self.menuLeftConstraint) [self animateMenuToPosition:0 withDuration:1.5];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UINavigationController *navcontroller = (UINavigationController *)segue.destinationViewController;
    self.localViewModel.presenter.baseNavigationController = (SHNavigationController *)self.navigationController;
    self.localViewModel.localPresenter.contentNavigationController = (ContentNavigationController *)navcontroller;
    SportsListViewController *vc = navcontroller.viewControllers[0];
    vc.menuController = self;
    SportsListViewModel *viewmodel = [[SportsListViewModel alloc]initWithViewController:vc];
    vc.viewModel = viewmodel;
    vc.viewModel.presenter = self.viewModel.presenter;
}

- (IBAction)settingsButtonPressed:(UIButton *)sender
{
    [self.localViewModel showSettingsView];
}

- (IBAction)menuButtonPressed:(UIButton *)sender
{   
    [self.menuButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop){
        [button setSelected:[button isEqual:sender]];
    }];
    
    NSInteger index = [self.menuButtons indexOfObject:sender];
    [self.localViewModel menuItemSelected:index];
}

- (IBAction)handleMenuPanGesture:(UIPanGestureRecognizer *)sender
{
    NSInteger inverseMenuWidth = (self.menuWidth.constant * -1);
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        float velocity = [sender velocityInView:self.view].x;
        NSInteger position = velocity <= 0 ? inverseMenuWidth : 0;
        
        float dur = 0.8;
        if (velocity > -500 || velocity < 500) dur = 1.0;
        
        [self animateMenuToPosition:position withDuration:dur];
    }
    else if (sender.state == UIGestureRecognizerStateChanged)
    {
        NSInteger dampenedTranslation = [sender translationInView:sender.view].x;
        NSInteger newConstraint = self.menuLeftConstraint.constant + dampenedTranslation;
        if (newConstraint < inverseMenuWidth)
        {
            newConstraint = inverseMenuWidth;
        }
        else if (newConstraint > 0)
        {
            newConstraint = 0;
        }
        self.menuLeftConstraint.constant = newConstraint;
        [(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0,0) inView:sender.view];
    }
}

- (void)animateMenuToPosition:(NSInteger)pos withDuration:(NSInteger)dur
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:dur
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.menuLeftConstraint.constant = pos;
                         [self.view layoutIfNeeded];
                     } completion:nil];
}

- (MenuViewModel *)localViewModel
{
    if (!self.viewModel)
    {
        self.viewModel = [MenuViewModel new];
        self.viewModel.viewController = self;
        self.viewModel.presenter = [SBPresenter new];
    }
    return (MenuViewModel *)self.viewModel;
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

@end
