//
//  SettingsViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 11/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsTableViewDataSource.h"
#import "SettingsViewModel.h"

@interface SettingsViewController ()

@property (strong, nonatomic) SettingsTableViewDataSource *dataSource;
@property (strong, nonatomic) SettingsViewModel *localViewModel;

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"close_modal_button"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
   [self.navigationItem setLeftBarButtonItem:barItem];

    self.title = self.viewModel.title;
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationController setNavigationBarHidden:true];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:true];
}

- (void)applyBindings
{
    self.dataSource = [SettingsTableViewDataSource new];
    self.dataSource.items = [self.localViewModel.settingsMenuItems mutableCopy];
    [self.dataSource setTableView:self.tableView];
    [RACObserve(self.dataSource, selectedItem)subscribeNext:^(SHViewModel *x) {
        if (x) {
            [self.viewModel showViewModel:x];
        }
        else{
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:true];
        }
    }];
    
}

- (IBAction)cancelButtonPressed:(UIBarButtonItem *)sender
{
    [self.viewModel closeThis];
}

-(SettingsViewModel *)localViewModel
{
    return (SettingsViewModel *)self.viewModel;
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

@end
