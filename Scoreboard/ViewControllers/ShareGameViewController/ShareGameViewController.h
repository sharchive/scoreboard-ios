//
//  ShareGameViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 10/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHModalViewController.h"

@interface ShareGameViewController : SHModalViewController

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *uiShareButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *keyBoardSpaceConstraint;
@property (strong, nonatomic) IBOutlet UIImageView *textImageView;

- (IBAction)shareButtonPressed:(id)sender;

@end
