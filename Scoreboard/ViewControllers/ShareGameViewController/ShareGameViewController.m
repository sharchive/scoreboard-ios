//
//  ShareGameViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 10/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "ShareGameViewController.h"
#import "ShareGameViewModel.h"
#import "Match.h"

@interface ShareGameViewController ()

@property (strong, nonatomic) ShareGameViewModel *localViewModel;

@end

@implementation ShareGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *storyboardName = [[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:[NSBundle mainBundle]];    
    self.descriptionTextView.inputAccessoryView = [[storyboard instantiateViewControllerWithIdentifier:@"SocialPickerViewController"]view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotificationFired:) name:UIKeyboardDidShowNotification object:nil];
   [self.descriptionTextView becomeFirstResponder];
}

- (void)applyBindings
{
    [super applyBindings];
    @weakify(self)
    [RACObserve(self.localViewModel, activityInProgress)subscribeNext:^(NSNumber *x)
    {
        @strongify(self)
        
        self.activityIndicator.hidden = ![x boolValue];
      if ([x boolValue] == true)
        {
            [self.activityIndicator startAnimating];
        }else
        {
            self.shareButton.enabled = !self.localViewModel.game.gameSharedValue;
            self.descriptionTextView.text = self.localViewModel.shareDescription;
        }
    }];

    [RACObserve(self.localViewModel.game, gameShared)subscribeNext:^(NSNumber *x) {

    }];
    
    RAC(self.localViewModel, shareDescription) = self.descriptionTextView.rac_textSignal;
}

- (void)setAppearance
{
    [self.textImageView setImage:[[UIImage imageNamed:@"share_text_view_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)]];
    self.descriptionTextView.backgroundColor = [UIColor clearColor];
    [self.view layoutIfNeeded];
}

- (IBAction)shareButtonPressed:(UIBarButtonItem *)sender
{
    [self.localViewModel shareGameLink];
}

- (ShareGameViewModel *)localViewModel
{
    return (ShareGameViewModel *)self.viewModel;
}

- (void)keyboardNotificationFired:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if (UIInterfaceOrientationIsLandscape([[UIDevice currentDevice] orientation]))
    {
        self.keyBoardSpaceConstraint.constant = kbSize.width;
    }
    else
    {
        self.keyBoardSpaceConstraint.constant = kbSize.height;
    }
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

@end
