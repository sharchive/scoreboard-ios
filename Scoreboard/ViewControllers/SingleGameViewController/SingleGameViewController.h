//
//  SingleGameViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 21/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHModalViewController.h"

@interface SingleGameViewController : SHModalViewController

@end
