//
//  SingleGameViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 21/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SingleGameViewController.h"
#import "SingleGameViewModel.h"
#import "Match.h"

@interface SingleGameViewController ()

@property (strong, nonatomic) SingleGameViewModel *localViewModel;

@end

@implementation SingleGameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)applyBindings
{
    [self.localViewModel addObserver:self
                          forKeyPath:@"game"
                             options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                             context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    Match *game = [change objectForKey:NSKeyValueChangeNewKey];
    [self setTitle:[NSString stringWithFormat:@"%@",game.date]];
}

- (SingleGameViewModel *)localViewModel
{
    return (SingleGameViewModel *)self.viewModel;
}

@end
