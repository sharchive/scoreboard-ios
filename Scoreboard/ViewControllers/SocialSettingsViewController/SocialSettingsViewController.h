//
//  SocialSettingsViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHViewController.h"

@class SocialSettingsTableViewCell;

@interface SocialSettingsViewController : SHViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
