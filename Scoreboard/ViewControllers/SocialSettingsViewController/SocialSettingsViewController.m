//
//  SocialSettingsViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SocialSettingsViewController.h"
#import "SocialSettingsViewModel.h"
#import "SocialSettingsTableViewDataSource.h"

@interface SocialSettingsViewController ()

@property (strong, nonatomic) SocialSettingsViewModel *localViewModel;
@property (strong, nonatomic) SocialSettingsTableViewDataSource *dataSource;

@end

@implementation SocialSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)applyBindings
{
    [super applyBindings];
    self.dataSource = [SocialSettingsTableViewDataSource new];
    self.dataSource.items = [self.localViewModel.socialItems mutableCopy];
    self.dataSource.tableView = self.tableView;
}

- (SocialSettingsViewModel *)localViewModel
{
    return (SocialSettingsViewModel *)self.viewModel;
}

- (BOOL)prefersStatusBarHidden
{
    return true;
}

@end
