//
//  SportsListViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 27/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHViewController.h"

@class MenuViewController;
@class SportsListViewModel;

@interface SportsListViewController : SHViewController

@property (strong, nonatomic) MenuViewController *menuController;

- (IBAction)playButtonPressed:(UIButton *)sender;

@end
