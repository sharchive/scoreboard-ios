//
//  SportsListViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 27/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportsListViewController.h"
#import "SSLoginManager.h"
#import "SportsListViewModel.h"
#import "MenuViewController.h"
#import "SportsListTableViewDataSource.h"
#import "SportSettingsTableViewDataSource.h"
#import "SHSport.h"
#import "AppearanceManager.h"

@interface SportsListViewController ()

@property (strong, nonatomic) IBOutlet UITableView *sportListTableView;
@property (strong, nonatomic) IBOutlet UITableView *settingsTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sportListWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sportListLeftConstraint;
@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *sportsTopButtons;

@property (strong, nonatomic) SSLoginManager *loginManager;
@property (strong, nonatomic) SportsListViewModel *localViewModel;
@property (strong, nonatomic) SportsListTableViewDataSource *sportListDataSource;
@property (strong, nonatomic) SportSettingsTableViewDataSource *sportSettingsDataSource;

@end

@implementation SportsListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.loginManager  = [SSLoginManager new];
}

- (void)applyBindings
{
    self.sportListDataSource = [[SportsListTableViewDataSource alloc]initWithTableView:self.sportListTableView];
    
    self.sportListDataSource.items = [self.localViewModel.sportCategories mutableCopy];
    
    self.sportSettingsDataSource = [[SportSettingsTableViewDataSource alloc]initWithTableView:self.settingsTableView];
    
    [self.sportListDataSource addObserver:self
                               forKeyPath:@"selectedItem"
                                  options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                  context:nil];
    
    [self.sportSettingsDataSource addObserver:self
                                   forKeyPath:@"madeChanges"
                                      options:NSKeyValueObservingOptionNew
                                      context:nil];
}

- (void)setAppearance
{
    UIImage *bg_image = [UIImage imageNamed:@"dark_stripe_bg"];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:bg_image]];
    
    
    [self.playButton.layer setBorderColor:[AppearanceManager aquaBlueColour].CGColor];
    
    if (SB_DEVICE_IS_IPAD)
    {
        [self.playButton.layer setCornerRadius:16];
        [self.playButton.layer setBorderWidth:2.0];
    }
    else
    {
        [self.playButton.layer setCornerRadius:12];
        [self.playButton.layer setBorderWidth:1.0];
    }
}

- (void)setFonts
{
    [self.playButton.titleLabel setFont:[AppearanceManager standardFont]];
}

- (IBAction)playButtonPressed:(UIButton *)sender
{
    [self.localViewModel showGameView];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    if ([keyPath isEqualToString:@"selectedItem"])
    {
        if ([[change objectForKey:NSKeyValueChangeNewKey] isKindOfClass:[NSNull class]] || [change objectForKey:NSKeyValueChangeNewKey] == nil) {
            [self handleSportChange:nil];
        }
        else
        {
            SHSport *sport = (SHSport *)[change objectForKey:NSKeyValueChangeNewKey];
            [self handleSportChange:sport];
        }
    }
    else
    {
        [self.localViewModel saveSports];
    }
}

- (void)handleSportChange:(SHSport *)sport
{
    if ([sport.variations count] > 0) {
        [self animateListViewToShow:[sport.variations allObjects]];
    }
    else
    {
        self.sportSettingsDataSource.selectedItem = sport;
        self.localViewModel.sport = sport;
        [self.settingsTableView setHidden:false];
        [self animationButtonVisibility:sport.parent != nil];
    }
    self.settingsTableView.hidden = !sport.showSettingsValue;
}

- (void)animationButtonVisibility:(BOOL)visible
{
    NSInteger alpha = 0;
    if (visible == true) {
        alpha = 1;
    }
    
    if (visible == true) {
        [self.sportsTopButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
            [button setHidden:false];
        }];
    }
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.15
                     animations:^{
                         [self.sportsTopButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
                             [button setAlpha:alpha];
                         }];
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         if (visible == false) {
                             [self.sportsTopButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
                                 [button setHidden:true];
                             }];
                         }
                     }];
}

- (void)animateListViewToShow:(NSArray *)sports
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.15
                     animations:^{
                         self.sportListLeftConstraint.constant = -self.sportListWidthConstraint.constant;
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         
                         self.sportListDataSource.items = [sports mutableCopy];
                         [UIView animateWithDuration:0.15
                                          animations:^{
                                              self.sportListLeftConstraint.constant = 0;
                                              [self.view layoutIfNeeded];
                                          }];
                     }];
}

- (IBAction)newButtonPressed:(UIButton *)sender
{
    SHSport *sport = [self.localViewModel cloneAsVariationSport:self.sportListDataSource.selectedSport.parent];
    [self.sportListDataSource insertItem:sport];
}

- (IBAction)backButtonPressed:(id)sender
{
    [self animateListViewToShow:[self.localViewModel.sportCategories mutableCopy]];
    [self animationButtonVisibility:false];
}

- (SportsListViewModel *)localViewModel
{
    return  (SportsListViewModel *)self.viewModel;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

@end
