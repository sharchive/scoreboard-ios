//
//  TimeAdjustInputViewController.h
//  Scoreboard
//
//  Created by Tom Bates on 27/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHViewController.h"

@interface TimeAdjustInputViewController : SHViewController

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *timeButtons;

@property (strong, nonatomic) IBOutlet UIButton *minutesTimeButton;
@property (strong, nonatomic) IBOutlet UIButton *secondsTimeButton;
@property (strong, nonatomic) IBOutlet UIButton *miliTimeButton;

- (IBAction)increaseButtonPressed:(UIButton *)sender;
- (IBAction)decreaseButtonPressed:(UIButton *)sender;
- (IBAction)timeButtonPressed:(UIButton *)sender;

@end
