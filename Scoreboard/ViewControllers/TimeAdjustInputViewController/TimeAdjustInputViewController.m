//
//  TimeAdjustInputViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 27/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "TimeAdjustInputViewController.h"
#import "TimeAdjustInputViewModel.h"
#import "NSNumber+TimeAdditions.h"
#import "SHSport.h"
#import "AppearanceManager.h"

@interface TimeAdjustInputViewController ()

@property (strong, nonatomic) TimeAdjustInputViewModel *localViewModel;
@property (strong, nonatomic) IBOutlet UIView *timeBackgroundView;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@end

@implementation TimeAdjustInputViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self timeButtonPressed:self.timeButtons[0]];
}

- (void)applyBindings
{
    [super applyBindings];
    [self.localViewModel addObserver:self forKeyPath:@"time" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)setAppearance
{
    self.timeBackgroundView.layer.cornerRadius = 5.0;
    [self.timeBackgroundView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"large_time_change_bg"]]];
}

- (void)setFonts
{
    [self.buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
        [button.titleLabel setFont:[AppearanceManager largeFont]];
    }];
    
    [self.labels enumerateObjectsUsingBlock:^(UILabel *label, NSUInteger idx, BOOL *stop) {
        [label setFont:[AppearanceManager largeFont]];
    }];
}

- (IBAction)increaseButtonPressed:(UIButton *)sender
{
    if (self.minutesTimeButton.selected)
    {
        [self.localViewModel increaseMinutes];
    }
    else if (self.secondsTimeButton.selected)
    {
        [self.localViewModel increaseSeconds];
    }
    else
    {
        [self.localViewModel increaseTime];
    }
}

- (IBAction)decreaseButtonPressed:(UIButton *)sender
{
    if (self.minutesTimeButton.selected)
    {
        [self.localViewModel decreaseMinutes];
    }
    else if (self.secondsTimeButton.selected)
    {
        [self.localViewModel decreaseSeconds];
    }
    else
    {
        [self.localViewModel decreaseTime];
    }
}

- (void)observedKey:(NSString *)keyPath changedValue:(id)newValue
{
    [self setTimeButtonTitles:newValue];
}

- (void)setTimeButtonTitles:(NSNumber *)time
{
    [self.minutesTimeButton setTitle:[time minutesString] forState:UIControlStateNormal];
    [self.minutesTimeButton setTitle:[time minutesString] forState:UIControlStateSelected];
    
    [self.secondsTimeButton setTitle:[time secondsString] forState:UIControlStateNormal];
    [self.secondsTimeButton setTitle:[time secondsString] forState:UIControlStateSelected];
    
    [self.miliTimeButton setTitle:[time milliString] forState:UIControlStateNormal];
    [self.miliTimeButton setTitle:[time milliString] forState:UIControlStateSelected];
    
}

- (IBAction)timeButtonPressed:(UIButton *)sender
{
    [self.timeButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
        [button setSelected:[button isEqual:sender]];
    }];
}

- (TimeAdjustInputViewModel *)localViewModel
{
    return (TimeAdjustInputViewModel *)self.viewModel;
}

@end
