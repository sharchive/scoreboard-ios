//
//  UserNotificationViewController.m
//  Scoreboard
//
//  Created by Tom Bates on 03/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "UserNotificationViewController.h"
#import "UserNotificationCollectionViewDataSource.h"
#import "UserNotificationViewModel.h"

@interface UserNotificationViewController ()

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) UserNotificationCollectionViewDataSource *notificationDataSource;
@property (strong, nonatomic) UserNotificationViewModel *localViewModel;

@end

@implementation UserNotificationViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.notificationDataSource = [[UserNotificationCollectionViewDataSource alloc] initWithCollectionView:self.collectionView];
    self.notificationDataSource.viewController = self;
    self.notificationDataSource.items = self.localViewModel.currentNotifications;
}

- (void)applyBindings
{
    [super applyBindings];
    [self.viewModel addObserver:self forKeyPath:@"pendingNotification" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    id oldMessage = [change objectForKey:NSKeyValueChangeOldKey];
    id newMessage = [change objectForKey:NSKeyValueChangeNewKey];
    if (newMessage && newMessage != oldMessage)
    {
        [self.notificationDataSource insertItem:newMessage];
    }
}

- (UserNotificationViewModel *)localViewModel
{
    return (UserNotificationViewModel *) self.viewModel;
}

@end
