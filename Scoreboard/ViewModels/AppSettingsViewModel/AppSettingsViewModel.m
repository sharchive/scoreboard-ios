//
//  AppSettingsViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 27/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "AppSettingsViewModel.h"
#import "AppService.h"

@interface AppSettingsViewModel ()

@property (strong, nonatomic) AppService *appService;

@end

@implementation AppSettingsViewModel

- (id)init
{
    self = [super init];
    if (self) {
        self.title = [NSLocalizedString(@"Other Apps", nil) uppercaseString];
        self.appService = [AppService new];
        [self loadAvailableApps];
    }
    return self;
}

- (void)loadAvailableApps
{
    self.appsController = [self.appService fetchAllApps];
}

@end
