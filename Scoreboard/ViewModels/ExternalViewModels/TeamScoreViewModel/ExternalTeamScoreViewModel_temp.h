//
//  ExternalTeamScoreViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 27/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SBViewModel.h"

@class TeamInstance;

@interface ExternalTeamScoreViewModel_temp : SBViewModel

- (instancetype)initWithTeamGame:(TeamInstance *)teamGame;

- (NSString *)teamName;
- (NSNumber *)teamScore;
- (NSNumber *)teamSecondaryScore;
- (UIColor *)teamColour;

@end
