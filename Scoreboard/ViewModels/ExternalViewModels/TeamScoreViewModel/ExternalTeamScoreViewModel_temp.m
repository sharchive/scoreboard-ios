//
//  ExternalTeamScoreViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 27/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "ExternalTeamScoreViewModel_temp.h"
#import "TeamInstance.h"
#import "Match.h"
#import "Team.h"
#import "TeamInstance.h"
#import "Colour.h"


@interface ExternalTeamScoreViewModel_temp ()

@property (strong, nonatomic) TeamInstance *teamGame;

@end

@implementation ExternalTeamScoreViewModel_temp

- (instancetype)initWithTeamGame:(TeamInstance *)teamGame
{
    self = [super init];
    if (self) {
        self.teamGame = teamGame;
    }
    return self;
}

- (NSString *)teamName
{
    return self.teamGame.team.full_name;
}

- (NSNumber *)teamScore
{
    return self.teamGame.score != nil ? self.teamGame.score : @0;
}

- (NSNumber *)teamSecondaryScore
{
    return self.teamGame.secondaryScore != nil ? self.teamGame.secondaryScore : @0;
}

- (UIColor *)teamColour
{
    Colour *teamColour = self.teamGame.assignedColour;
    return [UIColor colorWithRed:[teamColour.red floatValue] green:[teamColour.green floatValue] blue:[teamColour.blue floatValue] alpha:1];
}


@end
