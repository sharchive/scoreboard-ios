//
//  BaseGenericScoringViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "BaseGenericScoringViewModel.h"
#import "Match.h"
#import "SHSport.h"
#import "PeriodsCollectionViewDataSource.h"

@implementation BaseGenericScoringViewModel

- (GameTimeManager *)timeManager
{
    return self.gameManager.timeManager;
}

@end
