//
//  ExternalGenericScoringViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "BaseGenericScoringViewModel.h"

@interface ExternalGenericScoringViewModel : BaseGenericScoringViewModel

@end
