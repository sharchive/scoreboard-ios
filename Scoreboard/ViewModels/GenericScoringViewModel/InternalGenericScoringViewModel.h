//
//  InternalGenericScoringViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "BaseGenericScoringViewModel.h"

@class SocialManager;

@interface InternalGenericScoringViewModel : BaseGenericScoringViewModel

@property (strong, nonatomic) NSNumber *gameViewers;


@property (strong, nonatomic) SocialManager *socialManager;
@property (assign, nonatomic) SocialConnectionStatus facebookStatus;

- (void)closeGame:(BOOL)save;
- (void)shareButtonPressed;

@end
