//
//  InternalGenericScoringViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 28/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "InternalGenericScoringViewModel.h"
#import "GameManager.h"
#import "SocialManager.h"
#import "ShareGameViewModel.h"
#import "Match.h"

@interface InternalGenericScoringViewModel ()

@property (strong, nonatomic) NSTimer *refreshTimer;
@property (strong, nonatomic) NSNumber *refreshTime;

@end

@implementation InternalGenericScoringViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.socialManager = [SocialManager instance];
        self.refreshTime = @5;
        [self applySocialBindings];
    }
    return self;
}

- (void)applySocialBindings
{
    [RACObserve(self.socialManager, facebookStatus)subscribeNext:^(NSNumber *x){
        self.facebookStatus = (SocialConnectionStatus)[x intValue];
    }];
}

- (void)shareButtonPressed
{
    ShareGameViewModel *shareViewModel =[[ShareGameViewModel alloc]initWithGameManager:self.gameManager];
    [self showViewModel:shareViewModel];
    [RACObserve(shareViewModel, url)subscribeNext:^(NSString *x) {
        if (x){
            self.gameManager.externalURL = x;
            [self refreshGameViewers];
        }
    }];
}

- (void)refreshGameViewers
{
    [self.gameManager refreshGameViewersWithCompletion:^(BOOL success, NSNumber *viewers, NSNumber *refreshRate) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (viewers) self.gameViewers = viewers;
            if (refreshRate) self.refreshTime = refreshRate;
        });
        [self sceduleViewerRefresh];
    }];
}

- (void)sceduleViewerRefresh
{
    [self.refreshTimer invalidate];
    self.refreshTimer = [NSTimer timerWithTimeInterval:[self.refreshTime integerValue]
                                                target:self
                                              selector:@selector(refreshGameViewers)
                                              userInfo:nil
                                               repeats:true];
    [[NSRunLoop mainRunLoop] addTimer:self.refreshTimer forMode:NSRunLoopCommonModes];
}


- (void)closeGame:(BOOL)save
{
    [[UIApplication sharedApplication] setIdleTimerDisabled:false];
    [self.gameManager completeGame];
    if (save) [self.gameManager saveGame];
    [self.gameManager removeGameChanges];
    [self closeThis];
}

@end
