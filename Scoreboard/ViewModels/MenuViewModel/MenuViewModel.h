//
//  MenuViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@interface MenuViewModel : SBViewModel

- (void)menuItemSelected:(NSInteger)index;
- (void)showSettingsView;

@end
