//
//  MenuViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 22/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "MenuViewModel.h"
#import "SettingsViewModel.h"

@interface MenuViewModel ()

@end

@implementation MenuViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)menuItemSelected:(NSInteger)index
{
    self.localPresenter.presentationStyle = PresentationStyleMenu;
}

- (void)showSettingsView
{
    self.localPresenter.presentationStyle = PresentationStyleModalNav;
    SettingsViewModel *viewModel = [SettingsViewModel new];
    [self showViewModel:viewModel];

//    SBNavigationController *navController = [[SBNavigationController alloc]initWithRootViewController:viewModel.viewController];
//    navController.modalPresentationStyle = viewModel.viewController.modalPresentationStyle;
//    [self showViewController:navController];
}

@end
