//
//  SettingsViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 11/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SettingsViewModel.h"
#import "SocialSettingsViewModel.h"
#import "AppSettingsViewModel.h"


@implementation SettingsViewModel


- (id)init
{
    self = [super init];
    if (self) {
        self.title = [NSLocalizedString(@"Settings", nil) uppercaseString];
        self.settingsMenuItems = @[[SocialSettingsViewModel new], [AppSettingsViewModel new]];
    }
    return self;
}

@end
