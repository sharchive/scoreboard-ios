//
//  ShareGameViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 10/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@class SocialManager;
@class GameManager;
@class Match;

@interface ShareGameViewModel : SBViewModel

@property (strong, nonatomic) Match *game;
@property (strong, nonatomic) SocialManager *socialManager;
@property (assign, nonatomic) SocialConnectionStatus facebookStatus;
@property (strong, nonatomic) GameManager *gameManager;
@property (assign, nonatomic) BOOL activityInProgress;
@property (strong, nonatomic) NSString *shareDescription;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) RACSignal *fetchSignal;

- (id)initWithGameManager:(GameManager *)gameManager;
- (void)shareGameLink;

@end
