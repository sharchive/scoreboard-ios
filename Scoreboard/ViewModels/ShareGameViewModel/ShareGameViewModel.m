//
//  ShareGameViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 10/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "ShareGameViewModel.h"
#import "SocialManager.h"
#import "Match.h"
#import "GameManager.h"

@implementation ShareGameViewModel

- (id)initWithGameManager:(GameManager *)gameManager
{
    self = [super init];
    if (self) {
        self.title = [NSLocalizedString(@"Share", nil) uppercaseString];
        self.gameManager = gameManager;
        [self getGameURL];
        self.socialManager = [SocialManager instance];
    }
    return self;
}

- (void)getGameURL
{
    self.activityInProgress = true;
    
    if (self.game.externalURL != nil)
    {
        self.shareDescription = [self descriptionForGame];
        self.activityInProgress = false;
        return;
    }
    
    @weakify (self)
    [self.gameManager getURLForGameWithCompletion:^(NSString *gameURL, NSError *error) {
        @strongify(self)
        if (gameURL.length > 0)
        {
            self.url = gameURL;
            self.game.externalURL = gameURL;
            self.shareDescription = [self descriptionForGame];
        }
        else NSLog(@"Error getting URL");
        
        self.activityInProgress = false;
    }];
}

- (void)shareGameLink
{
    @weakify (self)
    self.activityInProgress = true;
    [self.socialManager shareGameURL:self.game.externalURL completion:^(BOOL success, NSError *error) {
        @strongify (self)
        if (!error)
        {
            self.game.gameShared = [NSNumber numberWithBool:success];
        }
        else NSLog(@"Share error %@",error);

        self.activityInProgress = false;
    }];
    
}

- (NSString *)descriptionForGame
{
    NSString *urlString = [NSLocalizedString(@"Share game message", nil) stringByReplacingOccurrencesOfString:@"<url>" withString:[NSString stringWithFormat:@"%@ ",self.game.externalURL]];
    return urlString;
}

- (Match *)game
{
    return self.gameManager.game;
}

@end
