//
//  SingleGameViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 21/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@class Match;

@interface SingleGameViewModel : SBViewModel

@property (strong, nonatomic) Match *game;

@end
