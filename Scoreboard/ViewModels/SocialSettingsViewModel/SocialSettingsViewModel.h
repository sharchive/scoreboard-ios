//
//  SocialSettingsViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@interface SocialSettingsViewModel : SBViewModel

@property (assign, nonatomic) SocialConnectionStatus facebookStatus;
@property (strong, nonatomic) NSArray *socialItems;

@end
