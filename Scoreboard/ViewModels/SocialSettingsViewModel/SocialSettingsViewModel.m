//
//  SocialSettingsViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SocialSettingsViewModel.h"
#import "SocialManager.h"

@interface SocialSettingsViewModel ()

@property (strong, nonatomic) SocialManager *socialManager;

@end

@implementation SocialSettingsViewModel

- (id)init
{
    self = [super init];
    if (self) {
        self.title = [NSLocalizedString(@"Social", nil) uppercaseString];
        self.socialManager = [SocialManager instance];
        self.socialItems = self.socialManager.socialClients;
    }
    return self;
}

- (void)applySocialBindings
{
    [RACObserve(self.socialManager, facebookStatus)subscribeNext:^(NSNumber *x) {
        self.facebookStatus = (SocialConnectionStatus)[x intValue];
    }];
}

-(void)facebookButtonPressed
{
    if (self.socialManager.facebookStatus == SCSDisconnected)
    {
        [self.socialManager loginToFacebook];
    }
    else
    {
        [self.socialManager logoutOfFacebook];
    }
}

@end
