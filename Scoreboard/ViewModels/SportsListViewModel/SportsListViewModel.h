//
//  SportsListViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 27/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@class SportManager;
@class SHSport;

@interface SportsListViewModel : SBViewModel <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) SHSport *sport;
@property (strong, nonatomic) NSArray *sportCategories;

- (void)showGameView;
- (void)saveSports;
- (SHSport *)cloneAsVariationSport:(SHSport *)sport;

@end
