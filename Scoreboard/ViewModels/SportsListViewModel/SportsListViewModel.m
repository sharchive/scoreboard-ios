//
//  SportsListViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 27/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportsListViewModel.h"
#import "InternalGenericScoringViewModel.h"
#import "SportManager.h"
#import "SHSport.h"
#import "MatchBuilder.h"
#import "SportService.h"
#import "GameManager.h"
#import "Match.h"

@interface SportsListViewModel ()

@property (strong, nonatomic) SportService *sportService;

@end

@implementation SportsListViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sportService = [SportService new];
        self.sportCategories = [self.sportService allSportsCategories];
    }
    return self;
}

- (void)saveSports
{
    [self.sportService save];
}

- (void)showGameView
{
    GameManager *manager  = [GameManager new];
    manager.sport = self.sport;
    
    self.localPresenter.presentationStyle = PresentationStyleBase;
    
    InternalGenericScoringViewModel *viewModel = [InternalGenericScoringViewModel new];
    viewModel.gameManager = manager;
    [self showViewModel:viewModel];
}

- (SHSport *)cloneAsVariationSport:(SHSport *)sport
{
    SHSport *clone = (SHSport*)[sport clone];
    [sport addVariationsObject:clone];
    clone.showSettingsValue = true;
    clone.editableValue = true;
    return clone;    
}

@end
