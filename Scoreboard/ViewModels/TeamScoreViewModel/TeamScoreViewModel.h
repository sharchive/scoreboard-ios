//
//  TeamScoreViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 07/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@class TeamInstance;
@class GameManager;

@interface TeamScoreViewModel : SBViewModel

@property (strong, nonatomic, readonly) TeamInstance *teamGame;
@property (strong, nonatomic) NSString *teamName;
@property (strong, nonatomic) NSNumber *teamScore;
@property (strong, nonatomic) NSNumber *teamSecondaryScore;
@property (strong, nonatomic) UIColor *teamColour;

- (instancetype)initWithManager:(GameManager *)manager andTeamGame:(TeamInstance *)teamGame;
- (void)increaseTeamScore;
- (void)decreaseTeamScore;
- (void)increaseTeamSecondary;
- (void)decreaseTeamSecondary;

@end
