//
//  TeamScoreViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 07/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "TeamScoreViewModel.h"
#import "Match.h"
#import "Team.h"
#import "TeamInstance.h"
#import "GameManager.h"
#import "Colour.h"

@interface TeamScoreViewModel ()

@property (strong, nonatomic) GameManager *gameManager;
@property (strong, nonatomic, readwrite) TeamInstance *teamGame;

@end

@implementation TeamScoreViewModel

- (instancetype)initWithManager:(GameManager *)manager andTeamGame:(TeamInstance *)teamGame;
{
    self = [super init];
    if (self) {
        self.gameManager = manager;
        self.teamGame = teamGame;
    }
    return self;
}

- (void)increaseTeamScore
{
    NSInteger score = [self.teamGame.score integerValue];
    score ++;
    self.teamGame.score = [NSNumber numberWithInteger:score];
    [self updateTeamScores];
}

- (void)decreaseTeamScore
{
    NSInteger score = [self.teamGame.score integerValue];
    score --;
    self.teamGame.score = [NSNumber numberWithInteger:score];
    [self updateTeamScores];
}

- (void)increaseTeamSecondary
{
    NSInteger score = [self.teamGame.secondaryScore integerValue];
    score ++;
    self.teamGame.secondaryScore = [NSNumber numberWithInteger:score];
    [self updateTeamScores];
}

- (void)decreaseTeamSecondary
{
    NSInteger score = [self.teamGame.secondaryScore integerValue];
    score --;
    self.teamGame.secondaryScore = [NSNumber numberWithInteger:score];
    [self updateTeamScores];
}

- (NSString *)teamName
{
    return self.teamGame.team.full_name;
}

- (void)setTeamName:(NSString *)teamName
{
    self.teamGame.team.full_name = teamName;
    [self updateTeamNames];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self updateTeamNames];
}

- (NSNumber *)teamScore
{
    return self.teamGame.score != nil ? self.teamGame.score : @0;
}

- (NSNumber *)teamSecondaryScore
{
    return self.teamGame.secondaryScore != nil ? self.teamGame.secondaryScore : @0;
}

- (UIColor *)teamColour
{
    Colour *teamColour = self.teamGame.assignedColour;
    return [UIColor colorWithRed:[teamColour.red floatValue] green:[teamColour.green floatValue] blue:[teamColour.blue floatValue] alpha:1];
}

- (void)updateTeamScores
{
    if (self.teamGame.match.externalURL) [self.gameManager updateGameScoresWithCompletion:nil];
}

- (void)updateTeamNames
{
    if (self.teamGame.match.externalURL) [self.gameManager updateTeamNames];
}

@end
