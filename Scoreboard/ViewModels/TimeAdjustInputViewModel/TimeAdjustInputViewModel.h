//
//  TimeAdjustInputViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 27/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@interface TimeAdjustInputViewModel : SBViewModel

@property (strong, nonatomic) NSNumber *time;

- (void)increaseMinutes;
- (void)increaseSeconds;
- (void)increaseTime;

- (void)decreaseMinutes;
- (void)decreaseSeconds;
- (void)decreaseTime;


@end
