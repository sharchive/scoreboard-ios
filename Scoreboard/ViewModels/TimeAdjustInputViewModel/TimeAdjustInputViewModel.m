//
//  TimeAdjustInputViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 27/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "TimeAdjustInputViewModel.h"

@implementation TimeAdjustInputViewModel

- (void)increaseMinutes
{
    NSInteger tempTime = [self.time integerValue];
    tempTime += 6000;
    self.time = [NSNumber numberWithInteger:tempTime];
}

- (void)increaseSeconds
{
    NSInteger tempTime = [self.time integerValue];
    tempTime += 100;
    self.time = [NSNumber numberWithInteger:tempTime];
}

- (void)increaseTime
{
    NSInteger tempTime = [self.time integerValue];
    tempTime ++;
    self.time = [NSNumber numberWithInteger:tempTime];
}

- (void)decreaseMinutes
{
    NSInteger tempTime = [self.time integerValue];
    if (tempTime > 0)
    {
        tempTime -= 6000;
        self.time = [NSNumber numberWithInteger:tempTime];
    }
}

- (void)decreaseSeconds
{
    NSInteger tempTime = [self.time integerValue];
    if (tempTime > 0)
    {
        tempTime -= 100;
        self.time = [NSNumber numberWithInteger:tempTime];
    }
}

- (void)decreaseTime
{
    NSInteger tempTime = [self.time integerValue];
    if (tempTime > 0)
    {
        tempTime --	;
        self.time = [NSNumber numberWithInteger:tempTime];
    }
}

@end
