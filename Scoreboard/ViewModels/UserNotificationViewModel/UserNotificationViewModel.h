//
//  UserNotificationViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 03/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBViewModel.h"

@class UserNotification;

@interface UserNotificationViewModel : SBViewModel

@property (strong, nonatomic) UserNotification *pendingNotification;
@property (strong, nonatomic) NSMutableArray *currentNotifications;

- (void)showMessage:(UserNotification *)message;

@end
