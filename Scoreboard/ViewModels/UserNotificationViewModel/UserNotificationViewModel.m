//
//  UserNotificationViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 03/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "UserNotificationViewModel.h"
#import "UserNotification.h"

@interface UserNotificationViewModel ()

@property (strong, nonatomic) NSTimer *timer;

@end

@implementation UserNotificationViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentNotifications = [NSMutableArray new];
    }
    return self;
}

- (void)showMessage:(UserNotification *)message
{
    self.pendingNotification = message;
}


@end
