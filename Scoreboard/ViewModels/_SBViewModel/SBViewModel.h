//
//  SBViewModel.h
//  Scoreboard
//
//  Created by Tom Bates on 12/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SHViewModel.h"
#import "SBPresenter.h"

@interface SBViewModel : SHViewModel

@property (strong, nonatomic) SBPresenter *localPresenter;

@end
