//
//  SBViewModel.m
//  Scoreboard
//
//  Created by Tom Bates on 12/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SBViewModel.h"

@implementation SBViewModel

- (SBPresenter *)localPresenter
{
    return (SBPresenter *)self.presenter;
}

@end
