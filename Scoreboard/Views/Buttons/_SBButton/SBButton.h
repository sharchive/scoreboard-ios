//
//  SBButton.h
//  Scoreboard
//
//  Created by Tom Bates on 23/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBButton : UIButton

@end
