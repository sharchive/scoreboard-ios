//
//  SBButton.m
//  Scoreboard
//
//  Created by Tom Bates on 23/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBButton.h"
#import "AppearanceManager.h"

@interface SBButton ()

@property (assign, nonatomic) NSInteger hitHeightDelta;
@property (assign, nonatomic) NSInteger hitWidthDelta;

@end

@implementation SBButton

- (void)awakeFromNib
{
    self.hitHeightDelta = 0;
    self.hitWidthDelta = 0;
    [self setFonts];
}

- (void)setFonts
{
    self.titleLabel.font = [AppearanceManager standardFont];
}

- (void)increaseHitAreaBy:(NSInteger)increase
{
    self.hitHeightDelta = increase;
    self.hitWidthDelta = increase;
}

- (void)increaseHitWidthBy:(NSInteger)increase
{
    self.hitWidthDelta = increase *-1;
}

- (void)increaseHitHeightBy:(NSInteger)increase
{
    self.hitHeightDelta = increase *-1;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    CGRect relativeFrame = self.bounds;
    if (self.bounds.size.width < 44)
    {
        self.hitWidthDelta = (44 - self.bounds.size.width) *-1;
    }
    if (self.bounds.size.height < 44)
    {
        self.hitHeightDelta = (44 - self.bounds.size.height) *-1;
    }
    UIEdgeInsets hitTestEdgeInsets = UIEdgeInsetsMake(self.hitHeightDelta, self.hitWidthDelta, self.hitHeightDelta, self.hitWidthDelta);
    CGRect hitFrame = UIEdgeInsetsInsetRect(relativeFrame, hitTestEdgeInsets);
    return CGRectContainsPoint(hitFrame, point);
}

@end
