//
//  PeriodsCollectionViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 22/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHCollectionViewCell.h"

@interface PeriodsCollectionViewCell : SHCollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (void)setOn:(BOOL)on;

@end
