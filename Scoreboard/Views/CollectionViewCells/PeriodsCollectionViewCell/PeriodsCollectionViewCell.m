//
//  PeriodsCollectionViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 22/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "PeriodsCollectionViewCell.h"

@implementation PeriodsCollectionViewCell

- (void)setOn:(BOOL)on
{
    if (on)
    {
        [self.imageView setImage:[UIImage imageNamed:@"periods_full"]];
    }
    else
    {
        [self.imageView setImage:[UIImage imageNamed:@"periods_clear"]];
    }
}

@end
