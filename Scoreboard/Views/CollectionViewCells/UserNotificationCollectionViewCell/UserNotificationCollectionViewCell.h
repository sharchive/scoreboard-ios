//
//  UserNotificationCollectionViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 03/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SHCollectionViewCell.h"

@class UserNotification;

@interface UserNotificationCollectionViewCell : SHCollectionViewCell

@property (strong, nonatomic) UserNotification *userNotification;
@property (strong, nonatomic) IBOutlet UIImageView *messageImageView;

@end
