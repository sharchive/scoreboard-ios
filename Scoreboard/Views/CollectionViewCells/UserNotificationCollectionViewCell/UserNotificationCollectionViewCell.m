//
//  UserNotificationCollectionViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 03/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "UserNotificationCollectionViewCell.h"
#import "UserNotification.h"
#import "SHCollectionViewDataSource.h"

@interface UserNotificationCollectionViewCell ()

@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) SHCollectionViewDataSource *localDataSource;

@end

@implementation UserNotificationCollectionViewCell

- (void)awakeFromNib
{
    self.titleLabel.font = [AppearanceManager standardFont];
}

- (void)setUserNotification:(UserNotification *)userNotification
{
    _userNotification = userNotification;
    self.titleLabel.text = userNotification.message;
    self.messageImageView.image = userNotification.icon;
    [self startTimer];
}

- (void)startTimer
{
    if (![self.timer isValid])
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(stopTimer) userInfo:nil repeats:false];
    }
}

- (void)stopTimer
{
    [self.timer invalidate];
    [self.localDataSource removeItem:self.userNotification];
}

- (SHCollectionViewDataSource *)localDataSource
{
    return (SHCollectionViewDataSource *) self.dataSource;
}

@end
