//
//  PeriodsCollectionView.h
//  Scoreboard
//
//  Created by Tom Bates on 24/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeriodsCollectionView : UICollectionView

@end
