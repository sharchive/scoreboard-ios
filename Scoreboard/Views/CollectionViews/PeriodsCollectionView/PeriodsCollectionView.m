//
//  PeriodsCollectionView.m
//  Scoreboard
//
//  Created by Tom Bates on 24/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "PeriodsCollectionView.h"
#import "PeriodsCollectionViewDataSource.h"
#import "GameManager.h"

@interface PeriodsCollectionView ()

@property (strong, nonatomic)UILongPressGestureRecognizer *longPress;
@property (strong, nonatomic)UIImageView *secondImageView;
@property (assign, nonatomic)float viewDelta;
@property (assign, nonatomic)BOOL monitorMovement;

@end

@implementation PeriodsCollectionView

- (void)awakeFromNib
{
    [self setAllowsSelection:false];
    self.viewDelta = 30.0;
    self.longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressEvent:)];
    self.monitorMovement = false;
    [self addGestureRecognizer:self.longPress];
}

- (void)longPressEvent:(UILongPressGestureRecognizer *)press
{
    switch (press.state) {
        case UIGestureRecognizerStateBegan:
            [self handlePressStart];
            break;
        
        case UIGestureRecognizerStateEnded:
            [self handlePressEnd];
            break;
        
        case UIGestureRecognizerStateChanged:
            [self handlePressMoved];
            break;
            
        case UIGestureRecognizerStateCancelled:
            [self handlePressCancelled];
            break;
        
        default:
            break;
    }
}

- (void)handlePressStart
{
    self.monitorMovement = true;
    UIImage *image = [self imageOfView];
    self.secondImageView = [[UIImageView alloc]initWithImage:image];
    CGRect secondFrame = self.frame;
    secondFrame.origin.y = (self.frame.origin.y - self.frame.size.height) - self.viewDelta;
    [self.secondImageView setFrame:secondFrame];
    
    [self.secondImageView setAlpha:0.5];
    [self.superview addSubview:self.secondImageView];
}

- (void)handlePressMoved
{
    PeriodsCollectionViewDataSource *dataSource = (PeriodsCollectionViewDataSource *) self.dataSource;
    NSInteger point = (NSInteger) [self.longPress locationInView:self].x;
    if (SB_DEVICE_IS_IPAD) point = (NSInteger) [self.longPress locationInView:self].y;
    
    NSInteger frameWidth = (NSInteger) self.frame.size.width;
    if (SB_DEVICE_IS_IPAD) frameWidth = (NSInteger) self.frame.size.height;
    NSInteger adjust = frameWidth / 7;
    
    NSInteger period = point/adjust;

    dataSource.gameManager.currentPeriod = period;
    [self updateImage];
}

- (void)handlePressEnd
{
    self.monitorMovement = false;
    [self.secondImageView removeFromSuperview];
    [self setSecondImageView:nil];
}

- (void)handlePressCancelled
{
    
}

- (void)updateImage
{
    self.secondImageView.image = [self imageOfView];
}

- (UIImage *) imageOfView
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

@end
