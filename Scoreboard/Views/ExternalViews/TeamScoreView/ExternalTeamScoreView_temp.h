//
//  ExternalTeamScoreView.h
//  Scoreboard
//
//  Created by Tom Bates on 27/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExternalTeamScoreViewModel_temp;

@interface ExternalTeamScoreView_temp : UIView

@property (strong, nonatomic) ExternalTeamScoreViewModel_temp *viewModel;
@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondaryScoreLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondaryConstraint;
@property (strong, nonatomic) IBOutlet UIView *secondaryView;

@end
