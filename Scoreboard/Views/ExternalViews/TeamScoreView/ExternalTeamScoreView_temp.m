//
//  ExternalTeamScoreView.m
//  Scoreboard
//
//  Created by Tom Bates on 27/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "ExternalTeamScoreView_temp.h"
#import "ExternalTeamScoreViewModel_temp.h"
#import "TeamInstance.h"
#import "Colour.h"

@implementation ExternalTeamScoreView_temp

- (void)applyBindings
{
    [self.viewModel addObserver:self forKeyPath:@"teamGame.score" options:NSKeyValueObservingOptionNew context:nil];
    [self.viewModel addObserver:self forKeyPath:@"teamGame.secondaryScore" options:NSKeyValueObservingOptionNew context:nil];
    [self.viewModel addObserver:self forKeyPath:@"teamGame.team.full_name" options:NSKeyValueObservingOptionNew context:nil];
    [self.viewModel addObserver:self forKeyPath:@"teamGame.assignedColour" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)setViewModel:(ExternalTeamScoreViewModel_temp *)viewModel
{
    _viewModel = viewModel;
    [self applyBindings];
}

- (void)adjustColour
{
    self.backgroundColor = self.viewModel.teamColour;
    self.secondaryView.backgroundColor = self.viewModel.teamColour;
}

- (void)setScoreLabelText
{
    self.scoreLabel.text = [self.viewModel.teamScore stringValue];
    self.secondaryScoreLabel.text = [self.viewModel.teamSecondaryScore stringValue];
}

- (void)setNameFieldText
{
    self.nameField.text = self.viewModel.teamName;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"teamGame.score"])
    {
        [self setScoreLabelText];
    }
    else if ([keyPath isEqualToString:@"teamGame.secondaryScore"])
    {
        [self setScoreLabelText];
    }
    else if ([keyPath isEqualToString:@"teamGame.assignedColour"])
    {
        [self adjustColour];
    }
    else
    {
        [self setNameFieldText];
    }
}

@end
