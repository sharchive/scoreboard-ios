//
//  SBLabel.h
//  Scoreboard
//
//  Created by Tom Bates on 03/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBLabel : UILabel

- (void)adjustIndent:(float)indent;

@end
