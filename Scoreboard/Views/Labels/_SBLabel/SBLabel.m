//
//  SBLabel.m
//  Scoreboard
//
//  Created by Tom Bates on 03/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SBLabel.h"

@interface SBLabel ()

@property (assign, nonatomic) float topIndent;

@end

@implementation SBLabel

- (void)awakeFromNib
{
    if (!self.topIndent) self.topIndent = 5.0;
}

- (void)adjustIndent:(float)indent
{
    self.topIndent = indent;
    self.text = self.text;
    [self drawTextInRect:self.frame];
}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {self.topIndent, 0, 0, 0};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
