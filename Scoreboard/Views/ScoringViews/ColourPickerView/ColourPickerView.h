//
//  ColourPickerView.h
//  Scoreboard
//
//  Created by Tom Bates on 13/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TeamInstance;

@interface ColourPickerView : UIView

@property (strong, nonatomic) TeamInstance *teamGame;

@end
