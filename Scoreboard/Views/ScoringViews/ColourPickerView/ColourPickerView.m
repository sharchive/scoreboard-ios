//
//  ColourPickerView.m
//  Scoreboard
//
//  Created by Tom Bates on 13/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "ColourPickerView.h"
#import "TeamInstance.h"
#import "Colour.h"
#import "ColourService.h"

@interface ColourPickerView ()

@property (strong, nonatomic) ColourService *colourService;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIImageView *colourPickerImageView;

@end

@implementation ColourPickerView

- (void)awakeFromNib
{
    self.colourService = [ColourService new];
    UIImage *image_bg = [[UIImage imageNamed:@"black_transparent_bg"]resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    self.backgroundImageView.image = image_bg;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint loc = [touch locationInView:self.colourPickerImageView];
    [self setTeamColourFromPoint:loc];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint loc = [touch locationInView:self.colourPickerImageView];
    [self setTeamColourFromPoint:loc];
}

-(void)setTeamColourFromPoint:(CGPoint)point
{
    unsigned char pixel[4] = {0};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixel,
                                                 1, 1, 8, 4, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    float newY = (self.colourPickerImageView.frame.size.height/2);
    CGContextTranslateCTM(context, -point.x, -newY);
    
    [self.colourPickerImageView.layer renderInContext:context];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    Colour *colour = self.teamGame.assignedColour;
    colour.redValue = pixel[0]/255.0;
    colour.greenValue = pixel[1]/255.0;
    colour.blueValue = pixel[2]/255.0;
    self.teamGame.assignedColour = colour;
}

@end
