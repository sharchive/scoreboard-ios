//
//  ExternalTeamScoreView.h
//  Scoreboard
//
//  Created by Tom Bates on 26/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "TeamScoreView.h"

@interface ExternalTeamScoreView : TeamScoreView

@end
