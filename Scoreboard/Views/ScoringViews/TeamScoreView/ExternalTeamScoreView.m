//
//  ExternalTeamScoreView.m
//  Scoreboard
//
//  Created by Tom Bates on 26/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "ExternalTeamScoreView.h"

@implementation ExternalTeamScoreView

- (void)setAppearance
{
    [self.scoreLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"big_score_bg_external"]]];
    [self.secondaryScoreLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"small_score_bg_external"]]];
}

- (void)setFonts
{
    self.nameField.font = [AppearanceManager externalLargeFont];
    self.scoreLabel.font = [AppearanceManager externalLargeFont];
    self.secondaryScoreLabel.font = [AppearanceManager externalMediumFont];
    
    UIEdgeInsets insets = {20, 0, 0, 0};
    [self.nameField drawTextInRect:UIEdgeInsetsInsetRect(self.secondaryScoreLabel.frame, insets)];
    [self.secondaryScoreLabel drawTextInRect:UIEdgeInsetsInsetRect(self.secondaryScoreLabel.frame, insets)];
}

@end
