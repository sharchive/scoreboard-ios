//
//  TeamScoreView.h
//  Scoreboard
//
//  Created by Tom Bates on 07/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamScoreViewModel.h"

@interface TeamScoreView : UIView <UITextFieldDelegate>

@property (strong, nonatomic) TeamScoreViewModel *viewModel;
@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondaryScoreLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondaryConstraint;
@property (strong, nonatomic) IBOutlet UIView *secondaryView;

@end
