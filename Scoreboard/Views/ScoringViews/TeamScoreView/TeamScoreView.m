//
//  TeamScoreView.m
//  Scoreboard
//
//  Created by Tom Bates on 07/04/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "TeamScoreView.h"
#import "TeamScoreViewModel.h"
#import "ColourPickerView.h"

@implementation TeamScoreView

- (void)awakeFromNib
{
    [self setUpLayers];
    [self setAppearance];
    [self setFonts];
    self.nameField.delegate = self;
}

- (void)setFonts
{
    self.nameField.font = [AppearanceManager largeFont];
    self.scoreLabel.font = [AppearanceManager largeFont];
    self.secondaryScoreLabel.font = [AppearanceManager mediumFont];
    UIEdgeInsets insets = {20, 0, 0, 0};
    [self.secondaryScoreLabel drawTextInRect:UIEdgeInsetsInsetRect(self.secondaryScoreLabel.frame, insets)];
}

- (void)setAppearance
{
    [self.scoreLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"big_score_bg"]]];
    [self.secondaryScoreLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"small_score_bg"]]];
}

- (void)setInputAccessory
{
    NSString *storyboardName = [[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:[NSBundle mainBundle]];
    ColourPickerView *colourView = (ColourPickerView *)[[storyboard instantiateViewControllerWithIdentifier:@"ColourPickerViewController"]view];
    colourView.teamGame = self.viewModel.teamGame;
    [self.nameField setInputAccessoryView:colourView];
}

- (void)setUpLayers
{
    if (SB_DEVICE_IS_IPHONE)
    {
        self.layer.cornerRadius = 20;
        self.secondaryView.layer.cornerRadius = 20;
    }
    else
    {
        self.layer.cornerRadius = 40;
        self.secondaryView.layer.cornerRadius = 40;
    }
    self.scoreLabel.layer.cornerRadius = 10;
    self.secondaryScoreLabel.layer.cornerRadius = 10;
}

- (void)setSwipeGestures
{
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc]initWithTarget:self.viewModel action:@selector(increaseTeamScore)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;

    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc]initWithTarget:self.viewModel action:@selector(decreaseTeamScore)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.scoreLabel setGestureRecognizers:@[swipeDown, swipeUp]];

    UISwipeGestureRecognizer *swipeSecondaryUp = [[UISwipeGestureRecognizer alloc]initWithTarget:self.viewModel action:@selector(increaseTeamSecondary)];
    swipeSecondaryUp.direction = UISwipeGestureRecognizerDirectionUp;
    
    UISwipeGestureRecognizer *swipeSecondaryDown = [[UISwipeGestureRecognizer alloc]initWithTarget:self.viewModel action:@selector(decreaseTeamSecondary)];
    swipeSecondaryDown.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.secondaryScoreLabel setGestureRecognizers:@[swipeSecondaryUp, swipeSecondaryDown]];
    [self.secondaryView setGestureRecognizers:@[swipeSecondaryUp, swipeSecondaryDown]];

}

- (void)setViewModel:(TeamScoreViewModel *)viewModel
{
    _viewModel = viewModel;
    [self setSwipeGestures];
    [self adjustColour];
    [self setScoreLabelText];
    [self applyBindings];
    [self setInputAccessory];
}

- (void)applyBindings
{
    [self.viewModel addObserver:self
                     forKeyPath:@"teamGame.score"
                        options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                        context:nil];
    
    [self.viewModel addObserver:self
                     forKeyPath:@"teamGame.secondaryScore"
                        options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                        context:nil];
    
    [self.viewModel addObserver:self
                     forKeyPath:@"teamGame.team.full_name"
                        options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                        context:nil];
    
    [self.viewModel addObserver:self
                     forKeyPath:@"teamGame.assignedColour"
                        options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                        context:nil];
}

- (void)adjustColour
{
    self.backgroundColor = self.viewModel.teamColour;
    self.secondaryView.backgroundColor = self.viewModel.teamColour;
}

- (void)setScoreLabelText
{
    self.scoreLabel.text = [self.viewModel.teamScore stringValue];
    self.secondaryScoreLabel.text = [self.viewModel.teamSecondaryScore stringValue];
}

- (void)setNameFieldText
{
    self.nameField.text = self.viewModel.teamName;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.viewModel.teamName = textField.text;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"teamGame.score"])
    {
        [self setScoreLabelText];
    }
    else if ([keyPath isEqualToString:@"teamGame.secondaryScore"])
    {
        [self setScoreLabelText];
    }
    else if ([keyPath isEqualToString:@"teamGame.assignedColour"])
    {
        [self adjustColour];
    }
    else
    {
        [self setNameFieldText];
    }
}


@end
