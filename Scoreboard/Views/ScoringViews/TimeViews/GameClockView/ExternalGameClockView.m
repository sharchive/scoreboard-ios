//
//  ExternalGameClockView.m
//  Scoreboard
//
//  Created by Tom Bates on 26/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "ExternalGameClockView.h"

@implementation ExternalGameClockView

- (void)setAppearance
{
    float fontSize = 60;
    UIFont *largeFont = [UIFont fontWithName:[AppearanceManager mainFontName] size:fontSize];
    self.label1.font = largeFont;
    self.label2.font = largeFont;
    self.label3.font = largeFont;
    self.label4.font = largeFont;
    
    fontSize = 35;
    UIFont *smallFont = [UIFont fontWithName:[AppearanceManager mainFontName] size:fontSize];
    [self.label5 adjustIndent:1.0];
    self.label5.font = smallFont;
    [self.label6 adjustIndent:1.0];
    self.label6.font = smallFont;
    
    [self.colonLabel adjustIndent:0.5];
}


@end
