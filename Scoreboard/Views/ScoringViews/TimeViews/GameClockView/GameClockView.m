//
//  GameClockView.m
//  Scoreboard
//
//  Created by Tom Bates on 07/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "GameClockView.h"
#import "GameTimeManager.h"

static NSString *remainingBindString = @"remainingPeriodTime";
static NSString *elapsedBindString = @"elapsedPeriodTime";

@implementation GameClockView

- (void)applyBinding
{
    [super applyBinding];
    if (self.gameTimeManager.periodCountup == true) {
        [self.gameTimeManager addObserver:self forKeyPath:elapsedBindString options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    }
    else
    {
        [self.gameTimeManager addObserver:self forKeyPath:remainingBindString options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
    }
}

- (void)singleTapped
{
    [self.gameTimeManager startStopTimer];
}

- (void)doubleTapped
{
    
}

- (void)longPressed
{
    [self.gameTimeManager resetTimes];
}

@end
