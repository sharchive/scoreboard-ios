//
//  ShotClockView.m
//  Scoreboard
//
//  Created by Tom Bates on 07/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "ShotClockView.h"
#import "NSNumber+TimeAdditions.h"
#import "GameTimeManager.h"

static NSString *timeBindString = @"remainingShotTime";

@implementation ShotClockView

- (void)applyBinding
{
    [super applyBinding];
    [self.gameTimeManager addObserver:self forKeyPath:timeBindString options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
}

- (void)singleTapped
{
    [self.gameTimeManager resetShotTime];
}

- (void)doubleTapped
{
    [self.gameTimeManager startStopTimer];
}

- (void)setAppearance
{
    [super setAppearance];
    
    if (SB_DEVICE_IS_IPAD)
    {
    float fontSize = 35;
    UIFont *largeFont = [UIFont fontWithName:[AppearanceManager mainFontName] size:fontSize];
    self.label1.font = largeFont;
    self.label2.font = largeFont;
    self.label3.font = largeFont;
    self.label4.font = largeFont;
    
    fontSize = 25;
    UIFont *smallFont = [UIFont fontWithName:[AppearanceManager mainFontName] size:fontSize];
    [self.label5 adjustIndent:1.0];
    self.label5.font = smallFont;
    [self.label6 adjustIndent:1.0];
    self.label6.font = smallFont;
    
    [self.colonLabel adjustIndent:0.5];
    }
}

@end
