//
//  ScoreTimerView.h
//  Scoreboard
//
//  Created by Tom Bates on 07/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBLabel.h"

@class GameTimeManager;

@interface ClockView : UIView

@property (strong, nonatomic) GameTimeManager *gameTimeManager;
@property (strong, nonatomic) IBOutlet SBLabel *label1;
@property (strong, nonatomic) IBOutlet SBLabel *label2;
@property (strong, nonatomic) IBOutlet SBLabel *label3;
@property (strong, nonatomic) IBOutlet SBLabel *label4;
@property (strong, nonatomic) IBOutlet SBLabel *label5;
@property (strong, nonatomic) IBOutlet SBLabel *label6;
@property (strong, nonatomic) IBOutlet SBLabel *colonLabel;

- (void)updateTextLabels:(NSString *)string;

- (void)applyBinding;

- (void)longPressed;

- (void)doubleTapped;

- (void)setAppearance;

@end
