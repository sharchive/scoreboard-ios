//
//  ScoreTimerView.m
//  Scoreboard
//
//  Created by Tom Bates on 07/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "ClockView.h"
#import "GameTimeManager.h"
#import "SBLabel.h"
#import "SBConstants.h"
#import "NSNumber+TimeAdditions.h"

@implementation ClockView

- (void)awakeFromNib
{
    [self setupGestures];
    [self setAppearance];
}

- (void)setAppearance
{
    float fontSize = 30;
    if (SB_DEVICE_IS_IPAD) fontSize = 60;
    UIFont *largeFont = [UIFont fontWithName:[AppearanceManager mainFontName] size:fontSize];
    self.label1.font = largeFont;
    self.label2.font = largeFont;
    self.label3.font = largeFont;
    self.label4.font = largeFont;

    fontSize = 15;
    if (SB_DEVICE_IS_IPAD) fontSize = 35;
    UIFont *smallFont = [UIFont fontWithName:[AppearanceManager mainFontName] size:fontSize];
    [self.label5 adjustIndent:1.0];
    self.label5.font = smallFont;
    [self.label6 adjustIndent:1.0];
    self.label6.font = smallFont;

    [self.colonLabel adjustIndent:0.5];
}

- (void)setupGestures
{
    UITapGestureRecognizer *singleTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapped)];
    [singleTapGesture setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:singleTapGesture];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapped)];
    [doubleTap setNumberOfTouchesRequired:2];
    [self addGestureRecognizer:doubleTap];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressed)];
    [longPress setMinimumPressDuration:0.5];
    [self addGestureRecognizer:longPress];
    
    self.userInteractionEnabled = true;
}

- (void)setGameTimeManager:(GameTimeManager *)gameTimeManager
{
    _gameTimeManager = gameTimeManager;
    [self applyBinding];
}

- (void)updateTextLabels:(NSString *)string
{
    self.label1.text = [string substringWithRange:NSMakeRange(0, 1)];
    self.label2.text = [string substringWithRange:NSMakeRange(1, 1)];
    self.label3.text = [string substringWithRange:NSMakeRange(2, 1)];
    self.label4.text = [string substringWithRange:NSMakeRange(3, 1)];
    self.label5.text = [string substringWithRange:NSMakeRange(4, 1)];
    self.label6.text = [string substringWithRange:NSMakeRange(5, 1)];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSNumber *time = [change objectForKey:NSKeyValueChangeNewKey];
    if (time >= 0)
    {
        [self updateTextLabels:[time bigClockString]];
    }
}

- (void)applyBinding
{
    
}

- (void)singleTapped
{
    
}

- (void)doubleTapped
{
    
}

- (void)longPressed
{
    
}


@end
