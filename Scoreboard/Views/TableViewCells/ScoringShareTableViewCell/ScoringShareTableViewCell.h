//
//  ScoringShareTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 16/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBTableViewCell.h"

@interface ScoringShareTableViewCell : SBTableViewCell

@end
