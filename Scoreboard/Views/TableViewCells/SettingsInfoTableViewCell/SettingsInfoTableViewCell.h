//
//  SettingsInfoTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 11/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SHTableViewCell.h"

@interface SettingsInfoTableViewCell : SHTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *buildTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *buildVersionLabel;
@property (strong, nonatomic) IBOutlet UILabel *buildDateLabel;

@end
