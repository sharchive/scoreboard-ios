//
//  SettingsInfoTableViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 11/05/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SettingsInfoTableViewCell.h"

@implementation SettingsInfoTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.userInteractionEnabled = false;
    [self setLabelText];
}

- (void)setFonts
{
    self.buildDateLabel.font = [AppearanceManager smallFont];
    self.buildTypeLabel.font = [AppearanceManager smallFont];
    self.buildVersionLabel.font = [AppearanceManager smallFont];
}

- (void)setLabelText
{
    NSString *build_date = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBuildDate"];

    self.buildDateLabel.text = build_date;
    
    NSString *build_type = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBuildType"];
    self.buildTypeLabel.text = [build_type uppercaseString];
    NSString *build_version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    self.buildVersionLabel.text = build_version;
}

@end
