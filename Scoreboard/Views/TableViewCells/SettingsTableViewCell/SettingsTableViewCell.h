//
//  SettingsTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 11/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBTableViewCell.h"

@interface SettingsTableViewCell : SBTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
