//
//  SocialSettingsTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBTableViewCell.h"

@class SBSocialManager;

@interface SocialSettingsTableViewCell : SBTableViewCell

@property (strong, nonatomic) SBSocialManager *socialManager;
@property (strong, nonatomic) IBOutlet UISwitch *connectSwitch;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)connectSwitchPressed:(UISwitch *)sender;

@end
