//
//  SocialSettingsTableViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 12/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SocialSettingsTableViewCell.h"
#import "SBSocialManager.h"

@implementation SocialSettingsTableViewCell

- (void)awakeFromNib
{
    [self.connectSwitch.layer setCornerRadius:16];
}

- (void)setSocialManager:(SBSocialManager *)socialManager
{
    _socialManager = socialManager;
    self.titleLabel.text = socialManager.title;
    @weakify (self)
    [RACObserve(socialManager, currentStatus)subscribeNext:^(NSNumber *x) {
        @strongify(self)
        SocialConnectionStatus status = (SocialConnectionStatus) [x intValue];
        [self.connectSwitch setOn:status == SCSConnected];
    }];
}

- (IBAction)connectSwitchPressed:(UISwitch *)sender
{
    if (self.socialManager.currentStatus == SCSConnected) [self.socialManager logout];
    else [self.socialManager login];
}


@end
