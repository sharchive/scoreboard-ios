//
//  SportListTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBTableViewCell.h"

@class SHSport;
@class SportsListTableViewDataSource;

@interface SportsListTableViewCell : SBTableViewCell

@property (strong, nonatomic) SHSport *sport;
@property (strong, nonatomic) SportsListTableViewDataSource *dataSource;

- (void)beginEditing;

@end
