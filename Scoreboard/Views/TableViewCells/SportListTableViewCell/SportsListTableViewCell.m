//
//  SportListTableViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportsListTableViewCell.h"
#import "SHSport.h"
#import "AppearanceManager.h"
#import "SportService.h"
#import "SportsListTableViewDataSource.h"

@interface SportsListTableViewCell () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *titleField;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPressGesture;

@end

@implementation SportsListTableViewCell

- (void)awakeFromNib
{
    self.titleField.delegate = self;
    [self.contentView addGestureRecognizer:self.longPressGesture];
}

- (void)setLabelText
{
    self.titleField.text = self.sport.name;
}

- (void)setFonts
{
    self.titleField.font = [AppearanceManager standardFont];
}

- (void)beginEditing
{
    [self.titleField setUserInteractionEnabled:true];
    [self.titleField becomeFirstResponder];
    [self setFonts];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) self.titleField.textColor = [AppearanceManager aquaBlueColour];
    else self.titleField.textColor = [UIColor whiteColor];
    [self.titleField resignFirstResponder];
    [self setFonts];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) self.titleField.textColor = [AppearanceManager aquaBlueColour];
    else self.titleField.textColor = [UIColor whiteColor];
    
    [self setFonts];
}

- (void)setSport:(SHSport *)sport
{
    _sport = sport;
    [self.longPressGesture setEnabled:self.sport.editableValue];
    [self setLabelText];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return self.sport.editableValue;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BOOL valid = textField.text.length > 0;
    if (valid) [textField resignFirstResponder];
    return valid;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.sport.name = textField.text;
    [[SportService new]save];
    [self.titleField setUserInteractionEnabled:false];
}

- (void)longPressed
{
    [self.dataSource selectIndexPath:self.indexPath];
    [self beginEditing];
}

- (UILongPressGestureRecognizer *)longPressGesture
{
    if (!_longPressGesture)
    {
        _longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed)];
    }
    return _longPressGesture;
}

@end
