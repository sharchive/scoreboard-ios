//
//  SettingsPeriodsTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsTableViewCell.h"

@class SBButton;

@interface SportSettingsPeriodsTableViewCell : SportSettingsTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *periodNumberLabel;

@property (strong, nonatomic) IBOutlet SBButton *plusButton;
@property (strong, nonatomic) IBOutlet SBButton *minusButton;

- (IBAction)increasePeriodButtonPressed:(UIButton *)sender;
- (IBAction)decreasePeriodButtonPressed:(UIButton *)sender;

@end
