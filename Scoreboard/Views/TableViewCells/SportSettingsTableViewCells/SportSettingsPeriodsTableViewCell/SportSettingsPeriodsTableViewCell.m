//
//  SettingsPeriodsTableViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsPeriodsTableViewCell.h"
#import "SHSport.h"
#import "SBButton.h"

@interface SportSettingsPeriodsTableViewCell ()

@end

@implementation SportSettingsPeriodsTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.periodNumberLabel.layer.cornerRadius = 3.0;
    [self.periodNumberLabel setBackgroundColor:[UIColor colorWithPatternImage:[AppearanceManager settingsLabelImage]]];
}

- (void)setFonts
{
    [super setFonts];
    self.periodNumberLabel.font = [AppearanceManager mediumFont];
}

- (void)setSport:(SHSport *)sport
{
    [super setSport:sport];
    [self setPeriodTitle];
}

- (IBAction)increasePeriodButtonPressed:(UIButton *)sender
{
    [self.sport increasePeriods];
    [self setPeriodTitle];
}

- (IBAction)decreasePeriodButtonPressed:(UIButton *)sender
{
    [self.sport decreasePeriods];
    [self setPeriodTitle];
}

- (void)setPeriodTitle
{
    self.periodNumberLabel.text = [self.sport.periods stringValue];
}

@end
