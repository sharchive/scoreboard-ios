//
//  SportSettingsSwitchTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsTableViewCell.h"

@interface SportSettingsSwitchTableViewCell : SportSettingsTableViewCell

@property (strong, nonatomic) IBOutlet UISwitch *cellSwitch;

- (IBAction)cellSwitched:(id)sender;

@end
