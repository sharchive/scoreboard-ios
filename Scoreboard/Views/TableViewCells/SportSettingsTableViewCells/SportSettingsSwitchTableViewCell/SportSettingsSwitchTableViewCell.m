//
//  SportSettingsSwitchTableViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsSwitchTableViewCell.h"
#import "SHSport.h"

@implementation SportSettingsSwitchTableViewCell

- (void)awakeFromNib
{
    [self.cellSwitch.layer setCornerRadius:16];
}

- (void)sportUpdated
{
    [super sportUpdated];
    NSNumber *propertyObject = [self.sport valueForKeyPath:self.cellItem[@"property"]];
    BOOL propertyOn = [propertyObject boolValue];
    [self.cellSwitch setOn:propertyOn animated:false];
}

- (IBAction)cellSwitched:(id)sender
{
    [self.sport setValue:[NSNumber numberWithBool:[sender isOn]] forKey:self.cellItem[@"property"]];
    if ([sender isOn])
    {
        [self.tableView insertRowsAtIndexPaths:[self cellArray] withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:true];
    }
    else
    {
        [self.tableView deleteRowsAtIndexPaths:[self cellArray] withRowAnimation:UITableViewRowAnimationRight];
    }
}

- (NSMutableArray *)cellArray
{
    NSArray *subCells = self.cellItem[@"cells"];
    NSMutableArray *indexes = [[NSMutableArray alloc]init];
    
    [subCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSIndexPath *path = [NSIndexPath indexPathForItem:idx + 1 inSection:self.indexPath.section];
        [indexes addObject:path];
    }];
    
    return indexes;
}

@end
