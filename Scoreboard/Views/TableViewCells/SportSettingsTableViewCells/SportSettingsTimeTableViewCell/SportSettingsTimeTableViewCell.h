//
//  SportSettingsTimeTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsTableViewCell.h"

@interface SportSettingsTimeTableViewCell : SportSettingsTableViewCell

@property (strong, nonatomic) IBOutlet UIButton *timeButton;

- (IBAction)timeButtonPressed:(UIButton *)sender;

@end
