//
//  SportSettingsTimeTableViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsTimeTableViewCell.h"
#import "TimeAdjustInputViewModel.h"
#import "SHSport.h"
#import "NSNumber+TimeAdditions.h"

@interface SportSettingsTimeTableViewCell () <UITextFieldDelegate>

@property (strong, nonatomic) UITextField *hiddenTextField;
@property (strong, nonatomic) TimeAdjustInputViewModel *adjustViewModel;

@end

@implementation SportSettingsTimeTableViewCell

- (void)awakeFromNib
{
    [self createHiddenTextField];
    [super awakeFromNib];
    [self.timeButton setBackgroundColor:[UIColor colorWithPatternImage:[AppearanceManager settingsLabelImage]]];
//    self.timeButton.enabled = self.sport.userGeneratedValue;
    self.timeButton.layer.cornerRadius = 3.0;
}

- (void)setFonts
{
    [super setFonts];
    [self.timeButton.titleLabel setFont:[AppearanceManager mediumFont]];
}

- (void)createHiddenTextField
{
    self.hiddenTextField = [[UITextField alloc]initWithFrame:CGRectZero];
    self.hiddenTextField.delegate = self;
    [self addSubview:self.hiddenTextField];
    self.adjustViewModel = [TimeAdjustInputViewModel new];
    self.hiddenTextField.inputView = self.adjustViewModel.view;
}

- (void)applyBindings
{
    
    [self.adjustViewModel addObserver:self forKeyPath:@"time"
                              options:NSKeyValueObservingOptionNew
                              context:NULL];
}

- (IBAction)timeButtonPressed:(UIButton *)sender
{
    self.adjustViewModel.time = [self.sport valueForKey:self.cellItem[@"property"]];
    [self.hiddenTextField becomeFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    CGRect frameInView = [self convertRect:self.frame toView:self.tableView.superview];
    //    CGPoint bottomLeft = [self bottomLeftForRect:frameInView];
    //    if (bottomLeft.y > 200)
    //    {
    //        CGRect newTableFrame = self.tableView.frame;
    //        newTableFrame.origin.y -= 100;
    //        self.tableView.frame = newTableFrame;
    //    }
}

- (CGPoint)bottomLeftForRect:(CGRect)frame
{
    float originY = frame.origin.y;
    float bottomY = originY + frame.size.height;
    return CGPointMake(frame.origin.x, bottomY);
}

- (void)setSport:(SHSport *)sport
{
    [super setSport:sport];
    NSNumber *time = [self.sport valueForKey:self.cellItem[@"property"]];
    [self setButtonTitle:time];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSNumber *time = [change objectForKey:NSKeyValueChangeNewKey];
    [self.sport setValue:time forKey:self.cellItem[@"property"]];
    [self setButtonTitle:time];
}

- (void)setButtonTitle:(NSNumber *)time
{
    NSString *string = [NSString stringWithFormat:@"%@:%@:%@",[time minutesString], [time secondsString], [time milliString]];
    [self.timeButton setTitle:string forState:UIControlStateNormal];
}

@end
