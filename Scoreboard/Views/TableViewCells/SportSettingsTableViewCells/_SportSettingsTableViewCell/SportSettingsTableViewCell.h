//
//  SportSettingsTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SBTableViewCell.h"

@class SHSport;

@interface SportSettingsTableViewCell : SBTableViewCell

@property (strong, nonatomic) NSDictionary *cellItem;
@property (strong, nonatomic) SHSport *sport;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *indexPath;

- (void)sportUpdated;
- (void)applyBindings;

@end
