//
//  SportSettingsTableViewCell.m
//  Scoreboard
//
//  Created by Tom Bates on 22/03/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import "SportSettingsTableViewCell.h"
#import "SHSport.h"

@implementation SportSettingsTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self applyBindings];
}

- (void)applyBindings{}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)sportUpdated
{
    self.titleLabel.text = NSLocalizedString(self.cellItem[@"title"], nil);

}

- (void)setSport:(SHSport *)sport
{
    _sport = sport;
    [self sportUpdated];
}

@end
