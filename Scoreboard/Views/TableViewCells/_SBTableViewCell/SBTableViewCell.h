//
//  SBTableViewCell.h
//  Scoreboard
//
//  Created by Tom Bates on 30/04/2014.
//  Copyright (c) 2014 Sport Hub. All rights reserved.
//

#import "SHTableViewCell.h"

@interface SBTableViewCell : SHTableViewCell

@property (strong, nonatomic)IBOutlet UILabel *titleLabel;

- (void)setFonts;

@end
