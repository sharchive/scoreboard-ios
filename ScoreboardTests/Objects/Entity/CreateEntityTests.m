//
//  CreateEntityTests.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CoreDataManager.h"
#import "_SHEntity.h"
#import "SHRepository.h"

@interface CreateEntityTests : XCTestCase

@property (strong,nonatomic)_SHEntity *createdEntity;

@end

@implementation CreateEntityTests

- (void)setUp
{
    [super setUp];
    [CoreDataManager createTestStore];
    SHRepository *repo = [SHRepository newRepository:[_SHEntity class]];
    self.createdEntity = [repo createNewEntity];
}

- (void)tearDown
{
    [CoreDataManager clearTestStore];
    [super tearDown];
}

- (void)test_entity_created_date_is_no_nil
{
    XCTAssertNotNil(self.createdEntity.dateCreatedUTC, @"entity dateCreatedUTC should not be nil");
}

- (void)test_entity_updated_date_is_no_nil
{
    XCTAssertNotNil(self.createdEntity.dateUpdatedUTC, @"entity dateUpdatedUTC should not be nil");
}

- (void)test_entity_has_no_database_id
{
    XCTAssertNil(self.createdEntity.guid, @"entity should no have a guid from the database");
}

- (void)test_entity_push_required_is_true
{
    XCTAssertTrue(self.createdEntity.pushRequiredValue, @"entity should require a push to the server");
}

- (void)test_entity_soft_delete_is_false
{
    XCTAssertFalse(self.createdEntity.softDeletedValue, @"entity should not have been soft deleted");
}

@end
