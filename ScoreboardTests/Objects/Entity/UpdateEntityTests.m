//
//  UpdateEntityTests.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CoreDataManager.h"
#import "_SHEntity.h"
#import "SHRepository.h"

@interface UpdateEntityTests : XCTestCase

@property (strong,nonatomic)_SHEntity          *createdEntity;
@property (assign,nonatomic)NSTimeInterval  createdUpdateInterval;
@property (assign,nonatomic)NSTimeInterval  updatedUpdateInterval;

@end

@implementation UpdateEntityTests

- (void)setUp
{
    [super setUp];
    [CoreDataManager createTestStore];
    SHRepository *repo = [SHRepository newRepository:[_SHEntity class]];
    self.createdEntity = [repo createNewEntity];
    self.createdEntity.pushRequiredValue = false;
    self.createdEntity.dateCreatedUTC = [NSDate dateWithTimeIntervalSince1970:1000];
    self.createdEntity.dateUpdatedUTC = [NSDate dateWithTimeIntervalSince1970:1000];
    self.createdUpdateInterval = [self.createdEntity.dateUpdatedUTC timeIntervalSince1970];
    self.createdEntity.softDeletedValue = true;
    self.updatedUpdateInterval = [self.createdEntity.dateUpdatedUTC timeIntervalSince1970];
    
}

- (void)tearDown
{
    [CoreDataManager clearTestStore];
    [super tearDown];
}

- (void)test_entity_updated_date_is_not_nil
{
    XCTAssertNotNil(self.createdEntity.dateUpdatedUTC, @"entity dateUpdatedUTC should not be nil");
}

- (void)test_entity_updated_should_not_be_the_same
{
    XCTAssertNotEqual(self.createdUpdateInterval, self.updatedUpdateInterval, @"update time should be updated when value changes");
}

- (void)test_entity_push_required_is_true
{
    XCTAssertTrue(self.createdEntity.pushRequiredValue, @"entity should require a push to the server");
}

@end
