//
//  CreateSportFromObjectTests.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SportService.h"
#import "CoreDataManager.h"
#import "SHSport.h"

@interface CreateSportFromObjectTests : XCTestCase

@property (strong, nonatomic)SportService *subject;
@property (strong, nonatomic)SHSport *createdSport;

@end

@implementation CreateSportFromObjectTests

- (void)setUp
{
    [super setUp];
    [CoreDataManager createTestStore];
    NSDictionary *exactSportObject = @{@"category": @"Football",
                              @"name": @"Default",
                              @"scoreboard": @"generic",
                              @"minTeams": @2,
                              @"maxTeams": @2,
                              @"periods": @2,
                              @"gameClockEnabled": @1,
                              @"gameClockCountUp": @1,
                              @"gameClockTime": @270000,
                              @"gameClockBuzzerEnabled": @0,
                              @"shotClockEnabled": @0};
    
    self.subject = [SportService new];
    self.createdSport = [self.subject createSportsFromDictionary:exactSportObject];
}

- (void)tearDown
{
    [CoreDataManager clearTestStore];
    [super tearDown];
}

- (void)test_sport_is_not_nil
{
    XCTAssertNotNil(self.createdSport, @"created sport should not be nil");
}

@end
