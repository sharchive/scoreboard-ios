//
//  CreateSportTests.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SportService.h"
#import "Sport.h"
#import "CoreDataManager.h"


@interface CreateSportTests : XCTestCase

@property (strong, nonatomic)SportService *subject;
@property (strong, nonatomic)SHSport *createdSport;

@end

@implementation CreateSportTests

- (void)setUp
{
    [super setUp];
    [CoreDataManager createTestStore];
    self.subject = [SportService new];
    self.createdSport = [self.subject createSport];
}

- (void)tearDown
{
    [CoreDataManager clearTestStore];
    [super tearDown];
}

- (void)test_sport_is_not_nil
{
    XCTAssertNotNil(self.createdSport, @"created sport should not be nil");
}

@end
