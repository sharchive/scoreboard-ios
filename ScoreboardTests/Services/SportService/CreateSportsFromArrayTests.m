//
//  CreateSportsFromArrayTests.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SportService.h"
#import "CoreDataManager.h"
#import "SHSport.h"

@interface CreateSportsFromArrayTests : XCTestCase

@property (strong, nonatomic)SportService *subject;
@property (strong, nonatomic)SHSport *createdSport;

@end

@implementation CreateSportsFromArrayTests

- (void)setUp
{
    [super setUp];
    [CoreDataManager createTestStore];
    NSArray *exactSportList =@[@{@"category": @"Football",
                                 @"name": @"Default",
                                 @"scoreboard": @"generic",
                                 @"minTeams": @2,
                                 @"maxTeams": @2,
                                 @"periods": @2,
                                 @"gameClockEnabled": @1,
                                 @"gameClockCountUp": @1,
                                 @"gameClockTime": @270000,
                                 @"gameClockBuzzerEnabled": @0,
                                 @"shotClockEnabled": @0},
                               
                               @{@"category": @"WaterPolo",
                                 @"name": @"New Name",
                                 @"scoreboard": @"generic",
                                 @"minTeams": @2,
                                 @"maxTeams": @2,
                                 @"periods": @2,
                                 @"gameClockEnabled": @1,
                                 @"gameClockCountUp": @1,
                                 @"gameClockTime": @270000,
                                 @"gameClockBuzzerEnabled": @0,
                                 @"shotClockEnabled": @0},
                               ];
    self.subject = [SportService new];
    [self.subject createSportsFromArray:exactSportList];
}

- (void)tearDown
{
    [CoreDataManager clearTestStore];
    [super tearDown];
}

- (void)test_correct_number_of_sports_are_created
{
    NSFetchedResultsController *allController = [self.subject fetchAllSports];
    NSInteger count = [allController.fetchedObjects count];

    XCTAssertTrue(count == 2, @"there should be two objects created");
}

- (void)test_objects_have_different_categories
{
    NSFetchedResultsController *waterPoloController = [self.subject fetchAllSportsInCategory:@"WaterPolo"];
    NSInteger count = [waterPoloController.fetchedObjects count];
    XCTAssertTrue(count == 1, @"there should only be 1 team with the category waterpolo there are %li",(long)count);
}


@end
