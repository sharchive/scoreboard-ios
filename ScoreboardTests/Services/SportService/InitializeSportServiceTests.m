//
//  InitializeSportServiceTests.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SportService.h"
#import "CoreDataManager.h"

@interface InitializeSportServiceTests : XCTestCase

@property (strong, nonatomic)SportService *subject;

@end

@implementation InitializeSportServiceTests

- (void)setUp
{
    [super setUp];
    [CoreDataManager createTestStore];
    self.subject = [SportService new];
}

- (void)tearDown
{
    [CoreDataManager clearTestStore];
    [super tearDown];
}

- (void)test_service_is_not_nil
{
    XCTAssertNotNil(self.subject, @"service should not be nil");
}

@end
