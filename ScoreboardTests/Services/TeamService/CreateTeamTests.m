//
//  CreateTeamTests.m
//  Scoreboard
//
//  Created by Tom Bates on 23/02/2014.
//  Copyright (c) 2014 Sport Search UK. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TeamService.h"
#import "CoreDataManager.h"
#import "Team.h"

@interface CreateTeamTests : XCTestCase

@property (strong, nonatomic)TeamService *subject;
@property (strong, nonatomic)Team *createdTeam;

@end

@implementation CreateTeamTests

- (void)setUp
{
    [super setUp];
    [CoreDataManager createTestStore];
    self.subject = [TeamService new];
    self.createdTeam = [self.subject createTeam];
}

- (void)tearDown
{
    [CoreDataManager clearTestStore];
    [super tearDown];
}

- (void)test_sport_is_not_nil
{
    XCTAssertNotNil(self.createdTeam, @"created sport should not be nil");
}

@end
