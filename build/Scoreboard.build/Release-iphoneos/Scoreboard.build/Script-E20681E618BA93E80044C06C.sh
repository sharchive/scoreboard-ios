#!/bin/sh
PATH=${PATH}:/usr/local/bin
MODELS_DIR="${PROJECT_DIR}/Scoreboard/CoreData"
DATA_MODEL_PACKAGE="$MODELS_DIR/_Model/Scoreboard.xcdatamodeld"
mogenerator --model "$DATA_MODEL_PACKAGE/Scoreboard.xcdatamodel" --output-dir "$MODELS_DIR/Objects" --template-var arc=true
