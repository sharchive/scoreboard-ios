#!/bin/sh
infoplist="${PROJECT_DIR}/Scoreboard/Scoreboard-Info.plist"
builddate=`date +%d-%m-%y\ %R`
if [[ -n "$builddate" ]]; then
/usr/libexec/PlistBuddy -c "Add :CFBuildDate $builddate" ${infoplist}
/usr/libexec/PlistBuddy -c "Set :CFBuildDate $builddate" ${infoplist}
fi
